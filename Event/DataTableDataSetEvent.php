<?php

namespace DolmIT\DataTablesBundle\Event;

use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use Symfony\Component\EventDispatcher\Event;

class DataTableDataSetEvent extends Event
{
    /**
     * @var array|null
     */
    private $data;

    /**
     * @var DataTableInterface
     */
    private $dataTable;

    public function __construct(DataTableInterface $dataTable, ?array $data)
    {
        $this->setDataTable($dataTable);
        $this->setData($data);
    }

    /**
     * @return array
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return $this
     */
    public function setData(?array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return DataTableInterface
     */
    public function getDataTable(): DataTableInterface
    {
        return $this->dataTable;
    }

    /**
     * @param DataTableInterface $dataTable
     *
     * @return $this
     */
    public function setDataTable(DataTableInterface $dataTable): self
    {
        $this->dataTable = $dataTable;

        return $this;
    }
}
