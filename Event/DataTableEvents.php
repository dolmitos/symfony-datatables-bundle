<?php

namespace DolmIT\DataTablesBundle\Event;

final class DataTableEvents
{
    /**
     * The DATATABLE_DATA_SET event occurs whenever data is set to the datatable.
     *
     * This event allows you to add additional elements to the data array before it is
     * attached to the DataTable object
     *
     * @Event("DolmIT\DataTablesBundle\Event\DataTableDataSetEvent")
     */
    const DATATABLE_DATA_SET = 'datatable.data.set';
}
