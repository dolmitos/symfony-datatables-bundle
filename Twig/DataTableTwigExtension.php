<?php

namespace DolmIT\DataTablesBundle\Twig;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackValidator;
use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use DolmIT\DataTablesBundle\DataTable\Formatter\DataTableFormatter;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Class DataTableTwigExtension.
 */
class DataTableTwigExtension extends Twig_Extension
{
    /**
     * The PropertyAccessor.
     *
     * @var PropertyAccessor
     */
    protected $accessor;

    /**
     * DataTableTwigExtension constructor.
     */
    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'dolmit_datatables_twig_extension';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction(
                'render_datatable',
                [$this, 'renderDataTable'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'render_datatable_table',
                [$this, 'renderDataTableTable'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'render_datatable_filter',
                [$this, 'renderDataTableFilter'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'render_datatable_statistics',
                [$this, 'renderDataTableStatistics'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'render_datatable_action_bar',
                [$this, 'renderDataTableActionBar'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new Twig_SimpleFunction(
                'isCallback',
                [$this, 'isCallback'],
                []
            ),
            new Twig_SimpleFunction(
                'render_datatable_tabulator_config',
                [$this, 'renderDataTableTabulatorConfig'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
        ];
    }

    //-------------------------------------------------
    // Functions
    //-------------------------------------------------

    public function isCallback($variable)
    {
        return CallbackValidator::isCallback($variable);
    }

    /**
     * Renders the template.
     *
     * @param Twig_Environment   $twig
     * @param DataTableInterface $dataTable
     *
     * @return string
     */
    public function renderDataTable(Twig_Environment $twig, DataTableInterface $dataTable)
    {
        return $twig->render(
            '@DolmITDataTables/datatable/datatable-container.html.twig',
            [
                'datatable_object' => $dataTable,
            ]
        );
    }

    /**
     * Renders the template.
     *
     * @param Twig_Environment   $twig
     * @param DataTableInterface $dataTable
     *
     * @return string
     */
    public function renderDataTableTable(Twig_Environment $twig, DataTableInterface $dataTable)
    {
        $formatter = new DataTableFormatter();
        $formatter->runFormatters($dataTable);

        return $twig->render(
            '@DolmITDataTables/datatable/datatable.html.twig',
            [
                'datatable_object' => $dataTable,
            ]
        );
    }

    /**
     * Renders a Filter template.
     *
     * @param Twig_Environment   $twig
     * @param DataTableInterface $dataTable
     *
     * @return string
     */
    public function renderDataTableFilter(Twig_Environment $twig, DataTableInterface $dataTable)
    {
        $form = $formView = null;

        if (
            $dataTable->getFilterBuilder()->getFormBuilder() &&
            $dataTable->getFilterBuilder()->getFormBuilder()->count()
        ) {
            $form = $dataTable->getFilterBuilder()->getFormBuilder()->getForm();
            $formView = $form->createView();
        }

        return $twig->render(
            '@DolmITDataTables/filter/filter-set.html.twig',
            [
                'datatable_object' => $dataTable,
                'datatable_filter_form' => $form,
                'datatable_filter_form_view' => $formView,
            ]
        );
    }

    /**
     * Renders a Statistics template.
     *
     * @param Twig_Environment   $twig
     * @param DataTableInterface $dataTable
     *
     * @return string
     */
    public function renderDataTableStatistics(Twig_Environment $twig, DataTableInterface $dataTable)
    {
        return $twig->render(
            '@DolmITDataTables/statistics/statistics.html.twig',
            [
                'datatable_object' => $dataTable,
            ]
        );
    }

    /**
     * Renders the ActionBar.
     *
     * @param Twig_Environment   $twig
     * @param DataTableInterface $dataTable
     *
     * @return string
     */
    public function renderDataTableActionBar(Twig_Environment $twig, DataTableInterface $dataTable)
    {
        return $twig->render(
            '@DolmITDataTables/actions/action-bar.html.twig',
            [
                'datatable_object' => $dataTable,
            ]
        );
    }

    /**
     * Renders the ActionBar.
     *
     * @param Twig_Environment   $twig
     * @param DataTableInterface $dataTable
     *
     * @return string
     */
    public function renderDataTableTabulatorConfig(Twig_Environment $twig, DataTableInterface $dataTable)
    {
        $formatter = new DataTableFormatter();
        $formatter->runConfigFormatters($dataTable);

        return $twig->render(
            '@DolmITDataTables/datatable/datatable-tabulator-config.js.twig',
            [
                'datatable_object' => $dataTable,
            ]
        );
    }
}
