TODO
==================================

List improvements to be made
----------------------------

1. Improve documentation
2. Create a command for creating a DataTable class
3. Add ability to hide columns, rows
4. Add row and table based editability
5. Add tests
6. Decide on row handling, currently the package is focused heavily towards Tabulator, which has no need for rows.

List of future improvements to be made
--------------------------------------
1. Add more libraries (like Datatables)
