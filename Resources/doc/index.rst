Getting Started With DolmITDataTablesBundle
==================================


Prerequisites
-------------

This version of the bundle requires Symfony 4+.


Installation
------------

1. Download dolmitos/symfony-datatables-bundle using composer
2. Enable the Bundle

Step 1: Download DolmITDataTablesBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Require the bundle with composer:

.. code-block:: bash

    $ composer require dolmitos/symfony-datatables-bundle

Composer will install the bundle to your project's ``vendor/dolmitos/symfony-datatables-bundle`` directory.

Step 2: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~

Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new DolmIT\DataTablesBundle\DolmITDataTablesBundle(),
            // ...
        );
    }
    ?>

Step 3: Satisfy requirements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Add jQuery (~3.3.1) and jQuery UI (~1.12.1) to your site::

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

Enable Tabulator by adding the following files to the pages DataTables - Tabulator are called on::

    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.5.3/css/tabulator.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tabulator/3.5.3/js/tabulator.min.js"></script>
Enable bundle assets::

    <script type="text/javascript" src="{{ asset('bundles/dolmitdatatables/build/js/datatables.js') }}"></script>