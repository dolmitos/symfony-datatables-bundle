Why did we need this?
==================================

#. We needed a DataTable that has a very customizable FilterSet
#. DataTable output cannot be directly connected to a single Entity, as we require tables for multiple types of Data
#. We needed the DataTable to have multiple structural forms, from very basic to highly complex, meanwhile still being connected to the same data source.
#. We needed to have much more than 1 table per page.