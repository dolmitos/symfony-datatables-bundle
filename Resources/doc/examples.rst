Examples
==================================

Create DataTable
----------------

1: Create from Table class
~~~~~~~~~~~~~~~~~~~~~~~~~~

Create Table from own class::

    $dataTable = $dataTableFactory->create(TestDataTable::class);
    $dataTable->buildDataTable();

buildDataTable function::

    public function buildDataTable(array $options = array())
    {
        $this->getOptions()->set([]);

        $this->getColumnBuilder()
            ->add('id', Column::class, array(
                'title' => 'Id',
            ))
            ->add('title', Column::class, array(
                'title' => 'Title',
                'editable' => [
                    TextEditable::class,
                    [
                        'editor_params' => null
                    ],
                ]
            ))
            ->add('first_name', Column::class, array(
                'title' => 'First Name',
            ))
            ->add('last_name', Column::class, array(
                'title' => 'Last name',
            ))
            ->add('phone', Column::class, array(
                'title' => 'Phone',
            ))
            ->add('email', Column::class, array(
                'title' => 'Email',
            ))
        ;

        $data = array(
            array("first_name"=>"Bevis","last_name"=>"Pearson","phone"=>"1-450-495-5187","email"=>"augue.eu.tempor@ultriciessemmagna.ca"),
            array("first_name"=>"Sybill","last_name"=>"Madden","phone"=>"1-212-427-4773","email"=>"sed@tempuslorem.net"),
            array("first_name"=>"Quynn","last_name"=>"Solis","phone"=>"1-604-367-8549","email"=>"scelerisque.mollis.Phasellus@sitametconsectetuer.ca"),
            array("first_name"=>"Laura","last_name"=>"Rutledge","phone"=>"236-9688","email"=>"dictum@dictumeleifend.net"),
            array("first_name"=>"Forrest","last_name"=>"Beasley","phone"=>"845-0010","email"=>"Sed.eu.eros@gravidasitamet.ca"),
        );

        foreach ($data as $row) {
            $this->getRowBuilder()
                ->add(Row::class, array(
                    'data' => $row,
                ));
        }

    }

Use DataTableLoaderTrait::

    public function buildDataTable(array $options = array())
    {
        $this
            ->loadData($options['data'] ?? null)
            ->loadTemplate($options['template'] ?? null)
            ->loadSettings($options['settings'] ?? null)
        ;
    }


2: Create from data
~~~~~~~~~~~~~~~~~~~

Create Table from input::

    $dataTable = $dataTableFactory->build(
        $data,
        $options
    );

    $options = ['template' => [],'settings'=>[]];


3: Add filters
~~~~~~~~~~~~~~

Add filter to datatable structure::

    $dataTable->getFilterBuilder()->create(
        $typeOfFilter, //If null, FilterForm::class will be used
        $defaultData,
        $options,
        $filters
    );

Example::

    $dataTable->getFilterBuilder()->create(
        null,
        [
            'filtergroup1' => [
                'first_name' => 'First name',
                'last_name' => 'Last name',
            ]
        ],
        [
            'method' => 'POST',
            'action' => '/route/to/controller/function',
            'attr' => [
                'class' => 'form-inline',
            ]
        ],
        [
            'test' => [
                'class' => SymfonyFormClass::class,
                'options' => [
                    'title' => 'Test',
                ]
            ],
        ]
    );
