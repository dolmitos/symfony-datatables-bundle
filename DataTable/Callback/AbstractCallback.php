<?php

namespace DolmIT\DataTablesBundle\DataTable\Callback;

use DolmIT\DataTablesBundle\DataTable\OptionsTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractCallback implements CallbackInterface
{
    use OptionsTrait;

    /**
     * @var string
     */
    protected $template;

    /**
     * @var array|null
     */
    protected $variables;

    /**
     * Options constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->initOptions();
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'template' => null,
            'variables' => [],
        ]);

        $resolver->setAllowedTypes('template', ['string']);
        $resolver->setAllowedTypes('variables', ['null', 'array']);

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     *
     * @return $this
     */
    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getVariables(): ?array
    {
        return $this->variables;
    }

    /**
     * @param array|null $variables
     *
     * @return $this
     */
    public function setVariables(?array $variables): self
    {
        $this->variables = $variables;

        return $this;
    }
}
