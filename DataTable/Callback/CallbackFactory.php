<?php

namespace DolmIT\DataTablesBundle\DataTable\Callback;

use DolmIT\DataTablesBundle\DataTable\Exception\CallbackFactoryException;

class CallbackFactory
{
    /**
     * Create.
     *
     * @param string|object $class
     *
     * @return object
     *
     * @throws CallbackFactoryException
     */
    public static function create($class)
    {
        if (empty($class) || !\is_string($class) && !$class instanceof CallbackInterface) {
            throw new CallbackFactoryException('Factory::create(): String or CallbackInterface expected.');
        }

        if ($class instanceof CallbackInterface) {
            /* @var $class object */
            return $class;
        }

        if (\is_string($class) && class_exists($class)) {
            $instance = new $class();

            if (!$instance instanceof CallbackInterface) {
                throw new CallbackFactoryException("Factory::create(): Expected $class to be an instance of CallbackInterface.");
            } else {
                return $instance;
            }
        } else {
            throw new CallbackFactoryException("Factory::create(): $class is not callable.");
        }
    }
}
