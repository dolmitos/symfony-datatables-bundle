<?php

namespace DolmIT\DataTablesBundle\DataTable\Callback;

use Symfony\Component\OptionsResolver\OptionsResolver;

interface CallbackInterface
{
    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return mixed
     */
    public function configureOptions(OptionsResolver $resolver);

    /**
     * Get a string describing a template.
     *
     * @return string|null
     */
    public function getTemplate(): ?string;

    /**
     * Get an array of values to use
     * in a template.
     *
     * @return array|null
     */
    public function getVariables(): ?array;
}
