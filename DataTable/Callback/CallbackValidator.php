<?php

namespace DolmIT\DataTablesBundle\DataTable\Callback;

class CallbackValidator
{
    public static function isCallback($variable)
    {
        return $variable instanceof CallbackInterface;
    }
}
