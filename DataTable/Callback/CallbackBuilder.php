<?php

namespace DolmIT\DataTablesBundle\DataTable\Callback;

use Exception;
use Twig_Environment;

class CallbackBuilder
{
    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    private $twig;

    /**
     * The name of the associated DataTable.
     *
     * @var string
     */
    private $datatableName;

    /**
     * CallbackBuilder constructor.
     *
     * @param Twig_Environment $twig
     * @param string           $datatableName
     */
    public function __construct(Twig_Environment $twig, $datatableName)
    {
        $this->twig = $twig;
        $this->datatableName = $datatableName;
    }

    //-------------------------------------------------
    // Builder
    //-------------------------------------------------

    /**
     * Add Callback.
     *
     * @param string|CallbackInterface $class
     * @param array                    $options
     *
     * @return $this
     *
     * @throws Exception
     * @throws \DolmIT\DataTablesBundle\DataTable\Exception\CallbackFactoryException
     */
    public function add($class, array $options = [])
    {
        /* @var $callback AbstractCallback */
        $callback = CallbackFactory::create($class);
        $callback->initOptions();

        $callback->set($options);

        return $this;
    }

    /**
     * Build Callback.
     *
     * @param string|CallbackInterface $class
     * @param array                    $options
     *
     * @return AbstractCallback
     *
     * @throws \DolmIT\DataTablesBundle\DataTable\Exception\CallbackFactoryException
     * @throws Exception
     */
    public function build($class, array $options = [])
    {
        /* @var $callback AbstractCallback */
        $callback = CallbackFactory::create($class);
        $callback->initOptions();

        $callback->set($options);

        return $callback;
    }
}
