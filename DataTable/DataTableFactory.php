<?php

namespace DolmIT\DataTablesBundle\DataTable;

use DolmIT\DataTablesBundle\DataTable\Exception\DataTableFactoryException;
use DolmIT\DataTablesBundle\DataTable\Options\OptionsFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;

/**
 * Class DataTableFactory.
 */
class DataTableFactory
{
    /**
     * The Translator service.
     *
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * Options.
     *
     * @var OptionsFactory
     */
    protected $optionsFactory;

    /**
     * Options.
     *
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var RouterInterface|null
     */
    protected $router;

    /**
     * @var TokenStorageInterface|null
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface|null
     */
    protected $authorizationChecker;

    /**
     * DataTableFactory constructor.
     *
     * @param TranslatorInterface      $translator
     * @param Twig_Environment         $twig
     * @param OptionsFactory           $optionsFactory
     * @param FormFactory              $formFactory
     * @param EventDispatcherInterface $eventDispatcher
     * @param RouterInterface          $router
     * @param TokenStorageInterface    $tokenStorage
     */
    public function __construct(
        TranslatorInterface $translator,
        Twig_Environment $twig,
        OptionsFactory $optionsFactory,
        FormFactory $formFactory,
        EventDispatcherInterface $eventDispatcher,
        ?RouterInterface $router,
        ?TokenStorageInterface $tokenStorage,
        ?AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->translator = $translator;
        $this->twig = $twig;
        $this->optionsFactory = $optionsFactory;
        $this->formFactory = $formFactory;
        $this->eventDispatcher = $eventDispatcher;
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    //-------------------------------------------------
    // Create DataTable
    //-------------------------------------------------

    /**
     * Create DataTable.
     *
     * @param string      $class
     * @param string|null $tableType
     *
     * @return DataTableInterface
     *
     * @throws DataTableFactoryException
     */
    public function create($class, ?string $tableType = null)
    {
        if (!\is_string($class)) {
            $type = \gettype($class);
            throw new DataTableFactoryException("DataTableFactory::create(): String expected, $type given");
        }

        if (\is_string($tableType)) {
            if (!\in_array($tableType, [
                DataTableInterface::TABLE_TYPE_HTML,
                DataTableInterface::TABLE_TYPE_TABULATOR,
            ])) {
                throw new DataTableFactoryException("DataTableFactory::create(): Type expected to be a table type defined in DataTableInterface, $tableType given");
            }
        }

        if (false === class_exists($class)) {
            throw new DataTableFactoryException("DataTableFactory::create(): $class does not exist");
        }

        if (\in_array(DataTableInterface::class, class_implements($class))) {
            return new $class(
                $this->translator,
                $this->twig,
                $this->optionsFactory,
                $this->formFactory,
                $this->eventDispatcher,
                $this->router,
                $this->tokenStorage,
                $this->authorizationChecker,
                $tableType
            );
        } else {
            throw new DataTableFactoryException("DataTableFactory::create(): The class $class should implement the DataTableInterface.");
        }
    }

    /**
     * Make DataTable from given parameters.
     *
     * $input can be a string of class that implements DataTableInterface
     * or an array of data, which will be loaded into an automatic table
     *
     * @param array|string $input
     * @param array        $options
     * @param string       $tableType
     *
     * @return DataTableInterface
     *
     * @throws DataTableFactoryException
     * @throws \Exception
     */
    public function make($input, array $options = [], string $tableType = DataTableInterface::TABLE_TYPE_TABULATOR)
    {
        if (\is_string($input)) {
            $dataTable = $this->create($input, $tableType);
        } elseif (\is_array($input)) {
            $dataTable = $this->create(AutomaticDataTable::class, $tableType);
        } else {
            throw new DataTableFactoryException('DataTableFactory::create(): The input variable should 
                a) Be a string of a class name implementing DataTableInterface or
                b) Be an associative array of data');
        }

        if (is_subclass_of($dataTable, AbstractDataTable::class)) {
            /* @var $dataTable AbstractDataTable */
            // Set all the data for the DataTable
            if (\is_array($input)) {
                $options['data'] = $input;
            }

            $dataTable->setData($options['data'] ?? null);
            $dataTable->setTemplate($options['template'] ?? null);
            $dataTable->setSettings($options['settings'] ?? null);
        }

        return $dataTable;
    }

    /**
     * @param        $input
     * @param array  $options
     * @param string $tableType
     *
     * @return DataTableInterface
     *
     * @throws DataTableFactoryException
     */
    public function build($input, array $options = [], string $tableType = DataTableInterface::TABLE_TYPE_TABULATOR)
    {
        $dataTable = $this->make($input, $options, $tableType);

        $dataTable->buildDataTable($options);

        return $dataTable;
    }
}
