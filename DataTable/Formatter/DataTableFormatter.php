<?php

namespace DolmIT\DataTablesBundle\DataTable\Formatter;

use DolmIT\DataTablesBundle\DataTable\Callback\Callback;
use DolmIT\DataTablesBundle\DataTable\Column\FormattableColumnInterface;
use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use Symfony\Component\Form\FormTypeInterface;

class DataTableFormatter
{
    /**
     * Format $dataTable $data.
     *
     * @param DataTableInterface
     */
    private function runFormFormatter(DataTableInterface $dataTable)
    {
        $editFormView = $viewFormView = $editPrototype = $viewPrototype = null;
        if (null !== ($editFormBuilder = $dataTable->getEditFormBuilder()->getFormBuilder())) {
            $editForm = $editFormBuilder->getForm();
            $options = $editForm->getConfig()->getOptions();
            $parent = $options['parent'] ?? null;
            if (null !== $parent) {
                if (!$parent instanceof FormTypeInterface) {
                    $parent = $editFormBuilder->getFormFactory()->createBuilder($parent)->getForm();
                }
                $editForm->setParent($parent);
            }

            $editFormView = $editForm->createView();
            $editPrototype = $editFormView->vars['prototype'];
        }
        if (null !== ($viewFormBuilder = $dataTable->getViewFormBuilder()->getFormBuilder())) {
            $viewForm = $viewFormBuilder->getForm();
            $options = $viewForm->getConfig()->getOptions();
            $parent = $options['parent'] ?? null;
            if (null !== $parent) {
                if (!$parent instanceof FormTypeInterface) {
                    $parent = $viewFormBuilder->getFormFactory()->createBuilder($parent)->getForm();
                }
                $viewForm->setParent($parent);
            }

            $viewFormView = $viewForm->createView();
            $viewPrototype = $viewFormView->vars['prototype'];
        }

        if (null === $editPrototype && null === $viewPrototype) {
            return false;
        }

        $columns = $dataTable->getColumnBuilder()->getRealColumns();

        if ($columns) {
            $editPrototypeFields = $viewPrototypeFields = [];
            if ($editPrototype) {
                $editPrototypeFields = $editPrototype->children;
            }
            if ($viewPrototype) {
                $viewPrototypeFields = $viewPrototype->children;
            }

            foreach ($columns as $column) {
                if ($column instanceof FormattableColumnInterface) {
                    if (null !== $column->getFormatter()) {
                        continue;
                    }

                    $viewPrototypeField = $this->getPrototypeField($viewPrototypeFields, $column->getIdentifier()) ?? null;
                    $editPrototypeField = $this->getPrototypeField($editPrototypeFields, $column->getIdentifier()) ?? null;

                    if ($editPrototypeField || $viewPrototypeField) {
                        $formFormatter = $dataTable->getCallbackBuilder()->build(Callback::class, [
                            'template' => '@DolmITDataTables/callbacks/formatters/form_column_formatter_callback.js.twig',
                            'variables' => [
                                'editFormPrefix' => $editFormView ? $editFormView->vars['id'] : null,
                                'viewFormPrefix' => $viewFormView ? $viewFormView->vars['id'] : null,
                                'editTemplate' => $editPrototypeField,
                                'viewTemplate' => $viewPrototypeField,
                                'column' => $column,
                                'datatable_object' => $dataTable,
                            ],
                        ]);
                        $column->setFormatter($formFormatter);
                    }
                }
            }
        }
    }

    private function getPrototypeField($prototypeFields = [], $columnIdentifier)
    {
        $prototypeField = null;
        if (false !== strpos($columnIdentifier, '.')) {
            $level = 0;
            $path = explode('.', $columnIdentifier);
            $tmpPrototypeFields = $prototypeFields;
            while (isset($tmpPrototypeFields[$path[$level]]) && $tmpPrototypeFields[$path[$level]]->children) {
                $tmpPrototypeFields = $tmpPrototypeFields[$path[$level]]->children;
                ++$level;
            }
            $prototypeField = $tmpPrototypeFields[$path[$level]] ?? null;
        } else {
            $prototypeField = $prototypeFields[$columnIdentifier] ?? null;
        }

        return $prototypeField;
    }

    /**
     * Format $dataTable $data.
     *
     * @param DataTableInterface
     */
    private function runDataFormatter(DataTableInterface $dataTable)
    {
        $columns = $dataTable->getColumnBuilder()->getRealColumns();

        $data = $dataTable->getData();

        if ($data) {
            foreach ($data as $key => &$row) {
                foreach ($columns as $column) {
                    $column->renderCellContent($row);
                }
            }
        }

        if (null !== $dataTable->getEditFormBuilder()->getFormBuilder() || null !== $dataTable->getViewFormBuilder()->getFormBuilder()) {
            foreach ($data as &$row) {
                $row[$dataTable->getOptions()->getEditableKey()] = ((null !== $dataTable->getEditFormBuilder()->getFormBuilder()) ? $dataTable->getOptions()->isDefaultCellStateEditable() : false);
            }
        }

        $dataTable->setData($data);
    }

    /**
     * Format $dataTable $statisticsData.
     *
     * @param DataTableInterface $dataTable
     */
    private function runStatisticsFormatter(DataTableInterface $dataTable)
    {
        $statistics = $dataTable->getStatisticsBuilder()->getStatistics();

        $data = $dataTable->getStatisticsData();

        if ($data) {
            foreach ($data as $key => &$value) {
                foreach ($statistics as $statistic) {
                    if ($statistic->getIdentifier() == $key) {
                        $statistic->renderCellContent($value);
                    }
                }
            }
        }

        $dataTable->setStatisticsData($data);
    }

    public function runConfigFormatters(DataTableInterface $dataTable)
    {
        $this->runFormFormatter($dataTable);
    }

    /**
     * @param DataTableInterface $dataTable
     */
    public function runFormatters(DataTableInterface $dataTable)
    {
        $this->runFormFormatter($dataTable);
        $this->runDataFormatter($dataTable);
        $this->runStatisticsFormatter($dataTable);
    }
}
