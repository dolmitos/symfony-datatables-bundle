<?php

namespace DolmIT\DataTablesBundle\DataTable\Form;

use Exception;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Twig_Environment;

abstract class BaseFormBuilder
{
    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    private $twig;

    /**
     * The name of the associated DataTable.
     *
     * @var string
     */
    private $datatableName;

    /**
     * The generated Form.
     *
     * @var FormBuilderInterface
     */
    private $formBuilder;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * FilterBuilder constructor.
     *
     * @param Twig_Environment     $twig
     * @param string               $datatableName
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(Twig_Environment $twig, $datatableName, FormFactoryInterface $formFactory)
    {
        $this->setTwig($twig);
        $this->setDatatableName($datatableName);

        $this->setFormFactory($formFactory);
    }

    //-------------------------------------------------
    // Builder
    //-------------------------------------------------

    /**
     * @param string $type
     * @param null   $data
     * @param array  $options
     * @param array  $filters
     *
     * @throws Exception
     */
    public function create($type = null, $data = null, array $options = [], array $fields = [])
    {
        if (null === $type) {
            $type = PrototypeFormType::class;
        }
        $this->setFormBuilder($this->getFormFactory()->createBuilder($type, $data, $options));

        if (\count($fields)) {
            foreach ($fields as $identifier => $field) {
                $this->addField($identifier, $field['class'], $field['options'] ?? []);
            }
        }
    }

    //-------------------------------------------------
    // Getters && Setters
    //-------------------------------------------------

    /**
     * @return bool
     */
    public function hasFormBuilder(): bool
    {
        return null !== $this->formBuilder;
    }

    /**
     * @return FormBuilderInterface|null
     */
    public function getFormBuilder(): ?FormBuilderInterface
    {
        return $this->formBuilder;
    }

    /**
     * @param FormBuilderInterface $formBuilder
     *
     * @return $this
     */
    public function setFormBuilder(FormBuilderInterface $formBuilder): self
    {
        $this->formBuilder = $formBuilder;

        return $this;
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig(): Twig_Environment
    {
        return $this->twig;
    }

    /**
     * @param Twig_Environment $twig
     *
     * @return $this
     */
    public function setTwig(Twig_Environment $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * @return string
     */
    public function getDatatableName(): string
    {
        return $this->datatableName;
    }

    /**
     * @param string $datatableName
     *
     * @return $this
     */
    public function setDatatableName(string $datatableName): self
    {
        $this->datatableName = $datatableName;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasFormFactory(): bool
    {
        return null !== $this->formFactory;
    }

    /**
     * @return mixed
     */
    public function getFormFactory(): FormFactoryInterface
    {
        return $this->formFactory;
    }

    /**
     * @param mixed $formFactory
     *
     * @return $this
     */
    public function setFormFactory(FormFactoryInterface $formFactory): self
    {
        $this->formFactory = $formFactory;

        return $this;
    }

    //-------------------------------------------------
    // Helper
    //-------------------------------------------------

    /**
     * Adds a field.
     *
     * @param string|null $identifier
     * @param string      $formType
     * @param array       $options
     *
     * @return $this
     */
    private function addField(?string $identifier, string $formType, array $options = [])
    {
        $this->getFormBuilder()->add($identifier, $formType, $options);

        return $this;
    }
}
