<?php

namespace DolmIT\DataTablesBundle\DataTable\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PrototypeFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('prototypeFormBuilder', $builder);
        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $prototype = null;
        if ($options['prototype']) {
            $prototypeOptions = array_replace([
                'required' => $options['required'],
                'label' => $options['prototype_name'].'label__',
            ], $options['entry_options']);

            if (null !== $options['prototype_data']) {
                $prototypeOptions['data'] = $options['prototype_data'];
            }
            $prototypeOptions['prototype'] = false;
            /** @var FormBuilderInterface $prototypeFormBuilder */
            $prototypeFormBuilder = $form->getConfig()->getAttribute('prototypeFormBuilder');
            /** @var FormBuilderInterface $prototypeBuilder */
            $prototypeBuilder = $prototypeFormBuilder->create($options['prototype_name'], \get_class($this), $prototypeOptions);

            /*
             * Add any missing fields to the prototype that were not defined in the class
             */
            foreach ($form->all() as $key => $child) {
                if (!$prototypeBuilder->has($key)) {
                    $prototypeBuilder->add($key, \get_class($child->getConfig()->getType()->getInnerType()), $child->getConfig()->getOptions());
                }
            }

            $prototype = $prototypeBuilder->getForm();
        }

        if ($prototype) {
            $view->vars['prototype'] = $prototype->setParent($form)->createView($view);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        if ($form->getConfig()->hasAttribute('prototype') && $view->vars['prototype']->vars['multipart']) {
            $view->vars['multipart'] = true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $entryOptionsNormalizer = function (Options $options, $value) {
            $value['block_name'] = 'entry';

            return $value;
        };

        $resolver->setDefaults([
            'parent' => null,
            'prototype' => true,
            'prototype_data' => null,
            'prototype_name' => '__name__',
            'entry_options' => [],
        ]);

        $resolver->setAllowedTypes('parent', ['null', 'string', FormTypeInterface::class]);
        $resolver->setNormalizer('entry_options', $entryOptionsNormalizer);
    }
}
