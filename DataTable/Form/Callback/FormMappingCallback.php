<?php

namespace DolmIT\DataTablesBundle\DataTable\Form\Callback;

class FormMappingCallback
{
    /**
     * Data identifier, which will be used to look for the requested data field.
     *
     * @var string|null
     */
    protected $identifier;

    /**
     * QuerySelector string to be used when searching for the correct
     * element from the template to add the data to.
     *
     * @var string|null
     */
    protected $querySelector;

    /**
     * Attribute name of the element obtained through the querySelector.
     * If attribute is given, the data will be added to the attribute, instead of the
     * element value or innerHTML.
     *
     * @var string|null
     */
    protected $attribute;

    /**
     * Attribute value, only used if no identifier is given to load the data from.
     *
     * @var string|null
     */
    protected $attributeValue;
    /**
     * Mapping type, can be for edit or view or both.
     *
     * @var string|null
     */
    protected $type;

    public function __construct($identifier = null, $querySelector = null, $attribute = null, $attributeValue = null, $type = FormMappingType::BOTH)
    {
        $this->identifier = $identifier;
        $this->querySelector = $querySelector;
        $this->attribute = $attribute;
        $this->attributeValue = $attributeValue;
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @return string|null
     */
    public function getQuerySelector(): ?string
    {
        return $this->querySelector;
    }

    /**
     * @return string|null
     */
    public function getAttribute(): ?string
    {
        return $this->attribute;
    }

    /**
     * @return string|null
     */
    public function getAttributeValue(): ?string
    {
        return $this->attributeValue;
    }

    /**
     * @return bool
     */
    public function isForEdit(): bool
    {
        return \in_array($this->getType(), [FormMappingType::EDIT, FormMappingType::BOTH]);
    }

    /**
     * @return bool
     */
    public function isForView(): bool
    {
        return \in_array($this->getType(), [FormMappingType::VIEW, FormMappingType::BOTH]);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
