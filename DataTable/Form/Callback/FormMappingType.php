<?php

namespace DolmIT\DataTablesBundle\DataTable\Form\Callback;

final class FormMappingType
{
    const BOTH = 0;
    const EDIT = 1;
    const VIEW = 2;
}
