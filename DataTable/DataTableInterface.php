<?php

namespace DolmIT\DataTablesBundle\DataTable;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackBuilder;
use DolmIT\DataTablesBundle\DataTable\Column\ColumnBuilder;
use DolmIT\DataTablesBundle\DataTable\Filter\FilterBuilder;
use DolmIT\DataTablesBundle\DataTable\Form\EditFormBuilder;
use DolmIT\DataTablesBundle\DataTable\Form\ViewFormBuilder;
use DolmIT\DataTablesBundle\DataTable\Options\AbstractOptions;
use DolmIT\DataTablesBundle\DataTable\Row\RowBuilder;
use DolmIT\DataTablesBundle\DataTable\Statistics\StatisticsBuilder;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;

/**
 * Interface DataTableInterface.
 */
interface DataTableInterface
{
    const NAME_REGEX = '/[a-zA-Z0-9\_]+/';
    const TABLE_TYPE_DATATABLES = 'DATATABLES';
    const TABLE_TYPE_TABULATOR = 'TABULATOR';
    const TABLE_TYPE_HTML = 'HTML';

    /**
     * Check if the table type is html.
     *
     * @return bool
     */
    public function isTypeHtml(): bool;

    /**
     * Check if the table type is Tabulator.
     *
     * @return bool
     */
    public function isTypeTabulator(): bool;

    /**
     * Check if the table type is Datatables.
     *
     * @return bool
     */
    public function isTypeDatatables(): bool;

    /**
     * Get the translator instance.
     *
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface;

    /**
     * Get the twig instance.
     *
     * @return Twig_Environment
     */
    public function getTwig(): Twig_Environment;

    /**
     * Builds the datatable.
     *
     * @param array $options
     */
    public function buildDataTable(array $options = []);

    /**
     * Get ColumnBuilder.
     *
     * @return ColumnBuilder
     */
    public function getColumnBuilder(): ColumnBuilder;

    /**
     * Get FilterBuilder.
     *
     * @return FilterBuilder
     */
    public function getFilterBuilder(): FilterBuilder;

    /**
     * Get EditFormBuilder.
     *
     * @return EditFormBuilder
     */
    public function getEditFormBuilder(): EditFormBuilder;

    /**
     * Get ViewFormBuilder.
     *
     * @return ViewFormBuilder
     */
    public function getViewFormBuilder(): ViewFormBuilder;

    /**
     * Get RowBuilder.
     *
     * @return RowBuilder
     */
    public function getRowBuilder(): RowBuilder;

    /**
     *  Get CallbackBuilder.
     *
     * @return CallbackBuilder
     */
    public function getCallbackBuilder(): CallbackBuilder;

    /**
     * Get StatisticsBuilder.
     *
     * @return StatisticsBuilder
     */
    public function getStatisticsBuilder(): StatisticsBuilder;

    /**
     * Returns the name of this datatable view.
     *
     * @return string
     */
    public function getName(): ?string;

    /**
     * Returns type of table.
     *
     * @return string
     */
    public function getType(): string;

    /**
     * @return array|null
     */
    public function getData(): ?array;

    /**
     * @param array|null $data
     *
     * @return $this
     */
    public function setData(?array $data);

    /**
     * @return array|null
     */
    public function getStatisticsData(): ?array;

    /**
     * @param array|null $statisticsData
     *
     * @return $this
     */
    public function setStatisticsData(?array $statisticsData);

    /**
     * @return AbstractOptions
     */
    public function getOptions(): AbstractOptions;

    /* DEFAULT VARIABLES */

    /**
     * @return string
     */
    public function getVariablePage(): string;

    /**
     * @return string
     */
    public function getVariablePerPage(): string;

    /**
     * @return string
     */
    public function getVariableFilters(): string;

    /**
     * @return string
     */
    public function getVariableSorters(): string;

    /**
     * @return string
     */
    public function getDefaultRequestMethod(): string;

    /**
     * @return int
     */
    public function getValuePerPage(): int;

    /**
     * @return int|null
     */
    public function getCount(): ?int;

    /**
     * @param int|null $count
     *
     * @return $this
     */
    public function setCount(?int $count);

    /**
     * @return int|null
     */
    public function getPage(): ?int;

    /**
     * @param int|null $page
     *
     * @return $this
     */
    public function setPage(?int $page);

    /**
     * @return int|null
     */
    public function getPerPage(): ?int;

    /**
     * @param int|null $perPage
     *
     * @return $this
     */
    public function setPerPage(?int $perPage);

    /**
     * @return string|null
     */
    public function getStatisticsClassName(): ?string;

    /**
     * @param string|null $statisticsClassName
     *
     * @return $this
     */
    public function setStatisticsClassName(?string $statisticsClassName);

    /**
     * @return array
     */
    public function getErrors(): array;

    /**
     * @param array $errors
     *
     * @return $this
     */
    public function setErrors(array $errors);
}
