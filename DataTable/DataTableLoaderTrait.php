<?php

namespace DolmIT\DataTablesBundle\DataTable;

use DolmIT\DataTablesBundle\DataTable\Column\Column;
use DolmIT\DataTablesBundle\DataTable\Exception\OptionsSetBeforeTemplateLoadedException;
use DolmIT\DataTablesBundle\DataTable\Row\Row;

/**
 * Trait for loader functions
 * Useful for automatically generating DataTable content.
 *
 * Trait DataTableLoaderTrait
 */
trait DataTableLoaderTrait
{
    /**
     * @return array
     */
    public function getIdentifiersFromData(): array
    {
        $identifiers = [];
        if ($this->getData()) {
            foreach ($this->getData() as $row) {
                foreach ($row as $key => $value) {
                    if (!\in_array($key, $identifiers)) {
                        $identifiers[] = $key;
                    }
                }
            }
        }
        $identifiers = array_unique($identifiers);

        return $identifiers;
    }

    /**
     * $columns structure
     * array(
     *      0 => array(
     *          'identifier' => 'string',
     *          'class' => 'string of valid class name for column' || null,
     *          'options' => array('of'=>'options') || [],
     *      ),
     * ).
     *
     * @param array $columns
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function loadColumns(array $columns): self
    {
        if (\count($columns)) {
            foreach ($columns as $column) {
                $options = $column['options'] ?? [];
                $options['title'] = $options['title'] ?? $this->getTranslationPrefix().$column['identifier'];
                $this->getColumnBuilder()
                    ->add(
                        $column['identifier'],
                        $column['class'] ?? Column::class,
                        $options);
            }
        }

        return $this;
    }

    /**
     * $rows structure
     * array(
     *      0 => array(
     *          'class' => 'string of valid class name for row' || null,
     *          'data' => array('ofkey'=>'values') || [],
     *      ),
     * ).
     *
     * @param array $rows
     *
     * @return $this
     */
    public function loadRows(array $rows): self
    {
        if (\count($rows)) {
            foreach ($rows as $row) {
                $this->getRowBuilder()
                    ->add(
                        $row['class'] ?? Row::class,
                        [
                            'data' => $row['data'],
                        ]
                    );
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function generateColumnsFromData(): array
    {
        $identifiers = $this->getIdentifiersFromData();

        $settings = $this->getSettings();

        $columns = [];
        if ($identifiers) {
            foreach ($identifiers as $identifier) {
                $columns[] = [
                    'identifier' => $identifier,
                    'class' => $settings['columns'][$identifier]['class'] ?? null,
                    'options' => $settings['columns'][$identifier]['options'] ?? null,
                ];
            }
        }

        return $columns;
    }

    /**
     * @return array
     */
    public function generateRowsFromData(): array
    {
        $settings = $this->getSettings();

        $rows = [];
        if (\count($this->getData())) {
            foreach ($this->getData() as $rowData) {
                $rows[] = [
                    'class' => $settings['rows']['class'] ?? null,
                    'data' => $rowData,
                ];
            }
        }

        return $rows;
    }

    /**
     * @param array|null $data
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function loadData(?array $data = []): self
    {
        if ($data) {
            $this->setData($data);
        }

        $columns = $this->generateColumnsFromData();
        $this->loadColumns($columns);

        $rows = $this->generateRowsFromData();
        $this->loadRows($rows);

        return $this;
    }

    /**
     * Array of default options.
     *
     * @param array|null $template
     *
     * @return DataTableLoaderTrait
     *
     * @throws OptionsSetBeforeTemplateLoadedException
     */
    public function loadTemplate(?array $template = []): self
    {
        if ($template) {
            $this->setTemplate($template);
        }

        $template = $this->getTemplate();

        if ($template && \count($template)) {
            $this->cleanOptions($template);

            if ($this->getOptions()->isOptionsLoaded()) {
                throw new OptionsSetBeforeTemplateLoadedException('Template options are being loaded after Options have already been set. Template has to be loaded before Options.');
            }

            $template['options_loaded'] = true;
            $this->getOptions()->set($template);
        }

        return $this;
    }

    /**
     * Array of option overriding settings.
     *
     * @param array|null $settings
     *
     * @return DataTableLoaderTrait
     */
    public function loadSettings(?array $settings = []): self
    {
        if ($settings) {
            $this->setSettings($settings);
        }

        $settings = $this->getSettings();

        if ($settings && \count($settings)) {
            $this->cleanOptions($settings);

            $template['options_loaded'] = true;
            $this->getOptions()->set($settings);
        }

        return $this;
    }

    /**
     * Clean the options array of items we don't
     * want to let through to the OptionsInterface.
     *
     * @param array $options
     */
    public function cleanOptions(array &$options)
    {
        unset($options['rows']);
        unset($options['columns']);
    }
}
