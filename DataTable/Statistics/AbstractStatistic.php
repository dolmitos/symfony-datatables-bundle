<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics;

use DolmIT\DataTablesBundle\DataTable\OptionsTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig_Environment;

abstract class AbstractStatistic implements StatisticInterface
{
    /*
     * Use the OptionsResolver.
     */
    use OptionsTrait;

    /**
     * Set the data identifier for the statistic from which the values are loaded.
     * Default: Loaded from config.
     *
     * Is set in the StatisticsBuilder.
     *
     * @var string
     */
    protected $identifier;

    /**
     * The position in the Statistics array.
     * Is set in the StatisticsBuilder.
     *
     * @var int
     */
    protected $index;

    /**
     * Set the column title.
     *
     * @var string
     */
    protected $title;

    /**
     * The Twig Environment to render Twig templates in Column rows.
     * Is set in the ColumnBuilder.
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * The name of the associated DataTable.
     * Is set in the ColumnBuilder.
     *
     * @var string
     */
    protected $datatableName;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * Options constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->initOptions();
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['identifier']);

        $resolver->setDefaults([
            'title' => null,
        ]);

        $resolver->setAllowedTypes('title', ['null', 'string']);

        return $this;
    }

    /**
     * @param mixed $value
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function renderCellContent(&$value)
    {
    }

    /**
     * @return FormFactory
     */
    public function getFormFactory(): FormFactory
    {
        return $this->formFactory;
    }

    /**
     * @param FormFactory $formFactory
     *
     * @return $this
     */
    public function setFormFactory(FormFactory $formFactory): self
    {
        $this->formFactory = $formFactory;

        return $this;
    }

    //-------------------------------------------------
    // Getters & Setters
    //-------------------------------------------------

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return AbstractColumn
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return AbstractColumn
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get Twig.
     *
     * @return Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Set Twig.
     *
     * @param Twig_Environment $twig
     *
     * @return $this
     */
    public function setTwig(Twig_Environment $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get datatableName.
     *
     * @return string
     */
    public function getDataTableName()
    {
        return $this->datatableName;
    }

    /**
     * Set datatableName.
     *
     * @param string $datatableName
     *
     * @return $this
     */
    public function setDataTableName($datatableName): self
    {
        $this->datatableName = $datatableName;

        return $this;
    }

    /**
     * Get index.
     *
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set index.
     *
     * @param int $index
     *
     * @return $this
     */
    public function setIndex($index): self
    {
        $this->index = $index;

        return $this;
    }
}
