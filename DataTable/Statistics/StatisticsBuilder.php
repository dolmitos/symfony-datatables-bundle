<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics;

use Exception;
use Symfony\Component\Form\FormFactory;
use Twig_Environment;

class StatisticsBuilder
{
    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * The name of the associated DataTable.
     *
     * @var string
     */
    private $datatableName;

    /**
     * The generated Statistics.
     *
     * @var array
     */
    private $statistics;

    /**
     * This variable stores the array of statistics names as keys and statistic ids as values
     * in order to perform search statistic id by name.
     *
     * @var array
     */
    private $statisticNames;

    /**
     * ColumnBuilder constructor.
     *
     * @param Twig_Environment $twig
     * @param string           $datatableName
     */
    public function __construct(Twig_Environment $twig, FormFactory $formFactory, $datatableName)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->datatableName = $datatableName;

        $this->statistics = [];
        $this->statisticNames = [];
    }

    //-------------------------------------------------
    // Builder
    //-------------------------------------------------

    /**
     * Add Statistic.
     *
     * @param string|null               $identifier
     * @param string|StatisticInterface $class
     * @param array                     $options
     *
     * @return $this
     *
     * @throws Exception
     */
    public function add(?string $identifier, $class, array $options = [])
    {
        /* @var $statistic AbstractStatistic */
        $statistic = StatisticsFactory::create($class);
        $statistic->initOptions();

        $this->setEnvironmentProperties($statistic);
        $statistic->set($options);

        $this->addStatistic($identifier, $statistic);

        return $this;
    }

    //-------------------------------------------------
    // Getters && Setters
    //-------------------------------------------------

    /**
     * @return StatisticInterface[]
     */
    public function getStatistics(): array
    {
        return $this->statistics;
    }

    /**
     * @param array $statistics
     *
     * @return StatisticsBuilder
     */
    public function setStatistics(array $statistics): self
    {
        $this->statistics = $statistics;

        return $this;
    }

    //-------------------------------------------------
    // Helper
    //-------------------------------------------------

    /**
     * Set environment properties.
     *
     * @param AbstractStatistic $statistic
     *
     * @return $this
     */
    private function setEnvironmentProperties(AbstractStatistic $statistic)
    {
        $statistic->setDataTableName($this->datatableName);
        $statistic->setTwig($this->twig);
        $statistic->setFormFactory($this->formFactory);

        return $this;
    }

    /**
     * Adds a Statistic.
     *
     * @param string|null       $identifier
     * @param AbstractStatistic $statistic
     *
     * @return $this
     */
    private function addStatistic(?string $identifier, AbstractStatistic $statistic)
    {
        $this->statistics[] = $statistic;
        $index = \count($this->statistics) - 1;
        $this->statisticNames[$identifier] = $index;
        $statistic->setIndex($index);
        $statistic->setIdentifier($identifier);

        return $this;
    }
}
