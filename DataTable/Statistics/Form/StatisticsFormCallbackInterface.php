<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics\Form;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

interface StatisticsFormCallbackInterface
{
    /**
     * Return a Form object from the given callback.
     *
     * @param FormFactoryInterface $formFactory
     * @param mixed                $value
     *
     * @return FormInterface
     */
    public function getForm(FormFactoryInterface $formFactory, $value): FormInterface;
}
