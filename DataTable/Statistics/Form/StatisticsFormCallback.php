<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics\Form;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class StatisticsFormCallback implements StatisticsFormCallbackInterface
{
    /**
     * @var callable
     */
    private $callback;

    /**
     * @param callable $callback The callable returning a form element
     */
    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * {@inheritdoc}
     */
    public function getForm(FormFactoryInterface $formFactory, $value): FormInterface
    {
        return \call_user_func($this->callback, $formFactory, $value);
    }
}
