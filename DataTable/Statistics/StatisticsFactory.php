<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics;

use DolmIT\DataTablesBundle\DataTable\Exception\StatisticsFactoryException;

class StatisticsFactory
{
    /**
     * Create.
     *
     * @param string|object $class
     *
     * @return object
     *
     * @throws StatisticsFactoryException
     */
    public static function create($class)
    {
        if (empty($class) || !\is_string($class) && !$class instanceof ColumnInterface) {
            throw new StatisticsFactoryException('Factory::create(): String or ColumnInterface expected.');
        }

        if ($class instanceof StatisticInterface) {
            /* @var $class object */
            return $class;
        }

        if (\is_string($class) && class_exists($class)) {
            $instance = new $class();

            if (!$instance instanceof StatisticInterface) {
                throw new StatisticsFactoryException("Factory::create(): Expected $class to be an instance of ColumnInterface.");
            } else {
                return $instance;
            }
        } else {
            throw new StatisticsFactoryException("Factory::create(): $class is not callable.");
        }
    }
}
