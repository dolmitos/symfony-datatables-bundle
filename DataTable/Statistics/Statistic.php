<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics;

use Symfony\Component\OptionsResolver\OptionsResolver;

class Statistic extends AbstractStatistic
{
    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([]);

        return $this;
    }
}
