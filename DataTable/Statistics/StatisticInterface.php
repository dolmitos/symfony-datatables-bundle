<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;

interface StatisticInterface
{
    public function configureOptions(OptionsResolver $resolver);

    /**
     * @return FormFactory|null
     */
    public function getFormFactory(): ?FormFactory;

    /**
     * @param FormFactory $formFactory
     *
     * @return mixed
     */
    public function setFormFactory(FormFactory $formFactory);

    /**
     * Render Cell contents based on Column type.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function renderCellContent(&$value);
}
