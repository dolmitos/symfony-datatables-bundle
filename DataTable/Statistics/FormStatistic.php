<?php

namespace DolmIT\DataTablesBundle\DataTable\Statistics;

use DolmIT\DataTablesBundle\DataTable\Editable\EditableTrait;
use DolmIT\DataTablesBundle\DataTable\Statistics\Form\FormCallback;
use DolmIT\DataTablesBundle\DataTable\Statistics\Form\StatisticsFormCallback;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormStatistic extends AbstractStatistic
{
    use EditableTrait;

    /**
     * @var FormCallback|null
     */
    protected $form;

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('form');

        $resolver->setAllowedTypes('form', [StatisticsFormCallback::class]);

        return $this;
    }

    /**
     * @param mixed $value
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function renderCellContent(&$value)
    {
        if (null !== $this->getFormCallback()) {
            $formCallback = $this->getFormCallback();
            $form = $formCallback->getForm($this->getFormFactory(), $value);

            $formView = $form->createView();

            $value = $this->getTwig()->render(
                '@DolmITDataTables/render/form.html.twig',
                [
                    'form' => $formView,
                ]
            );
        }
    }

    /**
     * Alias function.
     *
     * @return FormCallback|null
     */
    public function getFormCallback(): ?StatisticsFormCallback
    {
        return $this->getForm();
    }

    /**
     * @return FormCallback|null
     */
    public function getForm(): ?StatisticsFormCallback
    {
        return $this->form;
    }

    /**
     * @param FormCallback|null $form
     *
     * @return $this
     */
    public function setForm(?StatisticsFormCallback $form): self
    {
        $this->form = $form;

        return $this;
    }
}
