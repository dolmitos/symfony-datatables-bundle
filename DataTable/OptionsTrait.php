<?php

namespace DolmIT\DataTablesBundle\DataTable;

use Exception;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class OptionsTrait.
 */
trait OptionsTrait
{
    /**
     * Options container.
     *
     * @var array
     */
    protected $options;

    /**
     * The PropertyAccessor.
     *
     * @var PropertyAccessor
     */
    protected $accessor;

    //-------------------------------------------------
    // Public
    //-------------------------------------------------

    /**
     * Init optionsTrait.
     *
     * @param bool $resolve
     *
     * @return $this
     *
     * @throws Exception
     */
    public function initOptions($resolve = false)
    {
        $this->options = [];

        /* @noinspection PhpUndefinedMethodInspection */
        $this->accessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableMagicCall()
            ->getPropertyAccessor();

        if (true === $resolve) {
            $this->set($this->options);
        }

        return $this;
    }

    /**
     * Set options.
     *
     * @param array $options
     *
     * @return $this
     *
     * @throws Exception
     */
    public function set(array $options)
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $opts = $resolver->resolve($options);

        if (!$this->options) {
            $this->options = $opts;
        } else {
            $this->options = array_merge($this->options, $options);
        }

        $this->callingSettersWithOptions($this->options);

        return $this;
    }

    //-------------------------------------------------
    // Helper
    //-------------------------------------------------

    /**
     * Calls the setters.
     *
     * @param array $options
     *
     * @return $this
     */
    private function callingSettersWithOptions(array $options)
    {
        foreach ($options as $setter => $value) {
            $this->accessor->setValue($this, $setter, $value);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Option to JSON.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    protected function optionToJson($value)
    {
        if (\is_array($value)) {
            return json_encode($value);
        }

        return $value;
    }

    /**
     * Validates an array whether the "template" and "vars" options are set.
     *
     * @param array $array
     * @param array $other
     *
     * @return bool
     *
     * @throws Exception
     */
    protected function validateArrayForTemplateAndOther(array $array, array $other = ['template', 'vars'])
    {
        if (false === \array_key_exists('template', $array)) {
            throw new Exception(
                'OptionsTrait::validateArrayForTemplateAndOther(): The "template" option is required.'
            );
        }

        foreach ($array as $key => $value) {
            if (false === \in_array($key, $other)) {
                throw new Exception(
                    "OptionsTrait::validateArrayForTemplateAndOther(): $key is not an valid option."
                );
            }
        }

        return true;
    }
}
