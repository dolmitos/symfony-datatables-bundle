<?php

namespace DolmIT\DataTablesBundle\DataTable;

use DolmIT\DataTablesBundle\DataTable\Callback\Callback;
use DolmIT\DataTablesBundle\DataTable\Callback\CallbackBuilder;
use DolmIT\DataTablesBundle\DataTable\Column\ColumnBuilder;
use DolmIT\DataTablesBundle\DataTable\Exception\DataTableNameValidationException;
use DolmIT\DataTablesBundle\DataTable\Exception\OptionsFactoryException;
use DolmIT\DataTablesBundle\DataTable\Filter\FilterBuilder;
use DolmIT\DataTablesBundle\DataTable\Form\EditFormBuilder;
use DolmIT\DataTablesBundle\DataTable\Form\ViewFormBuilder;
use DolmIT\DataTablesBundle\DataTable\Options\AbstractOptions;
use DolmIT\DataTablesBundle\DataTable\Options\OptionsFactory;
use DolmIT\DataTablesBundle\DataTable\Row\RowBuilder;
use DolmIT\DataTablesBundle\DataTable\Statistics\StatisticsBuilder;
use DolmIT\DataTablesBundle\Event\DataTableDataSetEvent;
use DolmIT\DataTablesBundle\Event\DataTableEvents;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;

abstract class AbstractDataTable implements DataTableInterface
{
    /**
     * Set the type of the DataTable
     * Default: HTML
     * Options: HTML, TABULATOR.
     *
     * @var string
     */
    protected $type = self::TABLE_TYPE_HTML;

    protected $translationDomain = 'datatable';

    protected $translationPrefix = 'datatable.';

    /* Informational variables on current $data */

    /**
     * @var int|null
     */
    protected $count;

    /**
     * @var int|null
     */
    protected $page;

    /**
     * @var int|null
     */
    protected $perPage;

    /**
     * @var array
     */
    protected $errors = [];

    /* Informational variables END */

    /* VARIABLE OVERRIDES */

    protected $variablePage = 'page';
    protected $variablePerPage = 'size';
    protected $variableFilters = 'filters';
    protected $variableSorters = 'sorters';

    protected $defaultRequestMethod = 'POST';
    protected $valuePerPage = 10;

    /* VARIABLE OVERRIDES END */

    /**
     * @var string|null
     */
    protected $statisticsClassName;

    /**
     * Holds the current name for the DataTable.
     *
     * @var string
     */
    protected $name;

    /**
     * Holds the data that is being utilized by the DataTable.
     *
     * @var array|null
     */
    protected $data;

    /**
     * Holds the data that is being utilized by the DataTable statistics block.
     *
     * @var array|null
     */
    protected $statisticsData;

    /**
     * An array containing a set of default options for the DataTable.
     *
     * @var array|null
     */
    protected $template;

    /**
     * An array containing the options in array form for the DataTable.
     *
     * @var array|null
     */
    protected $settings;

    /**
     * The Translator service.
     *
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var EditFormBuilder
     */
    protected $editFormBuilder;

    /**
     * @var ViewFormBuilder
     */
    protected $viewFormBuilder;

    /**
     * A ColumnBuilder instance.
     *
     * @var ColumnBuilder
     */
    protected $columnBuilder;
    /**
     * A FilterBuilder instance.
     *
     * @var FilterBuilder
     */
    protected $filterBuilder;

    /**
     * A RowBuilder instance.
     *
     * @var RowBuilder
     */
    protected $rowBuilder;

    /**
     * An Options instance.
     *
     * @var AbstractOptions
     */
    protected $options;

    /**
     * A CallbackBuilder instance.
     *
     * @var CallbackBuilder
     */
    protected $callbackBuilder;

    /**
     * A StatisticsBuilder instance.
     *
     * @var StatisticsBuilder
     */
    protected $statisticsBuilder;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var RouterInterface|null
     */
    protected $router;
    /**
     * @var TokenStorageInterface|null
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface|null
     */
    protected $authorizationChecker;

    /**
     * AbstractDataTable constructor.
     *
     * @param TranslatorInterface                $translator
     * @param Twig_Environment                   $twig
     * @param OptionsFactory                     $optionsFactory
     * @param FormFactoryInterface               $formFactory
     * @param EventDispatcherInterface           $eventDispatcher
     * @param RouterInterface|null               $router
     * @param TokenStorageInterface|null         $tokenStorage
     * @param AuthorizationCheckerInterface|null $authorizationChecker
     * @param string|null                        $type
     *
     * @throws OptionsFactoryException
     */
    public function __construct(
        TranslatorInterface $translator,
        Twig_Environment $twig,
        OptionsFactory $optionsFactory,
        FormFactoryInterface $formFactory,
        EventDispatcherInterface $eventDispatcher,
        ?RouterInterface $router,
        ?TokenStorageInterface $tokenStorage,
        ?AuthorizationCheckerInterface $authorizationChecker,
        ?string $type = null
    ) {
        if ($type) {
            $this->setType($type);
        }
        if (!$this->getName()) {
            $this->setRandomName();
        }

        $this->validateName();

        $this->setTranslator($translator);
        $this->setTwig($twig);
        $this->setEventDispatcher($eventDispatcher);
        $this->setRouter($router);
        $this->setTokenStorage($tokenStorage);
        $this->setAuthorizationChecker($authorizationChecker);

        $this->columnBuilder = new ColumnBuilder($twig, $formFactory, $this->getName());
        $this->filterBuilder = new FilterBuilder($twig, $this->getName(), $formFactory);
        $this->editFormBuilder = new EditFormBuilder($twig, $this->getName(), $formFactory);
        $this->viewFormBuilder = new ViewFormBuilder($twig, $this->getName(), $formFactory);
        $this->rowBuilder = new RowBuilder($twig, $this->getName());
        $this->callbackBuilder = new CallbackBuilder($twig, $this->getName());
        $this->statisticsBuilder = new StatisticsBuilder($twig, $formFactory, $this->getName());

        $this->options = $optionsFactory->createByType($this->getType());

        $this->applyDefault();
    }

    //-------------------------------------------------
    // DataTableInterface
    //-------------------------------------------------

    public function isTypeHtml(): bool
    {
        return DataTableInterface::TABLE_TYPE_HTML === $this->type;
    }

    public function isTypeTabulator(): bool
    {
        return DataTableInterface::TABLE_TYPE_TABULATOR === $this->type;
    }

    public function isTypeDatatables(): bool
    {
        return DataTableInterface::TABLE_TYPE_DATATABLES === $this->type;
    }

    /**
     * @return FilterBuilder
     */
    public function getFilterBuilder(): FilterBuilder
    {
        return $this->filterBuilder;
    }

    /**
     * @return EditFormBuilder
     */
    public function getEditFormBuilder(): EditFormBuilder
    {
        return $this->editFormBuilder;
    }

    /**
     * @return ViewFormBuilder
     */
    public function getViewFormBuilder(): ViewFormBuilder
    {
        return $this->viewFormBuilder;
    }

    /**
     * @return ColumnBuilder
     */
    public function getColumnBuilder(): ColumnBuilder
    {
        return $this->columnBuilder;
    }

    /**
     * @return RowBuilder
     */
    public function getRowBuilder(): RowBuilder
    {
        return $this->rowBuilder;
    }

    //-------------------------------------------------
    // Private
    //-------------------------------------------------

    /**
     * Apply default parameters based on type.
     *
     * @throws Exception\CallbackFactoryException
     */
    private function applyDefault()
    {
        if ($this->isTypeTabulator()) {
            $this->applyDefaultTabulator();
        }
    }

    private function applyDefaultTabulator()
    {
        $paginationDataSent = $this->getCallbackBuilder()->build(Callback::class, [
            'template' => '@DolmITDataTables/callbacks/pagination_data_sent_callback.js.twig',
            'variables' => [
                'page' => $this->getVariablePage(),
                'size' => $this->getVariablePerPage(),
                'filters' => $this->getVariableFilters(),
                'sorters' => $this->getVariableSorters(),
            ],
        ]);

        $ajaxResponsePassthrough = $this->getCallbackBuilder()->build(Callback::class, [
            'template' => '@DolmITDataTables/callbacks/ajax_response_passthrough_callback.js.twig',
            'variables' => [
                'datatable_object' => $this,
            ],
        ]);

        $dataLoadedPassthrough = $this->getCallbackBuilder()->build(Callback::class, [
            'template' => '@DolmITDataTables/callbacks/data_loaded_passthrough_callback.js.twig',
            'variables' => [
                'datatable_object' => $this,
            ],
        ]);

        $renderStartedPassthrough = $this->getCallbackBuilder()->build(Callback::class, [
            'template' => '@DolmITDataTables/callbacks/render_started_passthrough_callback.js.twig',
            'variables' => [
                'datatable_object' => $this,
            ],
        ]);

        $this->getOptions()->set([
            'pagination_data_sent' => $paginationDataSent,
            'ajax_response_passthrough' => $ajaxResponsePassthrough,
            'data_loaded_passthrough' => $dataLoadedPassthrough,
            'render_started_passthrough' => $renderStartedPassthrough,
            'pagination_size' => $this->getValuePerPage(),
            'ajax_config' => $this->getDefaultRequestMethod(),
        ]);
    }

    /**
     * Checks the name only contains letters, numbers or underscores.
     *
     * @throws Exception
     */
    private function validateName()
    {
        if (1 !== preg_match(self::NAME_REGEX, $this->getName())) {
            throw new DataTableNameValidationException('AbstractDataTable::validateName(): The result of the getName method can only contain letters, numbers and underscore.');
        }
    }

    //-------------------------------------------------
    // Getters / Setters
    //-------------------------------------------------

    /**
     * {@inheritdoc}
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set a random name for the table.
     *
     * @return $this
     */
    public function setRandomName()
    {
        $this->setName(substr(sha1(microtime(true)), 0, 8));

        return $this;
    }

    /**
     * @return AbstractOptions
     */
    public function getOptions(): AbstractOptions
    {
        return $this->options;
    }

    /**
     * @param AbstractOptions $options
     *
     * @return $this
     */
    public function setOptions(AbstractOptions $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return CallbackBuilder
     */
    public function getCallbackBuilder(): CallbackBuilder
    {
        return $this->callbackBuilder;
    }

    /**
     * @param CallbackBuilder $callbackBuilder
     *
     * @return $this
     */
    public function setCallbackBuilder(CallbackBuilder $callbackBuilder): self
    {
        $this->callbackBuilder = $callbackBuilder;

        return $this;
    }

    /**
     * @return StatisticsBuilder
     */
    public function getStatisticsBuilder(): StatisticsBuilder
    {
        return $this->statisticsBuilder;
    }

    /**
     * @param StatisticsBuilder $statisticsBuilder
     *
     * @return $this
     */
    public function setStatisticsBuilder(StatisticsBuilder $statisticsBuilder): self
    {
        $this->statisticsBuilder = $statisticsBuilder;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     *
     * @return $this
     */
    public function setData(?array $data): self
    {
        $event = new DataTableDataSetEvent($this, $data);
        $data = $this->getEventDispatcher()->dispatch(
            DataTableEvents::DATATABLE_DATA_SET,
            $event
        )->getData();

        $this->data = $data;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getStatisticsData(): ?array
    {
        return $this->statisticsData;
    }

    /**
     * @param array|null $statisticsData
     *
     * @return $this
     */
    public function setStatisticsData(?array $statisticsData): self
    {
        $this->statisticsData = $statisticsData;

        return $this;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * @param TranslatorInterface $translator
     *
     * @return $this
     */
    public function setTranslator(TranslatorInterface $translator): self
    {
        $this->translator = $translator;

        return $this;
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig(): Twig_Environment
    {
        return $this->twig;
    }

    /**
     * @param Twig_Environment $twig
     *
     * @return $this
     */
    public function setTwig(Twig_Environment $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return $this
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): self
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }

    /**
     * @return RouterInterface|null
     */
    public function getRouter(): ?RouterInterface
    {
        return $this->router;
    }

    /**
     * @param RouterInterface|null $router
     *
     * @return $this
     */
    public function setRouter(?RouterInterface $router): self
    {
        $this->router = $router;

        return $this;
    }

    /**
     * @return TokenStorageInterface|null
     */
    public function getTokenStorage(): ?TokenStorageInterface
    {
        return $this->tokenStorage;
    }

    /**
     * @param TokenStorageInterface|null $tokenStorage
     *
     * @return $this
     */
    public function setTokenStorage(?TokenStorageInterface $tokenStorage): self
    {
        $this->tokenStorage = $tokenStorage;

        return $this;
    }

    /**
     * @return AuthorizationCheckerInterface|null
     */
    public function getAuthorizationChecker(): ?AuthorizationCheckerInterface
    {
        return $this->authorizationChecker;
    }

    /**
     * @param AuthorizationCheckerInterface|null $authorizationChecker
     *
     * @return $this
     */
    public function setAuthorizationChecker(?AuthorizationCheckerInterface $authorizationChecker): self
    {
        $this->authorizationChecker = $authorizationChecker;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getTemplate(): ?array
    {
        return $this->template;
    }

    /**
     * @param array|null $template
     *
     * @return $this
     */
    public function setTemplate(?array $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getSettings(): ?array
    {
        return $this->settings;
    }

    /**
     * @param array|null $settings
     *
     * @return $this
     */
    public function setSettings(?array $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationDomain(): string
    {
        return $this->translationDomain;
    }

    /**
     * @param string $translationDomain
     *
     * @return $this
     */
    public function setTranslationDomain(string $translationDomain): self
    {
        $this->translationDomain = $translationDomain;

        return $this;
    }

    /**
     * @return string
     */
    public function getTranslationPrefix(): string
    {
        return $this->translationPrefix;
    }

    /**
     * @param string $translationPrefix
     *
     * @return $this
     */
    public function setTranslationPrefix(string $translationPrefix): self
    {
        $this->translationPrefix = $translationPrefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getVariablePage(): string
    {
        return $this->variablePage;
    }

    /**
     * @return string
     */
    public function getVariablePerPage(): string
    {
        return $this->variablePerPage;
    }

    /**
     * @return string
     */
    public function getVariableFilters(): string
    {
        return $this->variableFilters;
    }

    /**
     * @return string
     */
    public function getVariableSorters(): string
    {
        return $this->variableSorters;
    }

    /**
     * @return string
     */
    public function getDefaultRequestMethod(): string
    {
        return $this->defaultRequestMethod;
    }

    /**
     * @return int
     */
    public function getValuePerPage(): int
    {
        return $this->valuePerPage;
    }

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param int|null $count
     *
     * @return $this
     */
    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     *
     * @return $this
     */
    public function setPage(?int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    /**
     * @param int|null $perPage
     *
     * @return $this
     */
    public function setPerPage(?int $perPage): self
    {
        $this->perPage = $perPage;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatisticsClassName(): ?string
    {
        return $this->statisticsClassName;
    }

    /**
     * @param string|null $statisticsClassName
     *
     * @return $this
     */
    public function setStatisticsClassName(?string $statisticsClassName): self
    {
        $this->statisticsClassName = $statisticsClassName;

        return $this;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     *
     * @return $this
     */
    public function setErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }
}
