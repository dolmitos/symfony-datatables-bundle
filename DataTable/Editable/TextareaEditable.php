<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use Symfony\Component\OptionsResolver\OptionsResolver;

class TextareaEditable extends AbstractEditable
{
    /**
     * {@inheritdoc}
     */
    public function getEditor()
    {
        return 'textarea';
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        return $this;
    }

    //-------------------------------------------------
    // Getters && Setters
    //-------------------------------------------------

    /**
     * Get rows.
     *
     * @return int
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Set rows.
     *
     * @param int $rows
     *
     * @return $this
     */
    public function setRows($rows)
    {
        $this->rows = $rows;

        return $this;
    }
}
