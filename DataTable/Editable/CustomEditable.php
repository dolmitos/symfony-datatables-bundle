<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomEditable extends AbstractEditable
{
    /**
     * Editor content
     * Default: false.
     *
     * @var bool|string|CallbackInterface|null
     */
    protected $editor;

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'editor' => false,
        ]);

        $resolver->setAllowedTypes('editor', ['null', 'boolean', 'string', CallbackInterface::class]);

        return $this;
    }

    /**
     * @param bool|CallbackInterface|string|null $editor
     *
     * @return CustomEditable
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|string|null
     */
    public function getEditor()
    {
        return $this->editor;
    }
}
