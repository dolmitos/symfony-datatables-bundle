<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackInterface;
use DolmIT\DataTablesBundle\DataTable\OptionsTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractEditable implements EditableInterface
{
    /*
     * Use the OptionsResolver.
     */
    use OptionsTrait;

    /**
     * Holds parameters for editor.
     *
     * @var array|null
     */
    protected $editorParams;

    /**
     * AbstractEditable constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->initOptions();
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'editor_params' => null,
        ]);

        $resolver->setAllowedTypes('editor_params', ['null', 'array']);

        return $this;
    }

    /**
     * @param array|null $editorParams
     *
     * @return $this
     */
    public function setEditorParams(?array $editorParams)
    {
        $this->editorParams = $editorParams;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEditorParams()
    {
        return $this->editorParams;
    }

    /**
     * Return JSON form of editor params.
     *
     * @return string
     */
    public function getEditorParamsJSON()
    {
        if (!$this->getEditorParams()) {
            return $this->optionToJson([]);
        }

        return $this->optionToJson($this->getEditorParams());
    }

    public function isCallback()
    {
        return $this->getEditor() instanceof CallbackInterface;
    }
}
