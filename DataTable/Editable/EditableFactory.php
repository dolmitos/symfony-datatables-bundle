<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use DolmIT\DataTablesBundle\DataTable\Exception\EditableFactoryException;

/**
 * Class Factory.
 */
class EditableFactory
{
    /**
     * Create.
     *
     * @param string|object $class
     *
     * @return object
     *
     * @throws EditableFactoryException
     */
    public static function create($class)
    {
        if (empty($class) || !\is_string($class) && !$class instanceof EditableInterface) {
            throw new EditableFactoryException('Factory::create(): String or EditableInterface expected.');
        }

        if ($class instanceof EditableInterface) {
            /* @var $class object */
            return $class;
        }

        if (\is_string($class) && class_exists($class)) {
            $instance = new $class();

            if (!$instance instanceof EditableInterface) {
                throw new EditableFactoryException("Factory::create(): Expected $class to be an instance of EditableInterface.");
            } else {
                return $instance;
            }
        } else {
            throw new EditableFactoryException("Factory::create(): $class is not callable.");
        }
    }
}
