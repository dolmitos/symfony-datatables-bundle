<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use Symfony\Component\OptionsResolver\OptionsResolver;

class TextEditable extends AbstractEditable
{
    /**
     * {@inheritdoc}
     */
    public function getEditor()
    {
        return 'input';
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        return $this;
    }
}
