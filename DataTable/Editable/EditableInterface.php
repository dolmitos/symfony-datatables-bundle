<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

interface EditableInterface
{
    /**
     * Return selected editor for editable field.
     *
     * @return mixed
     */
    public function getEditor();

    /**
     * Return possible editor parameters.
     *
     * @return mixed
     */
    public function getEditorParams();
}
