<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectEditable extends AbstractEditable
{
    /**
     * {@inheritdoc}
     */
    public function getEditor()
    {
        return 'select';
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('editor_params');

        return $this;
    }
}
