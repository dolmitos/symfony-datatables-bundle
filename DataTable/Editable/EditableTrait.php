<?php

namespace DolmIT\DataTablesBundle\DataTable\Editable;

use Exception;

trait EditableTrait
{
    /**
     * An EditableInterface instance.
     * Default: null.
     *
     * @var EditableInterface|null
     */
    protected $editable;

    //-------------------------------------------------
    // Getters && Setters
    //-------------------------------------------------

    /**
     * Get editable.
     *
     * @return EditableInterface|null
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set editable.
     *
     * @param array|null $editableClassAndOptions
     *
     * @return $this
     *
     * @throws Exception
     */
    public function setEditable($editableClassAndOptions)
    {
        if (\is_array($editableClassAndOptions)) {
            if (2 != \count($editableClassAndOptions)) {
                throw new Exception('EditableTrait::setEditable(): Two arguments expected.');
            }

            if (!isset($editableClassAndOptions[0]) || !\is_string($editableClassAndOptions[0]) && !$editableClassAndOptions[0] instanceof EditableInterface) {
                throw new Exception('EditableTrait::setEditable(): Set a Editable class.');
            }

            if (!isset($editableClassAndOptions[1]) || !\is_array($editableClassAndOptions[1])) {
                throw new Exception('EditableTrait::setEditable(): Set an options array.');
            }

            $newEditable = EditableFactory::create($editableClassAndOptions[0]);
            $this->editable = $newEditable->set($editableClassAndOptions[1]);
        } else {
            $this->editable = $editableClassAndOptions;
        }

        return $this;
    }
}
