<?php

namespace DolmIT\DataTablesBundle\DataTable;

class AutomaticDataTable extends AbstractDataTable
{
    use DataTableLoaderTrait;

    protected $type = DataTableInterface::TABLE_TYPE_TABULATOR;

    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function buildDataTable(array $options = [])
    {
        $this
            ->loadData($options['data'] ?? null)
            ->loadTemplate($options['template'] ?? null)
            ->loadSettings($options['settings'] ?? null)
        ;
    }
}
