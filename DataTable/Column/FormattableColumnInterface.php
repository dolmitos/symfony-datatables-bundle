<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackInterface;

interface FormattableColumnInterface
{
    /**
     * Return the column identifier.
     *
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * @return string|CallbackInterface|null
     */
    public function getFormatter();

    /**
     * @param string|CallbackInterface|null $formatter
     *
     * @return $this
     */
    public function setFormatter($formatter);

    /**
     * @return CallbackInterface|null
     */
    public function getFormatterParams(): ?CallbackInterface;

    /**
     * @param CallbackInterface|null $formatterParams
     *
     * @return $this
     */
    public function setFormatterParams(?CallbackInterface $formatterParams);

    /**
     * @return string|null
     */
    public function getFormValueQuerySelector(): ?string;

    /**
     * @param string|null $formValueQuerySelector
     *
     * @return $this
     */
    public function setFormValueQuerySelector(?string $formValueQuerySelector);

    /**
     * Get the FormMappingsCallbacks.
     *
     * @return array|null
     */
    public function getFormMappings(): ?array;
}
