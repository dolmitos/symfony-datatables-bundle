<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;

interface ColumnInterface
{
    public function configureOptions(OptionsResolver $resolver);

    /**
     * @return FormFactory|null
     */
    public function getFormFactory(): ?FormFactory;

    /**
     * @param FormFactory $formFactory
     *
     * @return mixed
     */
    public function setFormFactory(FormFactory $formFactory);

    /**
     * Render Cell contents based on Column type.
     *
     * @param array $row
     *
     * @return mixed
     */
    public function renderCellContent(array &$row);

    /**
     * Returns all active methods
     * Meaning they have values set.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getActiveMethods(): array;

    /**
     * Return the column identifier.
     *
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * Check if this object is a group column.
     *
     * @return bool
     */
    public function isGroup(): bool;
}
