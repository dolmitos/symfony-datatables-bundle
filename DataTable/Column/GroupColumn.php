<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupColumn extends AbstractColumn
{
    /**
     * Columns belonging to this group.
     *
     * @var array|AbstractColumn[]|null
     */
    protected $columns;

    /**
     * Check if this object is a group column.
     *
     * @return bool
     */
    public function isGroup(): bool
    {
        return true;
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([]);

        return $this;
    }

    /**
     * @return array|AbstractColumn[]|null
     */
    public function getColumns(): ?array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     *
     * @return GroupColumn
     */
    public function setColumns(array $columns): self
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * Adds a Column.
     *
     * @param AbstractColumn $column
     *
     * @return $this
     */
    public function addColumn(AbstractColumn $column)
    {
        if (!isset($this->columns[$column->getIdentifier()])) {
            $this->columns[$column->getIdentifier()] = $column;
        }

        return $this;
    }
}
