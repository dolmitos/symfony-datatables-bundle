<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use DolmIT\DataTablesBundle\DataTable\Editable\EditableTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Column extends AbstractColumn
{
    use EditableTrait;

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'editable' => null,
        ]);

        return $this;
    }
}
