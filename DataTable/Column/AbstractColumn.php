<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackBuilder;
use DolmIT\DataTablesBundle\DataTable\Callback\CallbackInterface;
use DolmIT\DataTablesBundle\DataTable\Form\Callback\FormMappingCallback;
use DolmIT\DataTablesBundle\DataTable\OptionsTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig_Environment;

/**
 * Class AbstractColumn.
 */
abstract class AbstractColumn implements ColumnInterface, FormattableColumnInterface
{
    /*
     * Use the OptionsResolver.
     */
    use OptionsTrait;

    /**
     * Array of column options that are allowed to be added to the view.
     *
     * @var array
     */
    protected $outputColumnOptions = [];

    /**
     * Identifier key for the group this column would belong to
     * Use null when no groups exist or if this is a group.
     *
     * @var string|null
     */
    protected $groupIdentifier;

    /**
     * Adds class names to each cell in a column.
     * Default: null.
     *
     * @var string|null
     */
    protected $cssClass;

    /**
     * Holds the form mapping option parameters.
     * Each array element is an object of FormMappingCallback.
     *
     * FormMappingCallback is meant to override automatic methods
     * in the sense that if fields are found automatically that
     * match a data value, they can be overwritten with another value using
     * these mappings.
     *
     * There is currently no way to force the system to not use a data value, if the
     * correct identifier is found in the template.
     *
     * Should this ever be required, please submit a feature request.
     *
     * @var array|FormMappingCallback[]|null
     */
    protected $formMappings;

    /**
     * Adds ability to control whether or not the system will
     * override the default contents of the cell.
     * This is useful to switch to false in case of sub-forms,
     * where symfony will attach a div with the actual id of what
     * we would want to override.
     *
     * But also allows to disable anything, if custom formMappings are used.
     *
     * @var bool
     */
    protected $formOverrideDefault = true;

    /**
     * Add a querySelector string for the element containing the
     * value that should be set in the cell value.
     *
     * @var string|null
     */
    protected $formValueQuerySelector;

    /**
     * Set the data identifier for the column from which the values are loaded.
     * Default: Loaded from config.
     *
     * Is set in the ColumnBuilder.
     *
     * @var string
     */
    protected $identifier;

    /**
     * Enable or disable ordering on this column.
     * Default: true.
     *
     * @var bool
     */
    protected $headerSort;

    /**
     * Enable or disable filter within header.
     *
     * Default false
     *
     * @var bool|string|null
     */
    protected $headerFilter;

    /**
     * @var CallbackInterface|null
     */
    protected $headerFilterParams;

    /**
     * Filter editor.
     *
     * @var string|null
     */
    protected $editor;

    /**
     * Optional parameters for editor.
     *
     * @var CallbackInterface|null
     */
    protected $editorParams;

    /**
     * Order direction application sequence.
     * Default: ['asc', 'desc'].
     *
     * @var array
     */
    protected $orderSequence;

    /**
     * Set the column title.
     *
     * @var string
     */
    protected $title;

    /**
     * Set the column title translation parameters.
     *
     * @var array|null
     */
    protected $titleTranslationParameters;

    /**
     * Set column value alignment
     * Default: null
     * Tabulator default: left.
     *
     * @var string|null
     */
    protected $align;

    /**
     * Enable or disable the display of this column.
     * Default: true.
     *
     * @var bool
     */
    protected $visible;

    /**
     * Column width in px.
     * Default: null.
     *
     * @var string|null
     */
    protected $width;

    /**
     * Column min width in px.
     * Default: null.
     *
     * @var string|null
     */
    protected $minWidth;

    /**
     * Column width shrinking.
     * Default: null.
     *
     * @var string|null
     */
    protected $widthShrink;

    /**
     * Column width growing.
     * Default: null.
     *
     * @var string|null
     */
    protected $widthGrow;

    /**
     * Set column resizable
     * Default: null.
     *
     * @var bool|null
     */
    protected $resizable;

    /**
     * Set column formatter
     * Default: null (plaintext).
     *
     * @var string|CallbackInterface|null
     */
    protected $formatter;

    /**
     * Responsivity level if responsiveLayout is active in Tabulator
     * Default: null.
     *
     * @var integer|null
     */
    protected $responsive;

    /**
     * Set column formatter params
     * Default: null (plaintext).
     *
     * @var CallbackInterface|null
     */
    protected $formatterParams;

    /**
     * Set the column as a rowhandle.
     *
     * @var bool
     */
    protected $rowHandle = false;

    /**
     * The Twig Environment to render Twig templates in Column rows.
     * Is set in the ColumnBuilder.
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * The name of the associated DataTable.
     * Is set in the ColumnBuilder.
     *
     * @var string
     */
    protected $datatableName;

    /**
     * @var CallbackBuilder
     */
    protected $callbackBuilder;

    /**
     * @var FormFactory
     */
    protected $formFactory;

    /**
     * Options constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->initOptions();
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['identifier']);

        $resolver->setDefaults([
            'css_class' => null,
            'headerSort' => true,
            'headerFilter' => false,
            'headerFilterParams' => null,
            'order_sequence' => ['asc', 'desc'],
            'title' => null,
            'title_translation_parameters' => null,
            'align' => null,
            'visible' => true,
            'width' => null,
            'min_width' => null,
            'width_shrink' => null,
            'width_grow' => null,
            'resizable' => null,
            'formatter' => null,
            'responsive' => null,
            'formatterParams' => null,
            'rowHandle' => false,
            'editor' => null,
            'editor_params' => null,
            'form_mappings' => null,
            'form_override_default' => true,
            'form_value_query_selector' => null,
            'group_identifier' => null,
        ]);

        $resolver->setAllowedTypes('css_class', ['null', 'string']);
        $resolver->setAllowedTypes('headerSort', 'bool');
        $resolver->setAllowedTypes('headerFilter', ['null', 'bool', 'string']);
        $resolver->setAllowedTypes('headerFilterParams', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('order_sequence', ['null', 'array']);
        $resolver->setAllowedTypes('title', ['null', 'string']);
        $resolver->setAllowedTypes('title_translation_parameters', ['null', 'array']);
        $resolver->setAllowedTypes('align', ['null', 'string']);
        $resolver->setAllowedTypes('visible', 'bool');
        $resolver->setAllowedTypes('width', ['null', 'string']);
        $resolver->setAllowedTypes('min_width', ['null', 'string']);
        $resolver->setAllowedTypes('width_shrink', ['null', 'string']);
        $resolver->setAllowedTypes('width_grow', ['null', 'string']);
        $resolver->setAllowedTypes('resizable', ['null', 'boolean']);
        $resolver->setAllowedTypes('formatter', ['null', 'string', CallbackInterface::class]);
        $resolver->setAllowedTypes('responsive', ['null', 'integer']);
        $resolver->setAllowedTypes('formatterParams', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('rowHandle', ['bool']);
        $resolver->setAllowedTypes('editor', ['null', 'string']);
        $resolver->setAllowedTypes('editor_params', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('form_mappings', ['null', 'array']);
        $resolver->setAllowedTypes('form_override_default', ['bool']);
        $resolver->setAllowedTypes('form_value_query_selector', ['string', 'null']);
        $resolver->setAllowedTypes('group_identifier', ['string', 'null']);

        return $this;
    }

    /**
     * @param array $row
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function renderCellContent(array &$row)
    {
    }

    /**
     * Return Getter functions that should not be used
     * in automatic method handling.
     *
     * @return array
     */
    protected function getDiscardedGetters()
    {
        return [
            'getDiscardedGetters',
            'getActiveMethods',
            'getAllMethods',
            'getTwig',
            'getFormFactory',
            'getCallbackBuilder',
            'getOptions',
            'getOrderSequence',
            'getTitle',
            'getIdentifier',
            'getTitleTranslationParameters',
        ];
    }

    private function snakeCaseToCamelCase(string $input, $capitalizeFirstSymbol = false): string
    {
        $str = str_replace('_', '', ucwords($input, '_'));

        if (!$capitalizeFirstSymbol) {
            $str = lcfirst($str);
        }

        return $str;
    }

    protected function getOutputColumnOptions()
    {
        return array_reduce($this->outputColumnOptions, function ($array, $item) {
            $item = $this->snakeCaseToCamelCase($item, true);
            $is = 'is'.$item;
            $get = 'get'.$item;

            return array_merge($array, [$is, $get]);
        }, []);
    }

    /**
     * Get class specific "get" methods from $this.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    protected function getAllMethods()
    {
        $reflection = new \ReflectionClass($this);
        $methods = [];
        foreach ($reflection->getMethods() as $method) {
            if (
                (
                    self::class == $method->class ||
                    Column::class == $method->class ||
                    \in_array($method->name, $this->getOutputColumnOptions())
                )
                    &&
                (
                    false !== strpos($method->name, 'get') && 0 == strpos($method->name, 'get') ||
                    false !== strpos($method->name, 'is') && 0 == strpos($method->name, 'is')
                )
                    &&
                (
                    !\in_array($method->name, $this->getDiscardedGetters())
                )
            ) {
                $methods[] = $method->name;
            }
        }

        return $methods;
    }

    /**
     * Returns all active methods
     * Meaning they have a result.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getActiveMethods(): array
    {
        $methods = $this->getAllMethods();

        $result = array_map(
            function ($method) {
                $result = $this->{$method}();

                return [$method => $result];
            },
            array_filter(
                $methods,
                function ($method) {
                    return $this->{$method}() !== null;
                }
            )
        );
        $return = [];
        foreach ($result as $k => $item) {
            if (false !== strpos(key($item), 'is')) {
                $key = lcfirst(ltrim(key($item), 'is'));
            } elseif (false !== strpos(key($item), 'get')) {
                $key = lcfirst(ltrim(key($item), 'get'));
            }
            $return[$key] = current($item);
        }

        return $return;
    }

    /**
     * @return FormFactory
     */
    public function getFormFactory(): FormFactory
    {
        return $this->formFactory;
    }

    /**
     * @param FormFactory $formFactory
     *
     * @return $this
     */
    public function setFormFactory(FormFactory $formFactory): self
    {
        $this->formFactory = $formFactory;

        return $this;
    }

    /**
     * Check if this object is a group column.
     *
     * @return bool
     */
    public function isGroup(): bool
    {
        return false;
    }

    //-------------------------------------------------
    // Getters & Setters
    //-------------------------------------------------

    /**
     * @return string|null
     */
    public function getGroupIdentifier(): ?string
    {
        return $this->groupIdentifier;
    }

    /**
     * @param string|null $groupIdentifier
     *
     * @return $this
     */
    public function setGroupIdentifier(?string $groupIdentifier): self
    {
        $this->groupIdentifier = $groupIdentifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHeaderSort(): bool
    {
        return $this->headerSort;
    }

    /**
     * @param bool $headerSort
     *
     * @return $this
     */
    public function setHeaderSort(bool $headerSort): self
    {
        $this->headerSort = $headerSort;

        return $this;
    }

    /**
     * @return array
     */
    public function getOrderSequence(): array
    {
        return $this->orderSequence;
    }

    /**
     * @param array $orderSequence
     *
     * @return $this
     */
    public function setOrderSequence(array $orderSequence): self
    {
        $this->orderSequence = $orderSequence;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getTitleTranslationParameters(): ?array
    {
        return $this->titleTranslationParameters;
    }

    /**
     * @param array|null $titleTranslationParameters
     *
     * @return $this
     */
    public function setTitleTranslationParameters(?array $titleTranslationParameters): self
    {
        $this->titleTranslationParameters = $titleTranslationParameters;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     *
     * @return $this
     */
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWidth(): ?string
    {
        return $this->width;
    }

    /**
     * @param string|null $width
     *
     * @return $this
     */
    public function setWidth(?string $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWidthShrink(): ?string
    {
        return $this->widthShrink;
    }

    /**
     * @param string|null $widthShrink
     *
     * @return $this
     */
    public function setWidthShrink(?string $widthShrink): self
    {
        $this->widthShrink = $widthShrink;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getWidthGrow(): ?string
    {
        return $this->widthGrow;
    }

    /**
     * @param string|null $widthGrow
     *
     * @return $this
     */
    public function setWidthGrow(?string $widthGrow): self
    {
        $this->widthGrow = $widthGrow;

        return $this;
    }

    /**
     * Get Twig.
     *
     * @return Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Set Twig.
     *
     * @param Twig_Environment $twig
     *
     * @return $this
     */
    public function setTwig(Twig_Environment $twig): self
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get datatableName.
     *
     * @return string
     */
    public function getDataTableName()
    {
        return $this->datatableName;
    }

    /**
     * Set datatableName.
     *
     * @param string $datatableName
     *
     * @return $this
     */
    public function setDataTableName($datatableName): self
    {
        $this->datatableName = $datatableName;

        return $this;
    }

    /**
     * @param string|null $cssClass
     *
     * @return $this
     */
    public function setCssClass(?string $cssClass): self
    {
        $this->cssClass = $cssClass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCssClass(): ?string
    {
        return $this->cssClass;
    }

    /**
     * @param bool|null $resizable
     *
     * @return $this
     */
    public function setResizable(?bool $resizable): self
    {
        $this->resizable = $resizable;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getResizable(): ?bool
    {
        return $this->resizable;
    }

    /**
     * @return string|CallbackInterface|null
     */
    public function getFormatter()
    {
        return $this->formatter;
    }

    /**
     * @param string|CallbackInterface|null $formatter
     *
     * @return $this
     */
    public function setFormatter($formatter): self
    {
        $this->formatter = $formatter;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getResponsive(): ?int
    {
        return $this->responsive;
    }

    /**
     * @param int|null $responsive
     *
     * @return $this
     */
    public function setResponsive(?int $responsive): self
    {
        $this->responsive = $responsive;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getFormatterParams(): ?CallbackInterface
    {
        return $this->formatterParams;
    }

    /**
     * @param CallbackInterface|null $formatterParams
     *
     * @return $this
     */
    public function setFormatterParams(?CallbackInterface $formatterParams): self
    {
        $this->formatterParams = $formatterParams;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRowHandle(): bool
    {
        return $this->rowHandle;
    }

    /**
     * @param bool $rowHandle
     *
     * @return $this
     */
    public function setRowHandle(bool $rowHandle): self
    {
        $this->rowHandle = $rowHandle;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMinWidth(): ?string
    {
        return $this->minWidth;
    }

    /**
     * @param string|null $minWidth
     *
     * @return $this
     */
    public function setMinWidth(?string $minWidth): self
    {
        $this->minWidth = $minWidth;

        return $this;
    }

    /**
     * @return CallbackBuilder
     */
    public function getCallbackBuilder(): CallbackBuilder
    {
        return $this->callbackBuilder;
    }

    /**
     * @param CallbackBuilder $callbackBuilder
     *
     * @return $this
     */
    public function setCallbackBuilder(CallbackBuilder $callbackBuilder): self
    {
        $this->callbackBuilder = $callbackBuilder;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getHeaderFilter()
    {
        return $this->headerFilter;
    }

    /**
     * @param bool|string|null $headerFilter
     *
     * @return $this
     */
    public function setHeaderFilter($headerFilter): self
    {
        $this->headerFilter = $headerFilter;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderFilterParams(): ?CallbackInterface
    {
        return $this->headerFilterParams;
    }

    /**
     * @param CallbackInterface|null $headerFilterParams
     *
     * @return $this
     */
    public function setHeaderFilterParams(?CallbackInterface $headerFilterParams): self
    {
        $this->headerFilterParams = $headerFilterParams;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEditor(): ?string
    {
        return $this->editor;
    }

    /**
     * @param string|null $editor
     *
     * @return $this
     */
    public function setEditor(?string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getEditorParams(): ?CallbackInterface
    {
        return $this->editorParams;
    }

    /**
     * @param CallbackInterface|null $editorParams
     *
     * @return $this
     */
    public function setEditorParams(?CallbackInterface $editorParams): self
    {
        $this->editorParams = $editorParams;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlign(): ?string
    {
        return $this->align;
    }

    /**
     * @param string|null $align
     *
     * @return $this
     */
    public function setAlign(?string $align): self
    {
        $this->align = $align;

        return $this;
    }

    /**
     * @return array|FormMappingCallback[]|null
     */
    public function getFormMappings(): ?array
    {
        return $this->formMappings;
    }

    /**
     * @param array|FormMappingCallback[]|null $formMappings
     *
     * @return $this
     */
    public function setFormMappings(?array $formMappings): self
    {
        $this->formMappings = $formMappings;

        return $this;
    }

    /**
     * @return bool
     */
    public function isFormOverrideDefault(): bool
    {
        return $this->formOverrideDefault;
    }

    /**
     * @param bool $formOverrideDefault
     *
     * @return $this
     */
    public function setFormOverrideDefault(bool $formOverrideDefault): self
    {
        $this->formOverrideDefault = $formOverrideDefault;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormValueQuerySelector(): ?string
    {
        return $this->formValueQuerySelector;
    }

    /**
     * @param string|null $formValueQuerySelector
     *
     * @return $this
     */
    public function setFormValueQuerySelector(?string $formValueQuerySelector): self
    {
        $this->formValueQuerySelector = $formValueQuerySelector;

        return $this;
    }
}
