<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackBuilder;
use Exception;
use Symfony\Component\Form\FormFactory;
use Twig_Environment;

/**
 * Class ColumnBuilder.
 */
class ColumnBuilder
{
    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    private $twig;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * The name of the associated DataTable.
     *
     * @var string
     */
    private $datatableName;

    /**
     * The generated Columns.
     *
     * @var array
     */
    private $columns;

    /**
     * ColumnBuilder constructor.
     *
     * @param Twig_Environment $twig
     * @param string           $datatableName
     */
    public function __construct(Twig_Environment $twig, FormFactory $formFactory, $datatableName)
    {
        $this->twig = $twig;
        $this->formFactory = $formFactory;
        $this->datatableName = $datatableName;

        $this->columns = [];
    }

    //-------------------------------------------------
    // Builder
    //-------------------------------------------------

    /**
     * Add Column.
     *
     * @param string|null            $identifier
     * @param string|ColumnInterface $class
     * @param array                  $options
     *
     * @return $this
     *
     * @throws Exception
     */
    public function add(?string $identifier, $class, array $options = [])
    {
        /* @var $column AbstractColumn */
        $column = ColumnFactory::create($class);
        $column->initOptions();

        $this->setEnvironmentProperties($column);
        $column->set($options);

        $this->addColumn($identifier, $column);

        return $this;
    }

    /**
     * Add Column Group.
     *
     * @param string|null            $identifier
     * @param string|ColumnInterface $class
     * @param array                  $options
     *
     * @return $this
     *
     * @throws Exception
     */
    public function addGroup(?string $identifier, array $options = [])
    {
        /* @var $column AbstractColumn */
        $column = ColumnFactory::create(GroupColumn::class);
        $column->initOptions();

        $this->setEnvironmentProperties($column);
        $column->set($options);

        $this->addColumn($identifier, $column);

        return $this;
    }

    //-------------------------------------------------
    // Getters && Setters
    //-------------------------------------------------

    /**
     * Get columns.
     *
     * @return array|ColumnInterface[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    //-------------------------------------------------
    // Helper
    //-------------------------------------------------

    /**
     * Set environment properties.
     *
     * @param AbstractColumn $column
     *
     * @return $this
     */
    private function setEnvironmentProperties(AbstractColumn $column)
    {
        $column->setDataTableName($this->datatableName);
        $column->setTwig($this->twig);
        $column->setFormFactory($this->formFactory);
        $column->setCallbackBuilder(new CallbackBuilder($this->twig, $this->datatableName));

        return $this;
    }

    /**
     * Adds a Column.
     *
     * @param string|null    $identifier
     * @param AbstractColumn $column
     *
     * @return $this
     */
    private function addColumn(?string $identifier, AbstractColumn $column)
    {
        $this->columns[$identifier] = $column;
        $column->setIdentifier($identifier);

        if ($column->isGroup()) {
            foreach ($this->getColumns() as $ident => $col) {
                if (!$col->isGroup() && $col->getGroupIdentifier() == $identifier) {
                    $column->addColumn($col);
                    unset($this->columns[$ident]);
                }
            }
        } else {
            if ($column->getGroupIdentifier()) {
                foreach ($this->getColumns() as $ident => $col) {
                    if ($col->isGroup() && $ident == $column->getGroupIdentifier()) {
                        $col->addColumn($column);
                        unset($this->columns[$column->getIdentifier()]);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Obtain real columns mainly for cell management, disregarding grouping
     *
     * @return array
     */
    public function getRealColumns()
    {
        $realColumns = [];
        foreach ($this->getColumns() as $column) {
            if ($column->isGroup() && $column instanceof GroupColumn && $column->getColumns()) {
                foreach ($column->getColumns() as $col) {
                    $realColumns[] = $col;
                }
            } else {
                $realColumns[] = $column;
            }
        }

        return $realColumns;
    }
}
