<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use DolmIT\DataTablesBundle\DataTable\Exception\ColumnFactoryException;

/**
 * Class Factory.
 */
class ColumnFactory
{
    /**
     * Create.
     *
     * @param string|object $class
     *
     * @return object
     *
     * @throws ColumnFactoryException
     */
    public static function create($class)
    {
        if (empty($class) || !\is_string($class) && !$class instanceof ColumnInterface) {
            throw new ColumnFactoryException('Factory::create(): String or ColumnInterface expected.');
        }

        if ($class instanceof ColumnInterface) {
            /* @var $class object */
            return $class;
        }

        if (\is_string($class) && class_exists($class)) {
            $instance = new $class();

            if (!$instance instanceof ColumnInterface) {
                throw new ColumnFactoryException("Factory::create(): Expected $class to be an instance of ColumnInterface.");
            } else {
                return $instance;
            }
        } else {
            throw new ColumnFactoryException("Factory::create(): $class is not callable.");
        }
    }
}
