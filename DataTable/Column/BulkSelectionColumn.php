<?php

namespace DolmIT\DataTablesBundle\DataTable\Column;

use DolmIT\DataTablesBundle\DataTable\Callback\Callback;
use DolmIT\DataTablesBundle\DataTable\Editable\EditableTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BulkSelectionColumn extends Column
{
    use EditableTrait;

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $column = $this;

        $bulkColumnFormatterCallback = $this->getCallbackBuilder()->build(Callback::class, [
            'template' => '@DolmITDataTables/callbacks/formatters/bulk_column_formatter_callback.js.twig',
            'variables' => [
                'datatable_object' => $this,
            ],
        ]);

        $resolver->setDefaults([
            'title' => '<input type="checkbox" onclick="DataTables.bulkSelect.selectAll(this);"/>',
            'width' => '40',
            'headerSort' => false,
            'formatter' => $bulkColumnFormatterCallback,
        ]);

        return $this;
    }
}
