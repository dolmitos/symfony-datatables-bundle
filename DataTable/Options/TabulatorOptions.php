<?php

namespace DolmIT\DataTablesBundle\DataTable\Options;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackInterface;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TabulatorOptions extends AbstractOptions
{
    /**
     * Currently responsible for allowing or disallowing the
     * combination of a custom filter with Tabulator.
     *
     * When set to true, will attach the custom filter to tabulator functionality.
     * When set to false, jQuery.ajax will be used to handle filters instead (default).
     *
     * Please note that even if the custom filter is used, ajaxURL must be set.
     *
     * @var bool
     */
    protected $ajaxFilterOverride;

    //-------------------------------------------------
    // DataTable Editable options
    //-------------------------------------------------

    /**
     * @var bool
     */
    protected $defaultCellStateEditable = false;

    /**
     * @var string
     */
    protected $editableKey = 'edit';

    //-------------------------------------------------
    // Table dynamic height options
    //-------------------------------------------------

    /**
     * Set the maximum possible height from any dynamic height setting.
     *
     * Value must be in pixels
     *
     * @var int|null
     */
    protected $dynamicHeightMax = null;

    /**
     * Set the minimum possible height from any dynamic height setting.
     *
     * Value must be in pixels
     *
     * @var int|null
     */
    protected $dynamicHeightMin = null;

    /**
     * Boolean to enable height based on rows.
     * DataTable height will be set to accomodate (roughly)
     * the amount of rows set in heightRowCount.
     *
     * Cannot be used together with dynamicHeightTarget
     *
     * Default false
     *
     * @var bool|null
     */
    protected $dynamicHeightRows = false;

    /**
     * Amount of rows to be shown in the table
     * when heightRowDynamic is active.
     *
     * Default 10
     *
     * @var int|null
     */
    protected $dynamicHeightRowCount = 10;

    /**
     * Valid JS selector for the target to fill the height of up to
     * the percentage set in dynamicHeightPercentage.
     *
     * Cannot be used together with dynamicHeightRows
     *
     * @var string|null
     */
    protected $dynamicHeightTarget;

    /**
     * Used with dynamicHeightTarget and sets the percent value of the
     * target element to match.
     *
     * Default 90
     *
     * @var int|null
     */
    protected $dynamicHeightTargetPercentage = 90;

    /**
     * Used with dynamicHeightTarget and sets the offset
     * value to remove from full height.
     * Used when some extra elements are known to exist
     * within the target container that the automatic
     * calculation wouldn't be aware of.
     *
     * Default 0
     *
     * @var int|null
     */
    protected $dynamicHeightTargetOffset = 0;

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'ajax_filter_override' => false,
            'height' => null,
            'virtual_dom' => null,
            'virtual_dom_buffer' => null,
            'placeholder' => null,
            'tooltips' => null,
            'tooltip_generation_mode' => null,
            'history' => null,
            'keybindings' => null,
            'locale' => null,
            'langs' => null,
            'download_data_formatter' => null,
            'layout' => null,
            'layout_columns_on_new_data' => null,
            'responsive_layout' => null,
            'responsive_layout_collapse_start_open' => null,
            'responsive_layout_collapse_use_formatters' => null,
            'responsive_layout_collapse_formatter' => null,
            'column_min_width' => null,
            'resizable_columns' => null,
            'movable_columns' => null,
            'tooltips_header' => null,
            'column_vert_align' => null,
            'add_row_pos' => null,
            'selectable' => null,
            'selectable_rolling_selection' => null,
            'selectable_persistence' => null,
            'movable_rows' => null,
            'movable_rows_connected_tables' => null,
            'movable_rows_sender' => null,
            'movable_rows_receiver' => null,
            'resizable_rows' => null,
            'scroll_to_row_position' => null,
            'scroll_to_row_if_visible' => null,
            'ajax_u_r_l' => null,
            'ajax_params' => null,
            'ajax_config' => null,
            'ajax_filtering' => null,
            'ajax_sorting' => null,
            'ajax_progressive_load' => null,
            'ajax_progressive_load_delay' => null,
            'ajax_progressive_load_scroll_margin' => null,
            'ajax_loader' => null,
            'ajax_loader_loading' => null,
            'ajax_loader_error' => null,
            'initial_sort' => null,
            'group_by' => null,
            'group_header' => null,
            'group_start_open' => null,
            'group_toggle_element' => null,
            'pagination' => null,
            'pagination_size' => null,
            'pagination_element' => null,
            'pagination_data_received' => null,
            'pagination_data_sent' => null,
            'paginator' => null,
            'pagination_add_row' => null,
            'table_building' => null,
            'table_built' => null,
            'column_moved' => null,
            'column_resized' => null,
            'column_visibility_changed' => null,
            'column_title_changed' => null,
            'row_click' => null,
            'row_dbl_click' => null,
            'row_context' => null,
            'row_tap' => null,
            'row_dbl_tap' => null,
            'row_tap_hold' => null,
            'row_added' => null,
            'row_updated' => null,
            'row_deleted' => null,
            'row_moved' => null,
            'row_resized' => null,
            'header_tap_hold' => null,
            'header_dbl_tap' => null,
            'header_tap' => null,
            'header_context' => null,
            'header_dbl_click' => null,
            'header_click' => null,
            'cell_click' => null,
            'cell_dbl_click' => null,
            'cell_context' => null,
            'cell_tap' => null,
            'cell_dbl_tap' => null,
            'cell_tap_hold' => null,
            'cell_editing' => null,
            'cell_edit_cancelled' => null,
            'cell_edited' => null,
            'data_loading' => null,
            'data_loaded' => null,
            'data_loaded_passthrough' => null,
            'data_edited' => null,
            'html_importing' => null,
            'html_imported' => null,
            'ajax_requesting' => null,
            'ajax_response' => null,
            'ajax_response_passthrough' => null,
            'ajax_error' => null,
            'data_filtering' => null,
            'data_filtered' => null,
            'data_sorting' => null,
            'data_sorted' => null,
            'render_started' => null,
            'render_started_passthrough' => null,
            'render_complete' => null,
            'page_loaded' => null,
            'localized' => null,
            'data_grouping' => null,
            'data_grouped' => null,
            'group_visibility_changed' => null,
            'group_click' => null,
            'group_dbl_click' => null,
            'group_context' => null,
            'group_tap' => null,
            'group_dbl_tap' => null,
            'group_tap_hold' => null,
            'row_selected' => null,
            'row_deselected' => null,
            'row_selection_changed' => null,
            'movable_rows_sending_start' => null,
            'movable_rows_sent' => null,
            'movable_rows_sent_failed' => null,
            'movable_rows_sending_stop' => null,
            'movable_rows_receiving_start' => null,
            'movable_rows_received' => null,
            'movable_rows_received_failed' => null,
            'movable_rows_receiving_stop' => null,
            'validation_failed' => null,
            'history_undo' => null,
            'history_redo' => null,
            'clipboard_copied' => null,
            'clipboard_pasted' => null,
            'clipboard_paste_error' => null,
            'download_ready' => null,
            'download_complete' => null,

            // DataTable editable options
            'default_cell_state_editable' => false,
            'editable_key' => 'edit',

            // Dynamic height
            'dynamic_height_max' => null,
            'dynamic_height_min' => null,
            'dynamic_height_rows' => false,
            'dynamic_height_row_count' => 10,
            'dynamic_height_target' => null,
            'dynamic_height_target_percentage' => 90,
            'dynamic_height_target_offset' => 0,

            // VERISON 4.1 ADDITION
            'data_tree' => null,
            'sort_order_reverse' => null,
            'column_header_sort_multi' => null,
            'selectable_range_mode' => null,
            'nested_field_separator' => false,

            // VERISON 4.4 ADDITION
            'invalid_option_warnings' => false,
        ]);

        $resolver->setAllowedTypes('ajax_filter_override', ['boolean']);
        $resolver->setAllowedTypes('height', ['string', 'integer', 'boolean', 'null']);
        $resolver->setAllowedTypes('virtual_dom', ['boolean', 'null']);
        $resolver->setAllowedTypes('virtual_dom_buffer', ['integer', 'null']);
        $resolver->setAllowedTypes('placeholder', ['string', 'null']);
        $resolver->setAllowedTypes('tooltips', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('tooltip_generation_mode', ['string', 'null']);
        $resolver->setAllowedTypes('history', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('keybindings', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('locale', ['string', 'boolean', 'null']);
        $resolver->setAllowedTypes('langs', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('download_data_formatter', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('layout', ['string', 'null']);
        $resolver->setAllowedTypes('layout_columns_on_new_data', ['boolean', 'null']);
        $resolver->setAllowedTypes('responsive_layout', ['boolean', 'null', 'string']);
        $resolver->setAllowedTypes('responsive_layout_collapse_start_open', ['boolean', 'null']);
        $resolver->setAllowedTypes('responsive_layout_collapse_use_formatters', ['boolean', 'null']);
        $resolver->setAllowedTypes('responsive_layout_collapse_formatter', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('column_min_width', ['string', 'null']);
        $resolver->setAllowedTypes('resizable_columns', ['boolean', 'null']);
        $resolver->setAllowedTypes('movable_columns', ['boolean', 'null']);
        $resolver->setAllowedTypes('tooltips_header', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('column_vert_align', ['string', 'null']);
        $resolver->setAllowedTypes('add_row_pos', ['string', 'null']);
        $resolver->setAllowedTypes('selectable', ['boolean', 'string', 'null']);
        $resolver->setAllowedTypes('selectable_rolling_selection', ['boolean', 'null']);
        $resolver->setAllowedTypes('selectable_persistence', ['boolean', 'null']);
        $resolver->setAllowedTypes('movable_rows', ['boolean', 'null']);
        $resolver->setAllowedTypes('movable_rows_connected_tables', ['string', 'null']);
        $resolver->setAllowedTypes('movable_rows_sender', ['string', CallbackInterface::class, 'boolean', 'null']);
        $resolver->setAllowedTypes('movable_rows_receiver', ['string', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('resizable_rows', ['boolean', 'null']);
        $resolver->setAllowedTypes('scroll_to_row_position', ['string', 'null']);
        $resolver->setAllowedTypes('scroll_to_row_if_visible', ['boolean', 'null']);
        $resolver->setAllowedTypes('ajax_u_r_l', ['string', 'boolean', 'null']);
        $resolver->setAllowedTypes('ajax_params', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_config', ['string', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_filtering', ['boolean', 'null']);
        $resolver->setAllowedTypes('ajax_sorting', ['boolean', 'null']);
        $resolver->setAllowedTypes('ajax_progressive_load', ['string', 'boolean', 'null']);
        $resolver->setAllowedTypes('ajax_progressive_load_delay', ['integer', 'null']);
        $resolver->setAllowedTypes('ajax_progressive_load_scroll_margin', ['integer', 'boolean', 'null']);
        $resolver->setAllowedTypes('ajax_loader', ['boolean', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_loader_loading', ['string', 'null']);
        $resolver->setAllowedTypes('ajax_loader_error', ['string', 'null']);
        $resolver->setAllowedTypes('initial_sort', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_by', ['string', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_header', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_start_open', [CallbackInterface::class, 'boolean', 'null']);
        $resolver->setAllowedTypes('group_toggle_element', ['string', 'boolean', 'null']);
        $resolver->setAllowedTypes('pagination', ['boolean', 'string', 'null']);
        $resolver->setAllowedTypes('pagination_size', ['integer', 'null']);
        $resolver->setAllowedTypes('pagination_element', ['string', CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('pagination_data_received', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('pagination_data_sent', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('paginator', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('pagination_add_row', ['string', 'null']);
        $resolver->setAllowedTypes('table_building', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('table_built', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('column_moved', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('column_resized', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('column_visibility_changed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('column_title_changed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_dbl_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_context', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_dbl_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_tap_hold', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_added', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_updated', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_deleted', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_moved', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_resized', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('header_tap_hold', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('header_dbl_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('header_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('header_context', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('header_dbl_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('header_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_dbl_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_context', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_dbl_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_tap_hold', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_editing', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_edit_cancelled', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('cell_edited', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_loading', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_loaded', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_loaded_passthrough', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_edited', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('html_importing', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('html_imported', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_requesting', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_response', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_response_passthrough', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('ajax_error', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_filtering', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_filtered', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_sorting', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_sorted', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('render_started', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('render_started_passthrough', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('render_complete', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('page_loaded', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('localized', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_grouping', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('data_grouped', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_visibility_changed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_dbl_click', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_context', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_dbl_tap', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('group_tap_hold', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_selected', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_deselected', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('row_selection_changed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_sending_start', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_sent', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_sent_failed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_sending_stop', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_receiving_start', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_received', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_received_failed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('movable_rows_receiving_stop', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('validation_failed', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('history_undo', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('history_redo', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('clipboard_copied', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('clipboard_pasted', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('clipboard_paste_error', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('download_ready', [CallbackInterface::class, 'null']);
        $resolver->setAllowedTypes('download_complete', [CallbackInterface::class, 'null']);

        // DataTable editable options
        $resolver->setAllowedTypes('default_cell_state_editable', ['bool']);
        $resolver->setAllowedTypes('editable_key', ['string']);

        // Dynamic height
        $resolver->setAllowedTypes('dynamic_height_max', ['null', 'integer']);
        $resolver->setAllowedTypes('dynamic_height_min', ['null', 'integer']);
        $resolver->setAllowedTypes('dynamic_height_rows', ['null', 'boolean']);
        $resolver->setAllowedTypes('dynamic_height_row_count', ['null', 'integer']);
        $resolver->setAllowedTypes('dynamic_height_target', ['null', 'string']);
        $resolver->setAllowedTypes('dynamic_height_target_percentage', ['null', 'integer']);
        $resolver->setAllowedTypes('dynamic_height_target_offset', ['null', 'integer']);

        // VERISON 4.1 ADDITION
        $resolver->setAllowedTypes('data_tree', ['boolean', 'null']);
        $resolver->setAllowedTypes('sort_order_reverse', ['boolean', 'null']);
        $resolver->setAllowedTypes('column_header_sort_multi', ['boolean', 'null']);
        $resolver->setAllowedTypes('selectable_range_mode', ['string', 'null']);
        $resolver->setAllowedTypes('nested_field_separator', ['string', 'null', 'boolean']);

        // VERISON 4.4 ADDITION
        $resolver->setAllowedTypes('invalid_option_warnings', ['null', 'boolean']);

        // Conditionals
        $resolver->setNormalizer('ajax_progressive_load', function (Options $options, $ajaxProgressiveLoad) {
            if (null !== $options['pagination'] && null !== $ajaxProgressiveLoad) {
                throw new InvalidOptionsException('\'pagination\' cannot be set at the same time as \'ajax_progressive_load\', this is a limitation with Tabulator.');
            }

            return $ajaxProgressiveLoad;
        });

        $resolver->setNormalizer('dynamic_height_target', function (Options $options, $dynamicHeightTarget) {
            if (null !== $options['dynamic_height_rows'] && false !== $options['dynamic_height_rows'] && null !== $dynamicHeightTarget && false !== $dynamicHeightTarget) {
                throw new InvalidOptionsException('\'dynamic_height_rows\' cannot be set at the same time as \'dynamic_height_target\'.');
            }

            return $dynamicHeightTarget;
        });

        return $this;
    }

    //-------------------------------------------------
    // VERSION 4.4 ADDITION
    //-------------------------------------------------

    /**
     * Setting the invalidOptionWarnings option to false will disable console warning messages
     * for invalid properties in the table constructor and column definition objects.
     *
     * Tabulator Default: true
     * Library Default: false
     *
     * @var bool|null
     */
    protected $invalidOptionWarnings;

    //-------------------------------------------------
    // VERSION 4.1 ADDITION
    //-------------------------------------------------

    /**
     * Enable DataTree layout.
     *
     * Tabulator Default false
     *
     * @var bool|null
     */
    protected $dataTree;

    /**
     * Ability to reverse sort ordering.
     *
     * Tabulator Default FALSE
     *
     * @var bool|null
     */
    protected $sortOrderReverse;

    /**
     * Add ability to switch off multi row sort.
     *
     * Tabulator Default TRUE
     *
     * @var bool|null
     */
    protected $columnHeaderSortMulti;

    /**
     * Activate ability to select range by setting this value to "click"
     * Holding down shift while clicking on a row will activate the entire selection.
     *
     * Tabulator Default null
     *
     * @var string|null
     */
    protected $selectableRangeMode;

    /**
     * This default differs from Tabulator due to how dots are used in doctrine
     * and might be included in source data. Name separation with pipe makes more sense.
     * Currently we disable this option by default.
     *
     * Library default false
     * Tabulator Default "."
     *
     * @var string|false|null
     */
    protected $nestedFieldSeparator = false;

    //-------------------------------------------------
    // General
    //-------------------------------------------------

    /**
     * Sets the height of the containing element,
     * can be set to any valid height css value.
     * If set to false (the default), the height
     * of the table will resize to fit the table data.
     * Tabulator default: false.
     *
     * @var string|int|bool|null
     */
    protected $height;

    /**
     * Enable rendering using the Virtual DOM engine
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#virtual-dom.
     *
     * @var bool|null
     */
    protected $virtualDom;

    /**
     * Manually set the size of the virtual DOM buffer
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#virtual-dom-buffer.
     *
     * @var int|null
     */
    protected $virtualDomBuffer;

    /**
     * placeholder element to display on empty table
     * Tabulator default: string
     * http://tabulator.info/docs/3.5#placeholder-empty.
     *
     * AbstractOptions has placeholder functions that use this field,
     * if you override this, the defaults will no longer work.
     * We recommend using the AbstractOptions to achieve your goals
     *
     * @var string|null
     */
    protected $placeholder;

    /**
     * Function to generate tooltips for cells
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#tooltips.
     *
     * @var bool|CallbackInterface|null
     */
    protected $tooltips;

    /**
     * When to regenerate cell tooltip value
     * Tabulator default: "load"
     * http://tabulator.info/docs/3.5#tooltips.
     *
     * @var string|null
     */
    protected $tooltipGenerationMode;

    /**
     * Enable user interaction history functionality
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#history.
     *
     * @var bool|CallbackInterface|null
     */
    protected $history;

    /**
     * Keybinding configuration object
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#keybindings.
     *
     * @var bool|CallbackInterface|null
     */
    protected $keybindings;

    /**
     * set the current localization language
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#localization.
     *
     * @var string|bool|null
     */
    protected $locale;

    /**
     * hold localization templates
     * Tabulator default: {}
     * http://tabulator.info/docs/3.5#localization.
     *
     * @var CallbackInterface|null
     */
    protected $langs;

    /**
     * callback function to alter table data before download
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#download.
     *
     * @var bool|CallbackInterface|null
     */
    protected $downloadDataFormatter;

    //-------------------------------------------------
    // Columns
    //-------------------------------------------------

    /**
     * Layout mode for the table columns
     * Tabulator default: "fitData"
     * http://tabulator.info/docs/3.5#layout.
     *
     * @var string|null
     */
    protected $layout;

    /**
     * Change column widths to match data
     * when loaded into table
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#layoutcolumnsonnewdata.
     *
     * @var bool|null
     */
    protected $layoutColumnsOnNewData;

    /**
     * Automatically hide/show columns to fit
     * the width of the Tabulator element
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#responsive-layout.
     *
     * @var bool|string|null
     */
    protected $responsiveLayout;

    /**
     * show collapsed column list
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#responsive-layout.
     *
     * @var bool|null
     */
    protected $responsiveLayoutCollapseStartOpen;

    /**
     * use formatters in collapsed column lists
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#responsive-layout.
     *
     * @var bool|null
     */
    protected $responsiveLayoutCollapseUseFormatters;

    /**
     * create contents of collapsed column list
     * Tabulator default: {}
     * http://tabulator.info/docs/3.5#responsive-layout.
     *
     * @var bool|CallbackInterface|null
     */
    protected $responsiveLayoutCollapseFormatter;

    /**
     * Minimum width for a column
     * Tabulator default: 40px
     * http://tabulator.info/docs/3.5#col-min-width.
     *
     * @var string|null
     */
    protected $columnMinWidth;

    /**
     * Allow user to resize columns
     * (via handles on the left and right edges of the column header)
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#col-resize.
     *
     * @var bool|null
     */
    protected $resizableColumns;

    /**
     * Allow users to move and reorder columns
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#col-move.
     *
     * @var bool|null
     */
    protected $movableColumns;

    /**
     * Function to generate tooltips for column headers
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#col-tooltips.
     *
     * @var bool|CallbackInterface|null
     */
    protected $tooltipsHeader;

    /**
     * Vertical alignment for contents of
     * column header (used in column grouping)
     * Tabulator default: "top"
     * http://tabulator.info/docs/3.5#column-groups.
     *
     * @var string|null
     */
    protected $columnVertAlign;

    //-------------------------------------------------
    // Rows
    //-------------------------------------------------

    /**
     * The position in the table for new rows
     * to be added, "bottom" or "top"
     * Tabulator default: "bottom"
     * http://tabulator.info/docs/3.5#addrow.
     *
     * @var string|null
     */
    protected $addRowPos;

    /**
     * Enable/Disable row selection
     * Tabulator default: "highlight"
     * http://tabulator.info/docs/3.5#selectable.
     *
     * @var bool|string|null
     */
    protected $selectable;

    /**
     * Allow rolling selection
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#selectable.
     *
     * @var bool|null
     */
    protected $selectableRollingSelection;

    /**
     * Maintain selected rows on filter or sort
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#selectable.
     *
     * @var bool|null
     */
    protected $selectablePersistence;

    /**
     * Allow users to move and reorder rows
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#movable.
     *
     * @var bool|null
     */
    protected $movableRows;

    /**
     * Connection selector for receving tables
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#row-move-table.
     *
     * Usage:
     * Add format #ID_OF_TABLE, separated by comma.
     *
     * @var string|null
     */
    protected $movableRowsConnectedTables;

    /**
     * Sender function to be executed when row has been sent
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#row-move-table.
     *
     * @var string|CallbackInterface|bool|null
     */
    protected $movableRowsSender;

    /**
     * Sender function to be executed when row has been received
     * Tabulator default: "insert"
     * http://tabulator.info/docs/3.5#row-move-table.
     *
     * @var string|CallbackInterface|null
     */
    protected $movableRowsReceiver;

    /**
     * Allow user to resize rows (via handles on the top
     * and bottom edges of the row)
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#row-resize.
     *
     * @var bool|null
     */
    protected $resizableRows;

    /**
     * Default row position after scrollToRow
     * Tabulator default: "top"
     * http://tabulator.info/docs/3.5#scroll-to-row.
     *
     * @var string|null
     */
    protected $scrollToRowPosition;

    /**
     * Allow currently visible rows to be scrolled to
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#scroll-to-row.
     *
     * @var bool|null $
     */
    protected $scrollToRowIfVisible;

    //-------------------------------------------------
    // Data
    //-------------------------------------------------

    /**
     * URL for remote Ajax data loading.
     *
     * Please note, that even if using custom filter, this
     * value must be set.
     *
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#
     *
     * @var string|bool|null
     */
    protected $ajaxURL;

    /**
     * Parameters to be passed to remote Ajax data loading request
     * Tabulator default: {}
     * http://tabulator.info/docs/3.5#ajax.
     *
     * @var CallbackInterface|null
     */
    protected $ajaxParams;

    /**
     * The HTTP request type for Ajax requests or
     * config object for the request
     * Tabulator default: "GET"
     * http://tabulator.info/docs/3.5#ajax.
     *
     * @var string|CallbackInterface|null
     */
    protected $ajaxConfig;

    /**
     * Send filter config to server instead of processing locally
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#ajax-filtering.
     *
     * @var bool|null
     */
    protected $ajaxFiltering;

    /**
     * Send sorter config to server instead of processing locally
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#ajax-sorting.
     *
     * @var bool|null
     */
    protected $ajaxSorting;

    /**
     * Progressively load data into the table in chunks
     * Value 'scroll' will allow data to be loaded in while scrolling.
     *
     * When used with pagination, will change page when scrolling to end.
     * So it is encouraged to turn off pagination when using progressive load.
     *
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#ajax-progressive
     *
     * @var string|bool|null
     */
    protected $ajaxProgressiveLoad;

    /**
     * Delay in milliseconds between each progressive load request
     * Tabulator default: 0
     * http://tabulator.info/docs/3.5#ajax-progressive.
     *
     * @var int|null
     */
    protected $ajaxProgressiveLoadDelay;

    /**
     * The remaning distance in pixels between the scroll
     * bar and the bottom of the table before an ajax is triggered
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#ajax-progressive.
     *
     * @var int|bool|null
     */
    protected $ajaxProgressiveLoadScrollMargin;

    /**
     * Show loader while data is loading,
     * can also take a function that must return a boolean
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#ajax-progressive.
     *
     * @var bool|CallbackInterface|null
     */
    protected $ajaxLoader;

    /**
     * html for loader element
     * Tabulator default: <div style='display:inline-block; border:4px solid #333; border-radius:10px; background:#fff; font-weight:bold; font-size:16px; color:#000; padding:10px 20px;'>Loading Data</div>.
     *
     * http://tabulator.info/docs/3.5#
     *
     * @var string|null
     */
    protected $ajaxLoaderLoading;

    /**
     * html for the loader element in the event of an error
     * Tabulator default:<div style='display:inline-block; border:4px solid #D00; border-radius:10px; background:#fff; font-weight:bold; font-size:16px; color:#590000; padding:10px 20px;'>Loading Error</div>
     * http://tabulator.info/docs/3.5#.
     *
     * @var string|null
     */
    protected $ajaxLoaderError;

    //-------------------------------------------------
    // Initial Data Sorting
    //-------------------------------------------------

    /**
     * Array of sorters to be applied on load.
     * Tabulator default: []
     * http://tabulator.info/docs/3.5#sort-load.
     *
     * @var CallbackInterface|null
     */
    protected $initialSort;

    //-------------------------------------------------
    // Groups
    //-------------------------------------------------

    /**
     * String/function to select field to group rows by
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#grouping.
     *
     * @var string|CallbackInterface|null
     */
    protected $groupBy;

    /**
     * function to layout group header row
     * Tabulator default: (See documentation)
     * http://tabulator.info/docs/3.5#grouping.
     *
     * @var CallbackInterface|null
     */
    protected $groupHeader;

    /**
     * Boolean/function to set the open/closed state
     * of groups when they are first created
     * Tabulator default: true
     * http://tabulator.info/docs/3.5#grouping.
     *
     * @var CallbackInterface|bool|null
     */
    protected $groupStartOpen;

    /**
     * Set which element triggers a group visibility toggle
     * Tabulator default: "arrow"
     * http://tabulator.info/docs/3.5#grouping.
     *
     * @var string|bool|null
     */
    protected $groupToggleElement;

    //-------------------------------------------------
    // Pagination
    //-------------------------------------------------

    /**
     * Choose pagination method, "local" or "remote"
     * Tabulator default: false
     * http://tabulator.info/docs/3.5#pagination.
     *
     * @var bool|string|null
     */
    protected $pagination;

    /**
     * Set the number of rows in each page
     * Tabulator default: 10
     * http://tabulator.info/docs/3.5#pagination.
     *
     * @var int|null
     */
    protected $paginationSize;

    /**
     * The element to contain the pagination selectors
     * Tabulator default: (generated tabulator footer)
     * http://tabulator.info/docs/3.5#pagination.
     *
     * Use CallbackInterface if you want to append a template
     * Use string if you want to call another element on the page
     *
     * @var string|CallbackInterface|null
     */
    protected $paginationElement;

    /**
     * Lookup list to link expected data fields
     * from the server to their function
     * Tabulator default: {}
     * http://tabulator.info/docs/3.5#pagination.
     *
     * @var CallbackInterface|null
     */
    protected $paginationDataReceived;

    /**
     * Lookup list to link fields expected
     * by the server to their function
     * Tabulator default: {}
     * http://tabulator.info/docs/3.5#pagination.
     *
     * @var CallbackInterface|null
     */
    protected $paginationDataSent;

    /**
     * Function used to build pagination request URL string
     * Tabulator default: null
     * http://tabulator.info/docs/3.5#pagination.
     *
     * @var CallbackInterface|null
     */
    protected $paginator;

    /**
     * Set where rows should be added to the table
     * Tabulator default: "page"
     * http://tabulator.info/docs/3.5#pagination.
     *
     * @var string|null
     */
    protected $paginationAddRow;

    //-------------------------------------------------
    // Callbacks and Events
    //-------------------------------------------------

    /**
     * Table Building
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $tableBuilding;

    /**
     * Table Built
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $tableBuilt;

    /**
     * Column Moved
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $columnMoved;

    /**
     * Column Resized
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $columnResized;

    /**
     * The columnVisibilityChanged callback is triggered
     * whenever a column changes between hidden and visible states.
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $columnVisibilityChanged;

    /**
     * The columnTitleChanged callback is triggered whenever a
     * user edits a column title when the editableTitle parameter
     * has been enabled in the column definition array.
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $columnTitleChanged;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowDblClick;

    /**
     * The rowContext callback is triggered when a user right clicks on a row.
     * If you want to prevent the browsers context menu being triggered in
     * this event you will need to include the preventDefault() function in your callback.
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowContext;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowDblTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowTapHold;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowAdded;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowUpdated;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowDeleted;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowMoved;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowResized;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $headerTapHold;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $headerDblTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $headerTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $headerContext;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $headerDblClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $headerClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellDblClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellContext;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellDblTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellTapHold;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellEditing;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellEditCancelled;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $cellEdited;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataLoading;

    /**
     * Passthrough will be called instead of dataLoaded,
     * passthrough will itself call dataLoaded after handling the data.
     *
     * @var CallbackInterface|null
     */
    protected $dataLoadedPassthrough;
    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataLoaded;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataEdited;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $htmlImporting;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $htmlImported;

    /**
     * The ajaxRequesting callback is triggered
     * when ever an ajax request is made.
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $ajaxRequesting;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $ajaxResponse;

    /**
     * Passthrough will be called instead of ajaxResponse,
     * passthrough will itself call ajaxResponse after handling the data.
     *
     * @var CallbackInterface|null
     */
    protected $ajaxResponsePassthrough;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $ajaxError;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataFiltering;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataFiltered;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataSorting;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataSorted;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $renderStarted;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $renderComplete;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $pageLoaded;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $localized;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataGrouping;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $dataGrouped;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupVisibilityChanged;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupDblClick;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupContext;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupDblTap;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $groupTapHold;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowSelected;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowDeselected;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $rowSelectionChanged;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsSendingStart;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsSent;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsSentFailed;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsSendingStop;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsReceivingStart;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsReceived;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsReceivedFailed;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $movableRowsReceivingStop;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $validationFailed;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $historyUndo;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $historyRedo;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $clipboardCopied;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $clipboardPasted;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $clipboardPasteError;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $downloadReady;

    /**
     * http://tabulator.info/docs/3.5#callbacks.
     *
     * @var CallbackInterface|null
     */
    protected $downloadComplete;

    /**
     * @return string
     */
    public function getEditableKey(): string
    {
        return $this->editableKey;
    }

    /**
     * @param string $editableKey
     *
     * @return $this
     */
    public function setEditableKey(string $editableKey): self
    {
        $this->editableKey = $editableKey;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDefaultCellStateEditable(): bool
    {
        return $this->defaultCellStateEditable;
    }

    /**
     * @param bool $defaultCellStateEditable
     *
     * @return $this
     */
    public function setDefaultCellStateEditable(bool $defaultCellStateEditable): self
    {
        $this->defaultCellStateEditable = $defaultCellStateEditable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAjaxFilterOverride(): bool
    {
        return $this->ajaxFilterOverride;
    }

    /**
     * @param bool $ajaxFilterOverride
     *
     * @return $this
     */
    public function setAjaxFilterOverride($ajaxFilterOverride): self
    {
        $this->ajaxFilterOverride = $ajaxFilterOverride;

        return $this;
    }

    /**
     * @param CallbackInterface|null $downloadComplete
     *
     * @return $this
     */
    public function setDownloadComplete(?CallbackInterface $downloadComplete): self
    {
        $this->downloadComplete = $downloadComplete;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDownloadComplete(): ?CallbackInterface
    {
        return $this->downloadComplete;
    }

    /**
     * @return bool|int|string|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param bool|int|string|null $height
     *
     * @return $this
     */
    public function setHeight($height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getVirtualDom(): ?bool
    {
        return $this->virtualDom;
    }

    /**
     * @param bool|null $virtualDom
     *
     * @return $this
     */
    public function setVirtualDom(?bool $virtualDom): self
    {
        $this->virtualDom = $virtualDom;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getVirtualDomBuffer(): ?int
    {
        return $this->virtualDomBuffer;
    }

    /**
     * @param int|null $virtualDomBuffer
     *
     * @return $this
     */
    public function setVirtualDomBuffer(?int $virtualDomBuffer): self
    {
        $this->virtualDomBuffer = $virtualDomBuffer;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlaceholder(): ?string
    {
        return $this->placeholder;
    }

    /**
     * @param string|null $placeholder
     *
     * @return $this
     */
    public function setPlaceholder(?string $placeholder): self
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getTooltips()
    {
        return $this->tooltips;
    }

    /**
     * @param bool|CallbackInterface|null $tooltips
     *
     * @return $this
     */
    public function setTooltips($tooltips)
    {
        $this->tooltips = $tooltips;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTooltipGenerationMode(): ?string
    {
        return $this->tooltipGenerationMode;
    }

    /**
     * @param string|null $tooltipGenerationMode
     *
     * @return $this
     */
    public function setTooltipGenerationMode(?string $tooltipGenerationMode): self
    {
        $this->tooltipGenerationMode = $tooltipGenerationMode;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param bool|CallbackInterface|null $history
     *
     * @return $this
     */
    public function setHistory($history)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getKeybindings()
    {
        return $this->keybindings;
    }

    /**
     * @param bool|CallbackInterface|null $keybindings
     *
     * @return $this
     */
    public function setKeybindings($keybindings)
    {
        $this->keybindings = $keybindings;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param bool|string|null $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getLangs(): ?CallbackInterface
    {
        return $this->langs;
    }

    /**
     * @param CallbackInterface|null $langs
     *
     * @return $this
     */
    public function setLangs(?CallbackInterface $langs): self
    {
        $this->langs = $langs;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getDownloadDataFormatter()
    {
        return $this->downloadDataFormatter;
    }

    /**
     * @param bool|CallbackInterface|null $downloadDataFormatter
     *
     * @return $this
     */
    public function setDownloadDataFormatter($downloadDataFormatter)
    {
        $this->downloadDataFormatter = $downloadDataFormatter;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLayout(): ?string
    {
        return $this->layout;
    }

    /**
     * @param string|null $layout
     *
     * @return $this
     */
    public function setLayout(?string $layout): self
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getLayoutColumnsOnNewData(): ?bool
    {
        return $this->layoutColumnsOnNewData;
    }

    /**
     * @param bool|null $layoutColumnsOnNewData
     *
     * @return $this
     */
    public function setLayoutColumnsOnNewData(?bool $layoutColumnsOnNewData): self
    {
        $this->layoutColumnsOnNewData = $layoutColumnsOnNewData;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getResponsiveLayout()
    {
        return $this->responsiveLayout;
    }

    /**
     * @param bool|string|null $responsiveLayout
     *
     * @return $this
     */
    public function setResponsiveLayout($responsiveLayout): self
    {
        $this->responsiveLayout = $responsiveLayout;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getResponsiveLayoutCollapseStartOpen(): ?bool
    {
        return $this->responsiveLayoutCollapseStartOpen;
    }

    /**
     * @param bool|null $responsiveLayoutCollapseStartOpen
     *
     * @return $this
     */
    public function setResponsiveLayoutCollapseStartOpen(?bool $responsiveLayoutCollapseStartOpen): self
    {
        $this->responsiveLayoutCollapseStartOpen = $responsiveLayoutCollapseStartOpen;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getResponsiveLayoutCollapseUseFormatters(): ?bool
    {
        return $this->responsiveLayoutCollapseUseFormatters;
    }

    /**
     * @param bool|null $responsiveLayoutCollapseUseFormatters
     *
     * @return $this
     */
    public function setResponsiveLayoutCollapseUseFormatters(?bool $responsiveLayoutCollapseUseFormatters): self
    {
        $this->responsiveLayoutCollapseUseFormatters = $responsiveLayoutCollapseUseFormatters;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getResponsiveLayoutCollapseFormatter()
    {
        return $this->responsiveLayoutCollapseFormatter;
    }

    /**
     * @param bool|CallbackInterface|null $responsiveLayoutCollapseFormatter
     *
     * @return $this
     */
    public function setResponsiveLayoutCollapseFormatter($responsiveLayoutCollapseFormatter)
    {
        $this->responsiveLayoutCollapseFormatter = $responsiveLayoutCollapseFormatter;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getColumnMinWidth(): ?string
    {
        return $this->columnMinWidth;
    }

    /**
     * @param string|null $columnMinWidth
     *
     * @return $this
     */
    public function setColumnMinWidth(?string $columnMinWidth): self
    {
        $this->columnMinWidth = $columnMinWidth;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getResizableColumns(): ?bool
    {
        return $this->resizableColumns;
    }

    /**
     * @param bool|null $resizableColumns
     *
     * @return $this
     */
    public function setResizableColumns(?bool $resizableColumns): self
    {
        $this->resizableColumns = $resizableColumns;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMovableColumns(): ?bool
    {
        return $this->movableColumns;
    }

    /**
     * @param bool|null $movableColumns
     *
     * @return $this
     */
    public function setMovableColumns(?bool $movableColumns): self
    {
        $this->movableColumns = $movableColumns;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getTooltipsHeader()
    {
        return $this->tooltipsHeader;
    }

    /**
     * @param bool|CallbackInterface|null $tooltipsHeader
     *
     * @return $this
     */
    public function setTooltipsHeader($tooltipsHeader)
    {
        $this->tooltipsHeader = $tooltipsHeader;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getColumnVertAlign(): ?string
    {
        return $this->columnVertAlign;
    }

    /**
     * @param string|null $columnVertAlign
     *
     * @return $this
     */
    public function setColumnVertAlign(?string $columnVertAlign): self
    {
        $this->columnVertAlign = $columnVertAlign;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddRowPos(): ?string
    {
        return $this->addRowPos;
    }

    /**
     * @param string|null $addRowPos
     *
     * @return $this
     */
    public function setAddRowPos(?string $addRowPos): self
    {
        $this->addRowPos = $addRowPos;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getSelectable()
    {
        return $this->selectable;
    }

    /**
     * @param bool|string|null $selectable
     *
     * @return $this
     */
    public function setSelectable($selectable)
    {
        $this->selectable = $selectable;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSelectableRollingSelection(): ?bool
    {
        return $this->selectableRollingSelection;
    }

    /**
     * @param bool|null $selectableRollingSelection
     *
     * @return $this
     */
    public function setSelectableRollingSelection(?bool $selectableRollingSelection): self
    {
        $this->selectableRollingSelection = $selectableRollingSelection;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSelectablePersistence(): ?bool
    {
        return $this->selectablePersistence;
    }

    /**
     * @param bool|null $selectablePersistence
     *
     * @return $this
     */
    public function setSelectablePersistence(?bool $selectablePersistence): self
    {
        $this->selectablePersistence = $selectablePersistence;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getMovableRows(): ?bool
    {
        return $this->movableRows;
    }

    /**
     * @param bool|null $movableRows
     *
     * @return $this
     */
    public function setMovableRows(?bool $movableRows): self
    {
        $this->movableRows = $movableRows;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMovableRowsConnectedTables(): ?string
    {
        return $this->movableRowsConnectedTables;
    }

    /**
     * @param string|null $movableRowsConnectedTables
     *
     * @return $this
     */
    public function setMovableRowsConnectedTables(?string $movableRowsConnectedTables): self
    {
        $this->movableRowsConnectedTables = $movableRowsConnectedTables;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|string|null
     */
    public function getMovableRowsSender()
    {
        return $this->movableRowsSender;
    }

    /**
     * @param bool|CallbackInterface|string|null $movableRowsSender
     *
     * @return $this
     */
    public function setMovableRowsSender($movableRowsSender)
    {
        $this->movableRowsSender = $movableRowsSender;

        return $this;
    }

    /**
     * @return CallbackInterface|string|null
     */
    public function getMovableRowsReceiver()
    {
        return $this->movableRowsReceiver;
    }

    /**
     * @param CallbackInterface|string|null $movableRowsReceiver
     *
     * @return $this
     */
    public function setMovableRowsReceiver($movableRowsReceiver)
    {
        $this->movableRowsReceiver = $movableRowsReceiver;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getResizableRows(): ?bool
    {
        return $this->resizableRows;
    }

    /**
     * @param bool|null $resizableRows
     *
     * @return $this
     */
    public function setResizableRows(?bool $resizableRows): self
    {
        $this->resizableRows = $resizableRows;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getScrollToRowPosition(): ?string
    {
        return $this->scrollToRowPosition;
    }

    /**
     * @param string|null $scrollToRowPosition
     *
     * @return $this
     */
    public function setScrollToRowPosition(?string $scrollToRowPosition): self
    {
        $this->scrollToRowPosition = $scrollToRowPosition;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getScrollToRowIfVisible(): ?bool
    {
        return $this->scrollToRowIfVisible;
    }

    /**
     * @param bool|null $scrollToRowIfVisible
     *
     * @return $this
     */
    public function setScrollToRowIfVisible(?bool $scrollToRowIfVisible): self
    {
        $this->scrollToRowIfVisible = $scrollToRowIfVisible;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getAjaxURL()
    {
        return $this->ajaxURL;
    }

    /**
     * @param bool|string|null $ajaxURL
     *
     * @return $this
     */
    public function setAjaxURL($ajaxURL)
    {
        $this->ajaxURL = $ajaxURL;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getAjaxParams(): ?CallbackInterface
    {
        return $this->ajaxParams;
    }

    /**
     * @param CallbackInterface|null $ajaxParams
     *
     * @return $this
     */
    public function setAjaxParams(?CallbackInterface $ajaxParams): self
    {
        $this->ajaxParams = $ajaxParams;

        return $this;
    }

    /**
     * @return CallbackInterface|string|null
     */
    public function getAjaxConfig()
    {
        return $this->ajaxConfig;
    }

    /**
     * @param CallbackInterface|string|null $ajaxConfig
     *
     * @return $this
     */
    public function setAjaxConfig($ajaxConfig)
    {
        $this->ajaxConfig = $ajaxConfig;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAjaxFiltering(): ?bool
    {
        return $this->ajaxFiltering;
    }

    /**
     * @param bool|null $ajaxFiltering
     *
     * @return $this
     */
    public function setAjaxFiltering(?bool $ajaxFiltering): self
    {
        $this->ajaxFiltering = $ajaxFiltering;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAjaxSorting(): ?bool
    {
        return $this->ajaxSorting;
    }

    /**
     * @param bool|null $ajaxSorting
     *
     * @return $this
     */
    public function setAjaxSorting(?bool $ajaxSorting): self
    {
        $this->ajaxSorting = $ajaxSorting;

        return $this;
    }

    /**
     * @return string|bool|null
     */
    public function getAjaxProgressiveLoad()
    {
        return $this->ajaxProgressiveLoad;
    }

    /**
     * @param string|bool|null $ajaxProgressiveLoad
     *
     * @return $this
     */
    public function setAjaxProgressiveLoad($ajaxProgressiveLoad): self
    {
        $this->ajaxProgressiveLoad = $ajaxProgressiveLoad;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getAjaxProgressiveLoadDelay(): ?int
    {
        return $this->ajaxProgressiveLoadDelay;
    }

    /**
     * @param int|null $ajaxProgressiveLoadDelay
     *
     * @return $this
     */
    public function setAjaxProgressiveLoadDelay(?int $ajaxProgressiveLoadDelay): self
    {
        $this->ajaxProgressiveLoadDelay = $ajaxProgressiveLoadDelay;

        return $this;
    }

    /**
     * @return bool|int|null
     */
    public function getAjaxProgressiveLoadScrollMargin()
    {
        return $this->ajaxProgressiveLoadScrollMargin;
    }

    /**
     * @param bool|int|null $ajaxProgressiveLoadScrollMargin
     *
     * @return $this
     */
    public function setAjaxProgressiveLoadScrollMargin($ajaxProgressiveLoadScrollMargin)
    {
        $this->ajaxProgressiveLoadScrollMargin = $ajaxProgressiveLoadScrollMargin;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getAjaxLoader()
    {
        return $this->ajaxLoader;
    }

    /**
     * @param bool|CallbackInterface|null $ajaxLoader
     *
     * @return $this
     */
    public function setAjaxLoader($ajaxLoader)
    {
        $this->ajaxLoader = $ajaxLoader;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAjaxLoaderLoading(): ?string
    {
        return $this->ajaxLoaderLoading;
    }

    /**
     * @param string|null $ajaxLoaderLoading
     *
     * @return $this
     */
    public function setAjaxLoaderLoading(?string $ajaxLoaderLoading): self
    {
        $this->ajaxLoaderLoading = $ajaxLoaderLoading;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAjaxLoaderError(): ?string
    {
        return $this->ajaxLoaderError;
    }

    /**
     * @param string|null $ajaxLoaderError
     *
     * @return $this
     */
    public function setAjaxLoaderError(?string $ajaxLoaderError): self
    {
        $this->ajaxLoaderError = $ajaxLoaderError;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getInitialSort(): ?CallbackInterface
    {
        return $this->initialSort;
    }

    /**
     * @param CallbackInterface|null $initialSort
     *
     * @return $this
     */
    public function setInitialSort(?CallbackInterface $initialSort): self
    {
        $this->initialSort = $initialSort;

        return $this;
    }

    /**
     * @return CallbackInterface|string|null
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }

    /**
     * @param CallbackInterface|string|null $groupBy
     *
     * @return $this
     */
    public function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupHeader(): ?CallbackInterface
    {
        return $this->groupHeader;
    }

    /**
     * @param CallbackInterface|null $groupHeader
     *
     * @return $this
     */
    public function setGroupHeader(?CallbackInterface $groupHeader): self
    {
        $this->groupHeader = $groupHeader;

        return $this;
    }

    /**
     * @return bool|CallbackInterface|null
     */
    public function getGroupStartOpen()
    {
        return $this->groupStartOpen;
    }

    /**
     * @param bool|CallbackInterface|null $groupStartOpen
     *
     * @return $this
     */
    public function setGroupStartOpen($groupStartOpen)
    {
        $this->groupStartOpen = $groupStartOpen;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getGroupToggleElement()
    {
        return $this->groupToggleElement;
    }

    /**
     * @param bool|string|null $groupToggleElement
     *
     * @return $this
     */
    public function setGroupToggleElement($groupToggleElement)
    {
        $this->groupToggleElement = $groupToggleElement;

        return $this;
    }

    /**
     * @return bool|string|null
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @param bool|string|null $pagination
     *
     * @return $this
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaginationSize(): ?int
    {
        return $this->paginationSize;
    }

    /**
     * @param int|null $paginationSize
     *
     * @return $this
     */
    public function setPaginationSize(?int $paginationSize): self
    {
        $this->paginationSize = $paginationSize;

        return $this;
    }

    /**
     * @return CallbackInterface|string|null
     */
    public function getPaginationElement()
    {
        return $this->paginationElement;
    }

    /**
     * @param CallbackInterface|string|null $paginationElement
     *
     * @return $this
     */
    public function setPaginationElement($paginationElement)
    {
        $this->paginationElement = $paginationElement;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getPaginationDataReceived(): ?CallbackInterface
    {
        return $this->paginationDataReceived;
    }

    /**
     * @param CallbackInterface|null $paginationDataReceived
     *
     * @return $this
     */
    public function setPaginationDataReceived(?CallbackInterface $paginationDataReceived): self
    {
        $this->paginationDataReceived = $paginationDataReceived;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getPaginationDataSent(): ?CallbackInterface
    {
        return $this->paginationDataSent;
    }

    /**
     * @param CallbackInterface|null $paginationDataSent
     *
     * @return $this
     */
    public function setPaginationDataSent(?CallbackInterface $paginationDataSent): self
    {
        $this->paginationDataSent = $paginationDataSent;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getPaginator(): ?CallbackInterface
    {
        return $this->paginator;
    }

    /**
     * @param CallbackInterface|null $paginator
     *
     * @return $this
     */
    public function setPaginator(?CallbackInterface $paginator): self
    {
        $this->paginator = $paginator;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaginationAddRow(): ?string
    {
        return $this->paginationAddRow;
    }

    /**
     * @param string|null $paginationAddRow
     *
     * @return $this
     */
    public function setPaginationAddRow(?string $paginationAddRow): self
    {
        $this->paginationAddRow = $paginationAddRow;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getTableBuilding(): ?CallbackInterface
    {
        return $this->tableBuilding;
    }

    /**
     * @param CallbackInterface|null $tableBuilding
     *
     * @return $this
     */
    public function setTableBuilding(?CallbackInterface $tableBuilding): self
    {
        $this->tableBuilding = $tableBuilding;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getTableBuilt(): ?CallbackInterface
    {
        return $this->tableBuilt;
    }

    /**
     * @param CallbackInterface|null $tableBuilt
     *
     * @return $this
     */
    public function setTableBuilt(?CallbackInterface $tableBuilt): self
    {
        $this->tableBuilt = $tableBuilt;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getColumnMoved(): ?CallbackInterface
    {
        return $this->columnMoved;
    }

    /**
     * @param CallbackInterface|null $columnMoved
     *
     * @return $this
     */
    public function setColumnMoved(?CallbackInterface $columnMoved): self
    {
        $this->columnMoved = $columnMoved;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getColumnResized(): ?CallbackInterface
    {
        return $this->columnResized;
    }

    /**
     * @param CallbackInterface|null $columnResized
     *
     * @return $this
     */
    public function setColumnResized(?CallbackInterface $columnResized): self
    {
        $this->columnResized = $columnResized;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getColumnVisibilityChanged(): ?CallbackInterface
    {
        return $this->columnVisibilityChanged;
    }

    /**
     * @param CallbackInterface|null $columnVisibilityChanged
     *
     * @return $this
     */
    public function setColumnVisibilityChanged(?CallbackInterface $columnVisibilityChanged): self
    {
        $this->columnVisibilityChanged = $columnVisibilityChanged;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getColumnTitleChanged(): ?CallbackInterface
    {
        return $this->columnTitleChanged;
    }

    /**
     * @param CallbackInterface|null $columnTitleChanged
     *
     * @return $this
     */
    public function setColumnTitleChanged(?CallbackInterface $columnTitleChanged): self
    {
        $this->columnTitleChanged = $columnTitleChanged;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowClick(): ?CallbackInterface
    {
        return $this->rowClick;
    }

    /**
     * @param CallbackInterface|null $rowClick
     *
     * @return $this
     */
    public function setRowClick(?CallbackInterface $rowClick): self
    {
        $this->rowClick = $rowClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowDblClick(): ?CallbackInterface
    {
        return $this->rowDblClick;
    }

    /**
     * @param CallbackInterface|null $rowDblClick
     *
     * @return $this
     */
    public function setRowDblClick(?CallbackInterface $rowDblClick): self
    {
        $this->rowDblClick = $rowDblClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowContext(): ?CallbackInterface
    {
        return $this->rowContext;
    }

    /**
     * @param CallbackInterface|null $rowContext
     *
     * @return $this
     */
    public function setRowContext(?CallbackInterface $rowContext): self
    {
        $this->rowContext = $rowContext;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowTap(): ?CallbackInterface
    {
        return $this->rowTap;
    }

    /**
     * @param CallbackInterface|null $rowTap
     *
     * @return $this
     */
    public function setRowTap(?CallbackInterface $rowTap): self
    {
        $this->rowTap = $rowTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowDblTap(): ?CallbackInterface
    {
        return $this->rowDblTap;
    }

    /**
     * @param CallbackInterface|null $rowDblTap
     *
     * @return $this
     */
    public function setRowDblTap(?CallbackInterface $rowDblTap): self
    {
        $this->rowDblTap = $rowDblTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowTapHold(): ?CallbackInterface
    {
        return $this->rowTapHold;
    }

    /**
     * @param CallbackInterface|null $rowTapHold
     *
     * @return $this
     */
    public function setRowTapHold(?CallbackInterface $rowTapHold): self
    {
        $this->rowTapHold = $rowTapHold;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowAdded(): ?CallbackInterface
    {
        return $this->rowAdded;
    }

    /**
     * @param CallbackInterface|null $rowAdded
     *
     * @return $this
     */
    public function setRowAdded(?CallbackInterface $rowAdded): self
    {
        $this->rowAdded = $rowAdded;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowUpdated(): ?CallbackInterface
    {
        return $this->rowUpdated;
    }

    /**
     * @param CallbackInterface|null $rowUpdated
     *
     * @return $this
     */
    public function setRowUpdated(?CallbackInterface $rowUpdated): self
    {
        $this->rowUpdated = $rowUpdated;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowDeleted(): ?CallbackInterface
    {
        return $this->rowDeleted;
    }

    /**
     * @param CallbackInterface|null $rowDeleted
     *
     * @return $this
     */
    public function setRowDeleted(?CallbackInterface $rowDeleted): self
    {
        $this->rowDeleted = $rowDeleted;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowMoved(): ?CallbackInterface
    {
        return $this->rowMoved;
    }

    /**
     * @param CallbackInterface|null $rowMoved
     *
     * @return $this
     */
    public function setRowMoved(?CallbackInterface $rowMoved): self
    {
        $this->rowMoved = $rowMoved;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowResized(): ?CallbackInterface
    {
        return $this->rowResized;
    }

    /**
     * @param CallbackInterface|null $rowResized
     *
     * @return $this
     */
    public function setRowResized(?CallbackInterface $rowResized): self
    {
        $this->rowResized = $rowResized;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderTapHold(): ?CallbackInterface
    {
        return $this->headerTapHold;
    }

    /**
     * @param CallbackInterface|null $headerTapHold
     *
     * @return $this
     */
    public function setHeaderTapHold(?CallbackInterface $headerTapHold): self
    {
        $this->headerTapHold = $headerTapHold;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderDblTap(): ?CallbackInterface
    {
        return $this->headerDblTap;
    }

    /**
     * @param CallbackInterface|null $headerDblTap
     *
     * @return $this
     */
    public function setHeaderDblTap(?CallbackInterface $headerDblTap): self
    {
        $this->headerDblTap = $headerDblTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderTap(): ?CallbackInterface
    {
        return $this->headerTap;
    }

    /**
     * @param CallbackInterface|null $headerTap
     *
     * @return $this
     */
    public function setHeaderTap(?CallbackInterface $headerTap): self
    {
        $this->headerTap = $headerTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderContext(): ?CallbackInterface
    {
        return $this->headerContext;
    }

    /**
     * @param CallbackInterface|null $headerContext
     *
     * @return $this
     */
    public function setHeaderContext(?CallbackInterface $headerContext): self
    {
        $this->headerContext = $headerContext;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderDblClick(): ?CallbackInterface
    {
        return $this->headerDblClick;
    }

    /**
     * @param CallbackInterface|null $headerDblClick
     *
     * @return $this
     */
    public function setHeaderDblClick(?CallbackInterface $headerDblClick): self
    {
        $this->headerDblClick = $headerDblClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHeaderClick(): ?CallbackInterface
    {
        return $this->headerClick;
    }

    /**
     * @param CallbackInterface|null $headerClick
     *
     * @return $this
     */
    public function setHeaderClick(?CallbackInterface $headerClick): self
    {
        $this->headerClick = $headerClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellClick(): ?CallbackInterface
    {
        return $this->cellClick;
    }

    /**
     * @param CallbackInterface|null $cellClick
     *
     * @return $this
     */
    public function setCellClick(?CallbackInterface $cellClick): self
    {
        $this->cellClick = $cellClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellDblClick(): ?CallbackInterface
    {
        return $this->cellDblClick;
    }

    /**
     * @param CallbackInterface|null $cellDblClick
     *
     * @return $this
     */
    public function setCellDblClick(?CallbackInterface $cellDblClick): self
    {
        $this->cellDblClick = $cellDblClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellContext(): ?CallbackInterface
    {
        return $this->cellContext;
    }

    /**
     * @param CallbackInterface|null $cellContext
     *
     * @return $this
     */
    public function setCellContext(?CallbackInterface $cellContext): self
    {
        $this->cellContext = $cellContext;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellTap(): ?CallbackInterface
    {
        return $this->cellTap;
    }

    /**
     * @param CallbackInterface|null $cellTap
     *
     * @return $this
     */
    public function setCellTap(?CallbackInterface $cellTap): self
    {
        $this->cellTap = $cellTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellDblTap(): ?CallbackInterface
    {
        return $this->cellDblTap;
    }

    /**
     * @param CallbackInterface|null $cellDblTap
     *
     * @return $this
     */
    public function setCellDblTap(?CallbackInterface $cellDblTap): self
    {
        $this->cellDblTap = $cellDblTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellTapHold(): ?CallbackInterface
    {
        return $this->cellTapHold;
    }

    /**
     * @param CallbackInterface|null $cellTapHold
     *
     * @return $this
     */
    public function setCellTapHold(?CallbackInterface $cellTapHold): self
    {
        $this->cellTapHold = $cellTapHold;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellEditing(): ?CallbackInterface
    {
        return $this->cellEditing;
    }

    /**
     * @param CallbackInterface|null $cellEditing
     *
     * @return $this
     */
    public function setCellEditing(?CallbackInterface $cellEditing): self
    {
        $this->cellEditing = $cellEditing;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellEditCancelled(): ?CallbackInterface
    {
        return $this->cellEditCancelled;
    }

    /**
     * @param CallbackInterface|null $cellEditCancelled
     *
     * @return $this
     */
    public function setCellEditCancelled(?CallbackInterface $cellEditCancelled): self
    {
        $this->cellEditCancelled = $cellEditCancelled;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getCellEdited(): ?CallbackInterface
    {
        return $this->cellEdited;
    }

    /**
     * @param CallbackInterface|null $cellEdited
     *
     * @return $this
     */
    public function setCellEdited(?CallbackInterface $cellEdited): self
    {
        $this->cellEdited = $cellEdited;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataLoading(): ?CallbackInterface
    {
        return $this->dataLoading;
    }

    /**
     * @param CallbackInterface|null $dataLoading
     *
     * @return $this
     */
    public function setDataLoading(?CallbackInterface $dataLoading): self
    {
        $this->dataLoading = $dataLoading;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataLoadedPassthrough(): ?CallbackInterface
    {
        return $this->dataLoadedPassthrough;
    }

    /**
     * @param CallbackInterface|null $dataLoadedPassthrough
     *
     * @return $this
     */
    public function setDataLoadedPassthrough(?CallbackInterface $dataLoadedPassthrough): self
    {
        $this->dataLoadedPassthrough = $dataLoadedPassthrough;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataLoaded(): ?CallbackInterface
    {
        return $this->dataLoaded;
    }

    /**
     * @param CallbackInterface|null $dataLoaded
     *
     * @return $this
     */
    public function setDataLoaded(?CallbackInterface $dataLoaded): self
    {
        $this->dataLoaded = $dataLoaded;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataEdited(): ?CallbackInterface
    {
        return $this->dataEdited;
    }

    /**
     * @param CallbackInterface|null $dataEdited
     *
     * @return $this
     */
    public function setDataEdited(?CallbackInterface $dataEdited): self
    {
        $this->dataEdited = $dataEdited;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHtmlImporting(): ?CallbackInterface
    {
        return $this->htmlImporting;
    }

    /**
     * @param CallbackInterface|null $htmlImporting
     *
     * @return $this
     */
    public function setHtmlImporting(?CallbackInterface $htmlImporting): self
    {
        $this->htmlImporting = $htmlImporting;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHtmlImported(): ?CallbackInterface
    {
        return $this->htmlImported;
    }

    /**
     * @param CallbackInterface|null $htmlImported
     *
     * @return $this
     */
    public function setHtmlImported(?CallbackInterface $htmlImported): self
    {
        $this->htmlImported = $htmlImported;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getAjaxRequesting(): ?CallbackInterface
    {
        return $this->ajaxRequesting;
    }

    /**
     * @param CallbackInterface|null $ajaxRequesting
     *
     * @return $this
     */
    public function setAjaxRequesting(?CallbackInterface $ajaxRequesting): self
    {
        $this->ajaxRequesting = $ajaxRequesting;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getAjaxResponse(): ?CallbackInterface
    {
        return $this->ajaxResponse;
    }

    /**
     * @param CallbackInterface|null $ajaxResponse
     *
     * @return $this
     */
    public function setAjaxResponse(?CallbackInterface $ajaxResponse): self
    {
        $this->ajaxResponse = $ajaxResponse;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getAjaxResponsePassthrough(): ?CallbackInterface
    {
        return $this->ajaxResponsePassthrough;
    }

    /**
     * @param CallbackInterface|null $ajaxResponsePassthrough
     *
     * @return $this
     */
    public function setAjaxResponsePassthrough(?CallbackInterface $ajaxResponsePassthrough): self
    {
        $this->ajaxResponsePassthrough = $ajaxResponsePassthrough;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getAjaxError(): ?CallbackInterface
    {
        return $this->ajaxError;
    }

    /**
     * @param CallbackInterface|null $ajaxError
     *
     * @return $this
     */
    public function setAjaxError(?CallbackInterface $ajaxError): self
    {
        $this->ajaxError = $ajaxError;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataFiltering(): ?CallbackInterface
    {
        return $this->dataFiltering;
    }

    /**
     * @param CallbackInterface|null $dataFiltering
     *
     * @return $this
     */
    public function setDataFiltering(?CallbackInterface $dataFiltering): self
    {
        $this->dataFiltering = $dataFiltering;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataFiltered(): ?CallbackInterface
    {
        return $this->dataFiltered;
    }

    /**
     * @param CallbackInterface|null $dataFiltered
     *
     * @return $this
     */
    public function setDataFiltered(?CallbackInterface $dataFiltered): self
    {
        $this->dataFiltered = $dataFiltered;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataSorting(): ?CallbackInterface
    {
        return $this->dataSorting;
    }

    /**
     * @param CallbackInterface|null $dataSorting
     *
     * @return $this
     */
    public function setDataSorting(?CallbackInterface $dataSorting): self
    {
        $this->dataSorting = $dataSorting;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataSorted(): ?CallbackInterface
    {
        return $this->dataSorted;
    }

    /**
     * @param CallbackInterface|null $dataSorted
     *
     * @return $this
     */
    public function setDataSorted(?CallbackInterface $dataSorted): self
    {
        $this->dataSorted = $dataSorted;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRenderStarted(): ?CallbackInterface
    {
        return $this->renderStarted;
    }

    /**
     * @param CallbackInterface|null $renderStarted
     *
     * @return $this
     */
    public function setRenderStarted(?CallbackInterface $renderStarted): self
    {
        $this->renderStarted = $renderStarted;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRenderStartedPassthrough(): ?CallbackInterface
    {
        return $this->renderStartedPassthrough;
    }

    /**
     * @param CallbackInterface|null $renderStartedPassthrough
     *
     * @return $this
     */
    public function setRenderStartedPassthrough(?CallbackInterface $renderStartedPassthrough): self
    {
        $this->renderStartedPassthrough = $renderStartedPassthrough;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRenderComplete(): ?CallbackInterface
    {
        return $this->renderComplete;
    }

    /**
     * @param CallbackInterface|null $renderComplete
     *
     * @return $this
     */
    public function setRenderComplete(?CallbackInterface $renderComplete): self
    {
        $this->renderComplete = $renderComplete;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getPageLoaded(): ?CallbackInterface
    {
        return $this->pageLoaded;
    }

    /**
     * @param CallbackInterface|null $pageLoaded
     *
     * @return $this
     */
    public function setPageLoaded(?CallbackInterface $pageLoaded): self
    {
        $this->pageLoaded = $pageLoaded;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getLocalized(): ?CallbackInterface
    {
        return $this->localized;
    }

    /**
     * @param CallbackInterface|null $localized
     *
     * @return $this
     */
    public function setLocalized(?CallbackInterface $localized): self
    {
        $this->localized = $localized;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataGrouping(): ?CallbackInterface
    {
        return $this->dataGrouping;
    }

    /**
     * @param CallbackInterface|null $dataGrouping
     *
     * @return $this
     */
    public function setDataGrouping(?CallbackInterface $dataGrouping): self
    {
        $this->dataGrouping = $dataGrouping;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDataGrouped(): ?CallbackInterface
    {
        return $this->dataGrouped;
    }

    /**
     * @param CallbackInterface|null $dataGrouped
     *
     * @return $this
     */
    public function setDataGrouped(?CallbackInterface $dataGrouped): self
    {
        $this->dataGrouped = $dataGrouped;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupVisibilityChanged(): ?CallbackInterface
    {
        return $this->groupVisibilityChanged;
    }

    /**
     * @param CallbackInterface|null $groupVisibilityChanged
     *
     * @return $this
     */
    public function setGroupVisibilityChanged(?CallbackInterface $groupVisibilityChanged): self
    {
        $this->groupVisibilityChanged = $groupVisibilityChanged;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupClick(): ?CallbackInterface
    {
        return $this->groupClick;
    }

    /**
     * @param CallbackInterface|null $groupClick
     *
     * @return $this
     */
    public function setGroupClick(?CallbackInterface $groupClick): self
    {
        $this->groupClick = $groupClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupDblClick(): ?CallbackInterface
    {
        return $this->groupDblClick;
    }

    /**
     * @param CallbackInterface|null $groupDblClick
     *
     * @return $this
     */
    public function setGroupDblClick(?CallbackInterface $groupDblClick): self
    {
        $this->groupDblClick = $groupDblClick;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupContext(): ?CallbackInterface
    {
        return $this->groupContext;
    }

    /**
     * @param CallbackInterface|null $groupContext
     *
     * @return $this
     */
    public function setGroupContext(?CallbackInterface $groupContext): self
    {
        $this->groupContext = $groupContext;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupTap(): ?CallbackInterface
    {
        return $this->groupTap;
    }

    /**
     * @param CallbackInterface|null $groupTap
     *
     * @return $this
     */
    public function setGroupTap(?CallbackInterface $groupTap): self
    {
        $this->groupTap = $groupTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupDblTap(): ?CallbackInterface
    {
        return $this->groupDblTap;
    }

    /**
     * @param CallbackInterface|null $groupDblTap
     *
     * @return $this
     */
    public function setGroupDblTap(?CallbackInterface $groupDblTap): self
    {
        $this->groupDblTap = $groupDblTap;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getGroupTapHold(): ?CallbackInterface
    {
        return $this->groupTapHold;
    }

    /**
     * @param CallbackInterface|null $groupTapHold
     *
     * @return $this
     */
    public function setGroupTapHold(?CallbackInterface $groupTapHold): self
    {
        $this->groupTapHold = $groupTapHold;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowSelected(): ?CallbackInterface
    {
        return $this->rowSelected;
    }

    /**
     * @param CallbackInterface|null $rowSelected
     *
     * @return $this
     */
    public function setRowSelected(?CallbackInterface $rowSelected): self
    {
        $this->rowSelected = $rowSelected;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowDeselected(): ?CallbackInterface
    {
        return $this->rowDeselected;
    }

    /**
     * @param CallbackInterface|null $rowDeselected
     *
     * @return $this
     */
    public function setRowDeselected(?CallbackInterface $rowDeselected): self
    {
        $this->rowDeselected = $rowDeselected;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getRowSelectionChanged(): ?CallbackInterface
    {
        return $this->rowSelectionChanged;
    }

    /**
     * @param CallbackInterface|null $rowSelectionChanged
     *
     * @return $this
     */
    public function setRowSelectionChanged(?CallbackInterface $rowSelectionChanged): self
    {
        $this->rowSelectionChanged = $rowSelectionChanged;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsSendingStart(): ?CallbackInterface
    {
        return $this->movableRowsSendingStart;
    }

    /**
     * @param CallbackInterface|null $movableRowsSendingStart
     *
     * @return $this
     */
    public function setMovableRowsSendingStart(?CallbackInterface $movableRowsSendingStart): self
    {
        $this->movableRowsSendingStart = $movableRowsSendingStart;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsSent(): ?CallbackInterface
    {
        return $this->movableRowsSent;
    }

    /**
     * @param CallbackInterface|null $movableRowsSent
     *
     * @return $this
     */
    public function setMovableRowsSent(?CallbackInterface $movableRowsSent): self
    {
        $this->movableRowsSent = $movableRowsSent;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsSentFailed(): ?CallbackInterface
    {
        return $this->movableRowsSentFailed;
    }

    /**
     * @param CallbackInterface|null $movableRowsSentFailed
     *
     * @return $this
     */
    public function setMovableRowsSentFailed(?CallbackInterface $movableRowsSentFailed): self
    {
        $this->movableRowsSentFailed = $movableRowsSentFailed;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsSendingStop(): ?CallbackInterface
    {
        return $this->movableRowsSendingStop;
    }

    /**
     * @param CallbackInterface|null $movableRowsSendingStop
     *
     * @return $this
     */
    public function setMovableRowsSendingStop(?CallbackInterface $movableRowsSendingStop): self
    {
        $this->movableRowsSendingStop = $movableRowsSendingStop;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsReceivingStart(): ?CallbackInterface
    {
        return $this->movableRowsReceivingStart;
    }

    /**
     * @param CallbackInterface|null $movableRowsReceivingStart
     *
     * @return $this
     */
    public function setMovableRowsReceivingStart(?CallbackInterface $movableRowsReceivingStart): self
    {
        $this->movableRowsReceivingStart = $movableRowsReceivingStart;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsReceived(): ?CallbackInterface
    {
        return $this->movableRowsReceived;
    }

    /**
     * @param CallbackInterface|null $movableRowsReceived
     *
     * @return $this
     */
    public function setMovableRowsReceived(?CallbackInterface $movableRowsReceived): self
    {
        $this->movableRowsReceived = $movableRowsReceived;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsReceivedFailed(): ?CallbackInterface
    {
        return $this->movableRowsReceivedFailed;
    }

    /**
     * @param CallbackInterface|null $movableRowsReceivedFailed
     *
     * @return $this
     */
    public function setMovableRowsReceivedFailed(?CallbackInterface $movableRowsReceivedFailed): self
    {
        $this->movableRowsReceivedFailed = $movableRowsReceivedFailed;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getMovableRowsReceivingStop(): ?CallbackInterface
    {
        return $this->movableRowsReceivingStop;
    }

    /**
     * @param CallbackInterface|null $movableRowsReceivingStop
     *
     * @return $this
     */
    public function setMovableRowsReceivingStop(?CallbackInterface $movableRowsReceivingStop): self
    {
        $this->movableRowsReceivingStop = $movableRowsReceivingStop;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getValidationFailed(): ?CallbackInterface
    {
        return $this->validationFailed;
    }

    /**
     * @param CallbackInterface|null $validationFailed
     *
     * @return $this
     */
    public function setValidationFailed(?CallbackInterface $validationFailed): self
    {
        $this->validationFailed = $validationFailed;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHistoryUndo(): ?CallbackInterface
    {
        return $this->historyUndo;
    }

    /**
     * @param CallbackInterface|null $historyUndo
     *
     * @return $this
     */
    public function setHistoryUndo(?CallbackInterface $historyUndo): self
    {
        $this->historyUndo = $historyUndo;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getHistoryRedo(): ?CallbackInterface
    {
        return $this->historyRedo;
    }

    /**
     * @param CallbackInterface|null $historyRedo
     *
     * @return $this
     */
    public function setHistoryRedo(?CallbackInterface $historyRedo): self
    {
        $this->historyRedo = $historyRedo;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getClipboardCopied(): ?CallbackInterface
    {
        return $this->clipboardCopied;
    }

    /**
     * @param CallbackInterface|null $clipboardCopied
     *
     * @return $this
     */
    public function setClipboardCopied(?CallbackInterface $clipboardCopied): self
    {
        $this->clipboardCopied = $clipboardCopied;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getClipboardPasted(): ?CallbackInterface
    {
        return $this->clipboardPasted;
    }

    /**
     * @param CallbackInterface|null $clipboardPasted
     *
     * @return $this
     */
    public function setClipboardPasted(?CallbackInterface $clipboardPasted): self
    {
        $this->clipboardPasted = $clipboardPasted;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getClipboardPasteError(): ?CallbackInterface
    {
        return $this->clipboardPasteError;
    }

    /**
     * @param CallbackInterface|null $clipboardPasteError
     *
     * @return $this
     */
    public function setClipboardPasteError(?CallbackInterface $clipboardPasteError): self
    {
        $this->clipboardPasteError = $clipboardPasteError;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getDownloadReady(): ?CallbackInterface
    {
        return $this->downloadReady;
    }

    /**
     * @param CallbackInterface|null $downloadReady
     *
     * @return $this
     */
    public function setDownloadReady(?CallbackInterface $downloadReady): self
    {
        $this->downloadReady = $downloadReady;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDataTree(): ?bool
    {
        return $this->dataTree;
    }

    /**
     * @param bool|null $dataTree
     *
     * @return $this
     */
    public function setDataTree(?bool $dataTree): self
    {
        $this->dataTree = $dataTree;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSortOrderReverse(): ?bool
    {
        return $this->sortOrderReverse;
    }

    /**
     * @param bool|null $sortOrderReverse
     *
     * @return $this
     */
    public function setSortOrderReverse(?bool $sortOrderReverse): self
    {
        $this->sortOrderReverse = $sortOrderReverse;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getColumnHeaderSortMulti(): ?bool
    {
        return $this->columnHeaderSortMulti;
    }

    /**
     * @param bool|null $columnHeaderSortMulti
     *
     * @return $this
     */
    public function setColumnHeaderSortMulti(?bool $columnHeaderSortMulti): self
    {
        $this->columnHeaderSortMulti = $columnHeaderSortMulti;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSelectableRangeMode(): ?string
    {
        return $this->selectableRangeMode;
    }

    /**
     * @param string|null $selectableRangeMode
     *
     * @return $this
     */
    public function setSelectableRangeMode(?string $selectableRangeMode): self
    {
        $this->selectableRangeMode = $selectableRangeMode;

        return $this;
    }

    /**
     * @return false|string|null
     */
    public function getNestedFieldSeparator()
    {
        return $this->nestedFieldSeparator;
    }

    /**
     * @param false|string|null $nestedFieldSeparator
     *
     * @return TabulatorOptions
     */
    public function setNestedFieldSeparator($nestedFieldSeparator)
    {
        $this->nestedFieldSeparator = $nestedFieldSeparator;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDynamicHeightMax(): ?int
    {
        return $this->dynamicHeightMax;
    }

    /**
     * @param int|null $dynamicHeightMax
     *
     * @return $this
     */
    public function setDynamicHeightMax(?int $dynamicHeightMax): self
    {
        $this->dynamicHeightMax = $dynamicHeightMax;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDynamicHeightMin(): ?int
    {
        return $this->dynamicHeightMin;
    }

    /**
     * @param int|null $dynamicHeightMin
     *
     * @return $this
     */
    public function setDynamicHeightMin(?int $dynamicHeightMin): self
    {
        $this->dynamicHeightMin = $dynamicHeightMin;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDynamicHeightRows(): ?bool
    {
        return $this->dynamicHeightRows;
    }

    /**
     * @param bool|null $dynamicHeightRows
     *
     * @return $this
     */
    public function setDynamicHeightRows(?bool $dynamicHeightRows): self
    {
        $this->dynamicHeightRows = $dynamicHeightRows;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDynamicHeightRowCount(): ?int
    {
        return $this->dynamicHeightRowCount;
    }

    /**
     * @param int|null $dynamicHeightRowCount
     *
     * @return $this
     */
    public function setDynamicHeightRowCount(?int $dynamicHeightRowCount): self
    {
        $this->dynamicHeightRowCount = $dynamicHeightRowCount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDynamicHeightTarget(): ?string
    {
        return $this->dynamicHeightTarget;
    }

    /**
     * @param string|null $dynamicHeightTarget
     *
     * @return $this
     */
    public function setDynamicHeightTarget(?string $dynamicHeightTarget): self
    {
        $this->dynamicHeightTarget = $dynamicHeightTarget;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDynamicHeightTargetPercentage(): ?int
    {
        return $this->dynamicHeightTargetPercentage;
    }

    /**
     * @param int|null $dynamicHeightPercentage
     *
     * @return $this
     */
    public function setDynamicHeightTargetPercentage(?int $dynamicHeightTargetPercentage): self
    {
        $this->dynamicHeightTargetPercentage = $dynamicHeightTargetPercentage;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDynamicHeightTargetOffset(): ?int
    {
        return $this->dynamicHeightTargetOffset;
    }

    /**
     * @param int|null $dynamicHeightTargetOffset
     *
     * @return $this
     */
    public function setDynamicHeightTargetOffset(?int $dynamicHeightTargetOffset): self
    {
        $this->dynamicHeightTargetOffset = $dynamicHeightTargetOffset;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getInvalidOptionWarnings(): ?bool
    {
        return $this->invalidOptionWarnings;
    }

    /**
     * @param bool|null $invalidOptionWarnings
     *
     * @return $this
     */
    public function setInvalidOptionWarnings(?bool $invalidOptionWarnings): self
    {
        $this->invalidOptionWarnings = $invalidOptionWarnings;

        return $this;
    }
}
