<?php

namespace DolmIT\DataTablesBundle\DataTable\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * This is just the base HTML class.
 *
 * Class HtmlOptions
 */
class HtmlOptions extends AbstractOptions
{
    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}
