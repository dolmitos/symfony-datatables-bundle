<?php

namespace DolmIT\DataTablesBundle\DataTable\Options;

use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use DolmIT\DataTablesBundle\DataTable\Exception\OptionsFactoryException;
use Exception;

/**
 * Class Factory.
 */
class OptionsFactory
{
    /**
     * @param $type
     *
     * @return object
     *
     * @throws OptionsFactoryException
     */
    public static function createByType($type)
    {
        switch ($type) {
            case DataTableInterface::TABLE_TYPE_HTML:
                return static::create(HtmlOptions::class);
                break;
            case DataTableInterface::TABLE_TYPE_DATATABLES:
                return static::create(DatatablesOptions::class);
                break;
            case DataTableInterface::TABLE_TYPE_TABULATOR:
                return static::create(TabulatorOptions::class);
                break;
            default:
                throw new OptionsFactoryException('Factory::createByType(): expected valid table type of DataTableInterface');
                break;
        }
    }

    /**
     * Create.
     *
     * @param string|object $class
     *
     * @return object
     *
     * @throws OptionsFactoryException
     * @throws Exception
     */
    public static function create($class)
    {
        if (empty($class) || !\is_string($class) && !$class instanceof OptionsInterface) {
            throw new OptionsFactoryException('Factory::create(): String or OptionsInterface expected.');
        }

        if ($class instanceof OptionsInterface) {
            /* @var $class object */
            return $class;
        }

        if (\is_string($class) && class_exists($class)) {
            $instance = new $class();

            if (!$instance instanceof OptionsInterface) {
                throw new OptionsFactoryException("Factory::create(): Expected $class to be an instance of OptionsInterface.");
            } else {
                if ($instance instanceof AbstractOptions) {
                    $instance->initOptions(true);
                }

                return $instance;
            }
        } else {
            throw new OptionsFactoryException("Factory::create(): $class is not callable.");
        }
    }
}
