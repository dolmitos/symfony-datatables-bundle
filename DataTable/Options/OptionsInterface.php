<?php

namespace DolmIT\DataTablesBundle\DataTable\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

interface OptionsInterface
{
    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver);
}
