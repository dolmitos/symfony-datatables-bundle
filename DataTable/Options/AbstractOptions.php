<?php

namespace DolmIT\DataTablesBundle\DataTable\Options;

use DolmIT\DataTablesBundle\DataTable\Callback\CallbackInterface;
use DolmIT\DataTablesBundle\DataTable\OptionsTrait;
use ReflectionClass;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * NB! Before adding functionality to this class
 * make sure to not add any getMethods as we check those
 * for existing options for output.
 *
 * Class AbstractOptions
 */
class AbstractOptions implements OptionsInterface
{
    /*
     * Use the OptionsResolver.
     */
    use OptionsTrait;

    protected $optionsLoaded = false;

    //-------------------------------------------------
    // Filter
    //-------------------------------------------------

    /**
     * @var bool
     */
    protected $filterShown;

    /**
     * @var bool
     */
    protected $filterAjax;

    /**
     * @var CallbackInterface|null
     */
    protected $filterAjaxDataFilter;

    /**
     * @var CallbackInterface|null
     */
    protected $filterAjaxDone;

    /**
     * @var CallbackInterface|null
     */
    protected $filterAjaxFail;

    /**
     * @var CallbackInterface|null
     */
    protected $filterAjaxAlways;

    /**
     * @var CallbackInterface|null
     */
    protected $filterAjaxThen;

    //-------------------------------------------------
    // Base table options
    //-------------------------------------------------

    /**
     * @var bool
     */
    protected $headerShown;

    /**
     * Set the class names that the table element will have.
     *
     * @var string|null
     */
    protected $tableClasses;

    /**
     * Set table row (TH) classes.
     *
     * @var string|null
     */
    protected $tableHeaderClasses;

    /**
     * Set table row (TR) classes.
     *
     * @var string|null
     */
    protected $tableRowClasses;

    /**
     * Set table row (TD) classes.
     *
     * @var string|null
     */
    protected $tableCellClasses;

    /**
     * Placeholder text for the empty data screen
     * DOM overrides.
     *
     * @var string|null
     */
    protected $emptyPlaceholderText;

    /**
     * Placeholder DOM element for the empty data screen.
     *
     * Example: document.getElementById('element-id');
     *
     * @var string|null
     */
    protected $emptyPlaceholderDom;

    /**
     * Placeholder text for the loading screen
     * DOM overrides.
     *
     * @var string|null
     */
    protected $loadingPlaceholderText;

    /**
     * Placeholder DOM element for the loading screen.
     *
     * Example: document.getElementById('element-id');
     *
     * @var string|null
     */
    protected $loadingPlaceholderDom;

    /**
     * Options constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->initOptions();
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Loaded options flag to block template from being applied after options are set
            'options_loaded' => false,

            // Base table options
            'header_shown' => true,
            'table_classes' => null,
            'table_row_classes' => null,
            'table_header_classes' => null,
            'table_cell_classes' => null,

            // Filter
            'filter_shown' => true,
            'filter_ajax' => false,
            'filter_ajax_data_filter' => null,
            'filter_ajax_done' => null,
            'filter_ajax_fail' => null,
            'filter_ajax_always' => null,
            'filter_ajax_then' => null,

            // Placeholders
            'empty_placeholder_text' => null,
            'empty_placeholder_dom' => null,
            'loading_placeholder_text' => null,
            'loading_placeholder_dom' => null,
        ]);

        // Loaded options flag to block template from being applied after options are set
        $resolver->setAllowedTypes('options_loaded', ['boolean']);

        // Base table options
        $resolver->setAllowedTypes('header_shown', ['bool']);
        $resolver->setAllowedTypes('table_classes', ['null', 'string']);
        $resolver->setAllowedTypes('table_row_classes', ['null', 'string']);
        $resolver->setAllowedTypes('table_header_classes', ['null', 'string']);
        $resolver->setAllowedTypes('table_cell_classes', ['null', 'string']);

        // Filter
        $resolver->setAllowedTypes('filter_shown', ['bool']);
        $resolver->setAllowedTypes('filter_ajax', ['bool']);
        $resolver->setAllowedTypes('filter_ajax_data_filter', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('filter_ajax_done', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('filter_ajax_fail', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('filter_ajax_always', ['null', CallbackInterface::class]);
        $resolver->setAllowedTypes('filter_ajax_then', ['null', CallbackInterface::class]);

        // Placeholders
        $resolver->setAllowedTypes('empty_placeholder_text', ['null', 'string']);
        $resolver->setAllowedTypes('empty_placeholder_dom', ['null', 'string']);
        $resolver->setAllowedTypes('loading_placeholder_text', ['null', 'string']);
        $resolver->setAllowedTypes('loading_placeholder_dom', ['null', 'string']);

        return $this;
    }

    /**
     * Return Getter functions that should not be used
     * in automatic method handling.
     *
     * @return array
     */
    private function getDiscardedGetters()
    {
        return [
            'getDiscardedGetters',
            'getActiveMethods',
            'getAllMethods',
            'getBaseMethods',
            'getCallbacks',
            'getOptions',
        ];
    }

    /**
     * Get all "get" methods from $this.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    private function getAllMethods()
    {
        $objectMethods = get_class_methods($this);
        $methods = array_filter($objectMethods, function ($method) {
            return false !== strpos($method, 'get') && !\in_array($method, $this->getDiscardedGetters());
        });

        return $methods;
    }

    /**
     * Get class specific "get" methods from $this.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    private function getBaseMethods()
    {
        $reflection = new ReflectionClass($this);
        $methods = [];
        foreach ($reflection->getMethods() as $method) {
            if (
                false === strpos($method->class, 'AbstractOptions')
                    &&
                (
                    false !== strpos($method->name, 'get') && 0 == strpos($method->name, 'get') ||
                    false !== strpos($method->name, 'is') && 0 == strpos($method->name, 'is')
                )
                    &&
                (
                    !\in_array($method->name, $this->getDiscardedGetters())
                )
            ) {
                $methods[] = $method->name;
            }
        }

        return $methods;
    }

    /**
     * Returns all active methods
     * Meaning they have a result.
     *
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getActiveMethods()
    {
        $methods = $this->getBaseMethods();
        $result = array_map(
            function ($method) {
                $result = $this->{$method}();

                return [$method => $result];
            },
            array_filter(
                $methods,
                function ($method) {
                    return $this->{$method}() !== null;
                }
            )
        );
        $return = [];
        foreach ($result as $k => $item) {
            if (false !== strpos(key($item), 'is')) {
                $key = lcfirst(ltrim(key($item), 'is'));
            } elseif (false !== strpos(key($item), 'get')) {
                $key = lcfirst(ltrim(key($item), 'get'));
            }
            $return[$key] = current($item);
        }

        return $return;
    }

    /**
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getOptions()
    {
        $methods = $this->getActiveMethods();

        return array_filter($methods, function ($method) {return !$method instanceof CallbackInterface; });
    }

    /**
     * @return array
     *
     * @throws \ReflectionException
     */
    public function getCallbacks()
    {
        $methods = $this->getActiveMethods();

        return array_filter($methods, function ($method) {return $method instanceof CallbackInterface; });
    }

    /**
     * @return string|null
     */
    public function getTableClasses(): ?string
    {
        return $this->tableClasses;
    }

    /**
     * @param string|null $tableClasses
     *
     * @return $this
     */
    public function setTableClasses(?string $tableClasses): self
    {
        $this->tableClasses = $tableClasses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableHeaderClasses(): ?string
    {
        return $this->tableHeaderClasses;
    }

    /**
     * @param string|null $tableHeaderClasses
     *
     * @return $this
     */
    public function setTableHeaderClasses(?string $tableHeaderClasses): self
    {
        $this->tableHeaderClasses = $tableHeaderClasses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableRowClasses(): ?string
    {
        return $this->tableRowClasses;
    }

    /**
     * @param string|null $tableRowClasses
     *
     * @return $this
     */
    public function setTableRowClasses(?string $tableRowClasses): self
    {
        $this->tableRowClasses = $tableRowClasses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableCellClasses(): ?string
    {
        return $this->tableCellClasses;
    }

    /**
     * @param string|null $tableCellClasses
     *
     * @return $this
     */
    public function setTableCellClasses(?string $tableCellClasses): self
    {
        $this->tableCellClasses = $tableCellClasses;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOptionsLoaded(): bool
    {
        return $this->optionsLoaded;
    }

    /**
     * @param bool $optionsLoaded
     *
     * @return $this
     */
    public function setOptionsLoaded(bool $optionsLoaded): self
    {
        $this->optionsLoaded = $optionsLoaded;

        return $this;
    }

    /**
     * @return bool
     */
    public function isFilterShown(): bool
    {
        return $this->filterShown;
    }

    /**
     * @param bool $filterShown
     *
     * @return $this
     */
    public function setFilterShown(bool $filterShown): self
    {
        $this->filterShown = $filterShown;

        return $this;
    }

    /**
     * @return bool
     */
    public function isFilterAjax(): bool
    {
        return $this->filterAjax;
    }

    /**
     * @param bool $filterAjax
     *
     * @return $this
     */
    public function setFilterAjax(bool $filterAjax): self
    {
        $this->filterAjax = $filterAjax;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getFilterAjaxDataFilter(): ?CallbackInterface
    {
        return $this->filterAjaxDataFilter;
    }

    /**
     * @param CallbackInterface|null $filterAjaxDataFilter
     *
     * @return $this
     */
    public function setFilterAjaxDataFilter(?CallbackInterface $filterAjaxDataFilter): self
    {
        $this->filterAjaxDataFilter = $filterAjaxDataFilter;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getFilterAjaxDone(): ?CallbackInterface
    {
        return $this->filterAjaxDone;
    }

    /**
     * @param CallbackInterface|null $filterAjaxDone
     *
     * @return $this
     */
    public function setFilterAjaxDone(?CallbackInterface $filterAjaxDone): self
    {
        $this->filterAjaxDone = $filterAjaxDone;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getFilterAjaxFail(): ?CallbackInterface
    {
        return $this->filterAjaxFail;
    }

    /**
     * @param CallbackInterface|null $filterAjaxFail
     *
     * @return $this
     */
    public function setFilterAjaxFail(?CallbackInterface $filterAjaxFail): self
    {
        $this->filterAjaxFail = $filterAjaxFail;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getFilterAjaxAlways(): ?CallbackInterface
    {
        return $this->filterAjaxAlways;
    }

    /**
     * @param CallbackInterface|null $filterAjaxAlways
     *
     * @return $this
     */
    public function setFilterAjaxAlways(?CallbackInterface $filterAjaxAlways): self
    {
        $this->filterAjaxAlways = $filterAjaxAlways;

        return $this;
    }

    /**
     * @return CallbackInterface|null
     */
    public function getFilterAjaxThen(): ?CallbackInterface
    {
        return $this->filterAjaxThen;
    }

    /**
     * @param CallbackInterface|null $filterAjaxThen
     *
     * @return $this
     */
    public function setFilterAjaxThen(?CallbackInterface $filterAjaxThen): self
    {
        $this->filterAjaxThen = $filterAjaxThen;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmptyPlaceholderText(): ?string
    {
        return $this->emptyPlaceholderText;
    }

    /**
     * @param string|null $emptyPlaceholderText
     *
     * @return $this
     */
    public function setEmptyPlaceholderText(?string $emptyPlaceholderText): self
    {
        $this->emptyPlaceholderText = $emptyPlaceholderText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmptyPlaceholderDom(): ?string
    {
        return $this->emptyPlaceholderDom;
    }

    /**
     * @param string|null $emptyPlaceholderDom
     *
     * @return $this
     */
    public function setEmptyPlaceholderDom(?string $emptyPlaceholderDom): self
    {
        $this->emptyPlaceholderDom = $emptyPlaceholderDom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLoadingPlaceholderText(): ?string
    {
        return $this->loadingPlaceholderText;
    }

    /**
     * @param string|null $loadingPlaceholderText
     *
     * @return $this
     */
    public function setLoadingPlaceholderText(?string $loadingPlaceholderText): self
    {
        $this->loadingPlaceholderText = $loadingPlaceholderText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLoadingPlaceholderDom(): ?string
    {
        return $this->loadingPlaceholderDom;
    }

    /**
     * @param string|null $loadingPlaceholderDom
     *
     * @return $this
     */
    public function setLoadingPlaceholderDom(?string $loadingPlaceholderDom): self
    {
        $this->loadingPlaceholderDom = $loadingPlaceholderDom;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHeaderShown(): bool
    {
        return $this->headerShown;
    }

    /**
     * @param bool $headerShown
     *
     * @return $this
     */
    public function setHeaderShown(bool $headerShown): self
    {
        $this->headerShown = $headerShown;

        return $this;
    }
}
