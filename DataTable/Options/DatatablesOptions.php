<?php

namespace DolmIT\DataTablesBundle\DataTable\Options;

use Symfony\Component\OptionsResolver\OptionsResolver;

class DatatablesOptions extends AbstractOptions
{
    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([]);

        return $this;
    }
}
