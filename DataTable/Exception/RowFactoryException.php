<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class RowFactoryException extends \Exception
{
}
