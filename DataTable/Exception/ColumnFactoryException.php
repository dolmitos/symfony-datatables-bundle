<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class ColumnFactoryException extends \Exception
{
}
