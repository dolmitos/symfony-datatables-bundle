<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class DataTableNameValidationException extends \Exception
{
}
