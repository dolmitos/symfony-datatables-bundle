<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class OptionsSetBeforeTemplateLoadedException extends \Exception
{
}
