<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class DataTableFactoryException extends \Exception
{
}
