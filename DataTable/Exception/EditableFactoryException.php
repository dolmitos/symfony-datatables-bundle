<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class EditableFactoryException extends \Exception
{
}
