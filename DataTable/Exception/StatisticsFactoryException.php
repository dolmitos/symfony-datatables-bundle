<?php

namespace DolmIT\DataTablesBundle\DataTable\Exception;

class StatisticsFactoryException extends \Exception
{
}
