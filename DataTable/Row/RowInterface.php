<?php

namespace DolmIT\DataTablesBundle\DataTable\Row;

use Symfony\Component\OptionsResolver\OptionsResolver;

interface RowInterface
{
    public function configureOptions(OptionsResolver $resolver);
}
