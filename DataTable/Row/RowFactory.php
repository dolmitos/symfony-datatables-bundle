<?php

namespace DolmIT\DataTablesBundle\DataTable\Row;

use DolmIT\DataTablesBundle\DataTable\Exception\RowFactoryException;

/**
 * Class Factory.
 */
class RowFactory
{
    /**
     * Create.
     *
     * @param string|object $class
     *
     * @return object
     *
     * @throws RowFactoryException
     */
    public static function create($class)
    {
        if (empty($class) || !\is_string($class) && !$class instanceof RowInterface) {
            throw new RowFactoryException('Factory::create(): String or RowInterface expected.');
        }

        if ($class instanceof RowInterface) {
            /* @var $class object */
            return $class;
        }

        if (\is_string($class) && class_exists($class)) {
            $instance = new $class();

            if (!$instance instanceof RowInterface) {
                throw new RowFactoryException("Factory::create(): Expected $class to be an instance of RowInterface.");
            } else {
                return $instance;
            }
        } else {
            throw new RowFactoryException("Factory::create(): $class is not callable.");
        }
    }
}
