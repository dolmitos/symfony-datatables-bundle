<?php

namespace DolmIT\DataTablesBundle\DataTable\Row;

use Symfony\Component\OptionsResolver\OptionsResolver;

class Row extends AbstractRow
{
    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }
}
