<?php

namespace DolmIT\DataTablesBundle\DataTable\Row;

use DolmIT\DataTablesBundle\DataTable\OptionsTrait;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig_Environment;

/**
 * Class AbstractColumn.
 */
abstract class AbstractRow implements RowInterface
{
    /*
     * Use the OptionsResolver.
     */
    use OptionsTrait;

    /**
     * Adds class names to row.
     * Default: null.
     *
     * @var string|null
     */
    protected $classNames;

    /**
     * The internal identifier of the row.
     * Is set in the RowBuilder.
     *
     * @var int
     */
    protected $index;

    /**
     * Hold the data for the given row.
     *
     * @var array|null
     */
    protected $data;

    /**
     * Enable or disable the display of this row.
     * Default: true.
     *
     * @var bool
     */
    protected $visible;

    /**
     * The Twig Environment to render Twig templates in Column rows.
     * Is set in the ColumnBuilder.
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * The name of the associated DataTable.
     * Is set in the RowBuilder.
     *
     * @var string
     */
    protected $datatableName;

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * Config options.
     *
     * @param OptionsResolver $resolver
     *
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['identifier']);

        $resolver->setDefaults([
            'class_names' => null,
            'data' => null,
            'visible' => true,
        ]);

        $resolver->setAllowedTypes('class_names', ['null', 'string']);
        $resolver->setAllowedTypes('data', ['null', 'array']);
        $resolver->setAllowedTypes('visible', 'bool');

        return $this;
    }

    /**
     * @param string $identifier
     *
     * @return mixed
     */
    public function getValue(string $identifier)
    {
        return isset($this->data[$identifier]) ? $this->data[$identifier] : null;
    }

    /**
     * @param string $identifier
     * @param null   $value
     *
     * @return $this
     */
    public function setValue(string $identifier, $value = null): self
    {
        $this->data[$identifier] = $value;

        return $this;
    }

    /**
     * @param string $identifier
     *
     * @return bool
     */
    public function isArrayValue(string $identifier)
    {
        return \is_array($this->getValue($identifier));
    }

    //-------------------------------------------------
    // Getters & Setters
    //-------------------------------------------------

    /**
     * @return string|null
     */
    public function getClassNames(): ?string
    {
        return $this->classNames;
    }

    /**
     * @param string|null $classNames
     *
     * @return AbstractRow
     */
    public function setClassNames(?string $classNames): self
    {
        $this->classNames = $classNames;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return $this->visible;
    }

    /**
     * @param bool $visible
     *
     * @return AbstractRow
     */
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get Twig.
     *
     * @return Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Set Twig.
     *
     * @param Twig_Environment $twig
     *
     * @return $this
     */
    public function setTwig(Twig_Environment $twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get datatableName.
     *
     * @return string
     */
    public function getDataTableName()
    {
        return $this->datatableName;
    }

    /**
     * Set datatableName.
     *
     * @param string $datatableName
     *
     * @return $this
     */
    public function setDataTableName($datatableName)
    {
        $this->datatableName = $datatableName;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array|null $data
     *
     * @return $this
     */
    public function setData(?array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @param int $index
     *
     * @return $this
     */
    public function setIndex(int $index): self
    {
        $this->index = $index;

        return $this;
    }
}
