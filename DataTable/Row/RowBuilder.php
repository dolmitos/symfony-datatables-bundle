<?php

namespace DolmIT\DataTablesBundle\DataTable\Row;

use Exception;
use Twig_Environment;

/**
 * Class RowBuilder.
 */
class RowBuilder
{
    /**
     * The Twig Environment.
     *
     * @var Twig_Environment
     */
    private $twig;

    /**
     * The name of the associated DataTable.
     *
     * @var string
     */
    private $datatableName;

    /**
     * The generated Rows.
     *
     * @var array
     */
    private $rows;

    /**
     * ColumnBuilder constructor.
     *
     * @param Twig_Environment $twig
     * @param string           $datatableName
     */
    public function __construct(Twig_Environment $twig, $datatableName)
    {
        $this->twig = $twig;
        $this->datatableName = $datatableName;

        $this->rows = [];
    }

    //-------------------------------------------------
    // Builder
    //-------------------------------------------------

    /**
     * Add Row.
     *
     * @param string|RowInterface $class
     * @param array               $options
     *
     * @return $this
     *
     * @throws Exception
     */
    public function add($class, array $options = [])
    {
        /* @var $column AbstractRow */
        $row = RowFactory::create($class);
        $row->initOptions();

        $this->setEnvironmentProperties($row);
        $row->set($options);

        $this->addRow($row);

        return $this;
    }

    //-------------------------------------------------
    // Getters && Setters
    //-------------------------------------------------

    /**
     * Get rows.
     *
     * @return array
     */
    public function getRows()
    {
        return $this->rows;
    }

    //-------------------------------------------------
    // Helper
    //-------------------------------------------------

    /**
     * Set environment properties.
     *
     * @param AbstractRow $column
     *
     * @return $this
     */
    private function setEnvironmentProperties(AbstractRow $column)
    {
        $column->setDataTableName($this->datatableName);
        $column->setTwig($this->twig);

        return $this;
    }

    /**
     * Adds a Row.
     *
     * @param AbstractRow $row
     *
     * @return $this
     */
    private function addRow(AbstractRow $row)
    {
        $index = \count($this->rows);
        $row->setIndex($index);
        $this->rows[] = $row;

        return $this;
    }
}
