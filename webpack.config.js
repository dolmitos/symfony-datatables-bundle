var Encore = require('@symfony/webpack-encore');


Encore
    // the project directory where compiled assets will be stored
    .setOutputPath('Resources/public/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .addEntry('js/datatables', './assets/js/datatables.js')
    .addStyleEntry('css/datatables', './assets/css/datatables.scss')
    .enableSassLoader()
    .enableSourceMaps(true)
    .cleanupOutputBeforeBuild()
    .configureBabel(function(babelConfig) {
        babelConfig.presets.push('env');
    })
;

const config = Encore.getWebpackConfig();
config.watchOptions = {
    poll: 1000,
    aggregateTimeout: 300
};
module.exports = [config];