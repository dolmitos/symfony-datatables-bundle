<?php

namespace DolmIT\DataTablesBundle\EventSubscriber;

use DolmIT\DataTablesBundle\DataTable\Column\BulkSelectionColumn;
use DolmIT\DataTablesBundle\Event\DataTableDataSetEvent;
use DolmIT\DataTablesBundle\Event\DataTableEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddBulkCheckedEventSubscriber implements EventSubscriberInterface
{
    public function onDataSet(DataTableDataSetEvent $event)
    {
        $columns = $event->getDataTable()->getColumnBuilder()->getRealColumns();

        if ($columns) {
            foreach ($columns as $key => $column) {
                if ($column instanceof BulkSelectionColumn) {
                    $data = $event->getData();
                    if ($data && \count($data) > 0) {
                        foreach ($data as &$row) {
                            // Add identifier as bulk value if none is set
                            if (!isset($row['bulk']) && isset($row['id'])) {
                                $row['bulk'] = $row['id'];
                            }
                            // Add bulk_checked value to hold checkbox value in datatable
                            if (!isset($row['bulk_checked'])) {
                                $row['bulk_checked'] = 0;
                            }
                        }
                    }
                    $event->setData($data);
                }
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            DataTableEvents::DATATABLE_DATA_SET => [['onDataSet', 20]],
        ];
    }
}
