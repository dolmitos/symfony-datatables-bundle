<?php

namespace DolmIT\DataTablesBundle\Response;

use DolmIT\DataTablesBundle\DataTable\DataTableInterface;
use DolmIT\DataTablesBundle\DataTable\Formatter\DataTableFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;

class DataTableResponse
{
    /**
     * @var DataTableInterface
     */
    private $dataTable;

    public function __construct(DataTableInterface $dataTable)
    {
        $this->dataTable = $dataTable;
    }

    /**
     * @return JsonResponse
     */
    public function getResponse()
    {
        $formatter = new DataTableFormatter();
        $formatter->runFormatters($this->dataTable);

        $outputHeader = [];

        if ($this->dataTable->isTypeTabulator()) {
            $outputHeader = [
                'last_page' => $this->dataTable->getCount() ? ceil($this->dataTable->getCount() / $this->dataTable->getPerPage()) : 1,
                'current_page' => $this->dataTable->getPage() ? $this->dataTable->getPage() : 1,
                'data' => [],
                'statistics' => [],
                'errors' => [],
            ];
        }

        $response = new JsonResponse();
        $response->setData(
            array_merge(
                $outputHeader,
                [
                    'data' => $this->dataTable->getData(),
                    'statistics' => $this->dataTable->getStatisticsData(),
                    'errors' => $this->dataTable->getErrors(),
                ]
            )
        );

        return $response;
    }
}
