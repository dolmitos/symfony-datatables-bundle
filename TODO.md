- Placeholder template sources need to be placed into tabulator config and loaded dynamically on datatable
  buildup. - Currently when called dynamically the placeholder wouldn't work.

- Remove reliance on jQuery as Tabulator no longer uses it either
- Statistics should be made more dynamic, currently only the class can be changed on the statistic element
    - Though there is the option to manually add the statistic blocks, which have ids that are tracked by the bundle and the data
      will be entered automatically.
    - Something to think about

- Add multi level column grouping by allowing group columns to have their own group_identifiers
    - This requires 
        - Some cleanup in columns.js.twig
        - Better management when adding groupings