"use strict";

class Utility {

    static findAncestor(el, cls, tag) {
        while (
            (el = el.parentElement) &&
            (
                (cls && tag && el.tagName.toLowerCase() !== tag && !el.classList.contains(cls)) ||
                (cls && !tag && !el.classList.contains(cls)) ||
                (!cls && tag && el.tagName.toLowerCase() !== tag) ||
                (!cls && !tag)
            )
            );
        return el;
    }

    static formToObject( form ) {
        let output = {};
        new FormData( form ).forEach(
            ( value, key ) => {
                // Remove brackets from key, to be able to contain straight arrays in output
                // The reason being, that while it would be correct to have key[]=1, key[]=2, Tabulator will send key[]=[0=>1,1=>2]
                if (key.indexOf('[]') !== -1) {
                    key = key.replace('[]','');
                    if ( !Object.prototype.hasOwnProperty.call( output, key ) ) {
                        output[ key ] = [];
                    }
                }
                // Check if property already exist
                if ( Object.prototype.hasOwnProperty.call( output, key )) {
                    let current = output[ key ];
                    if ( !Array.isArray( current ) ) {
                        // If it's not an array, convert it to an array.
                        current = output[ key ] = [ current ];
                    }
                    current.push( value ); // Add the new value to the array.
                } else {
                    output[ key ] = value;
                }
            }
        );
        return output;
    }

    static extend(obj, src) {
        for (var key in src) {
            if (src.hasOwnProperty(key)) obj[key] = src[key];
        }
        return obj;
    }

}

module.exports = Utility;