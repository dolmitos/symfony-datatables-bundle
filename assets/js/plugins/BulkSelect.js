let Utility = require('./utility');

module.exports = {

    /* selector => [], selector2 => []*/
    selected: [],
    
    selectAll: function(checkbox) {
        let dataTableSelector = '#' + Utility.findAncestor(checkbox,'tabulator').id;

        if (checkbox.checked) {
            this.selected[dataTableSelector] = true;
        } else {
            this.selected[dataTableSelector] = [];
        }

        this.applySelection(dataTableSelector);

        return true;
    },

    applySelection: function(dataTableSelector) {
        if (this.selected[dataTableSelector] === undefined) return false;

        let updateData = [], checkboxesToSkip = [];

        if (DataTables.container[dataTableSelector].getType() === DataTable.typeTabulator()) {
            let data = DataTables.container[dataTableSelector].getTabulator().getData();

            // Reset all checkboxes only if check all isn't checked
            if (this.selected[dataTableSelector] !== true) {
                for (let i in data) {
                    updateData.push({id:data[i].id,bulk_checked:0});
                }
                DataTables.container[dataTableSelector].getTabulator().updateData(updateData);
                updateData = [];
            }

            // Check all checkboxes
            if (this.selected[dataTableSelector] === true) {
                for (let i in data) {
                    if (!data[i].bulk_disabled) {
                        updateData.push({id:data[i].id,bulk_checked:1});
                    } else {
                        checkboxesToSkip.push(data[i].id);
                    }
                }
                DataTables.container[dataTableSelector].getTabulator().updateData(updateData);

                let checkboxes = document.querySelector(dataTableSelector).querySelectorAll('input.bulk-checkbox');
                for (let i = 0; i < checkboxes.length; i++) {
                    if (!checkboxesToSkip.includes(checkboxes[i].value)) {
                        checkboxes[i].checked = true;
                    }
                }

                DataTables.actionBar.open(dataTableSelector);
                return true;
            }

            if (this.selected[dataTableSelector].length > 0) {
                let checkboxes = document.querySelector(dataTableSelector).querySelectorAll('input.bulk-checkbox');
                for (let i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].checked = !!DataTables.bulkSelect.checkSelected(dataTableSelector, checkboxes[i].value);
                    updateData.push({id:checkboxes[i].value,bulk_checked: (checkboxes[i].checked ? 1 : 0) });
                }
                DataTables.actionBar.open(dataTableSelector);
            } else {
                let checkboxes = document.querySelector(dataTableSelector).querySelectorAll('input.bulk-checkbox');
                for (let i = 0; i < checkboxes.length; i++) {
                    checkboxes[i].checked = false;
                }
                DataTables.actionBar.close(dataTableSelector);
            }

            if (updateData.length) {
                DataTables.container[dataTableSelector].getTabulator().updateData(updateData);
            }

        }


        return true;
    },

    setSelected: function(dataTableSelector, value) {
        if (this.selected[dataTableSelector] === undefined) {
            this.selected[dataTableSelector] = [];
        }

        if (this.selected[dataTableSelector] !== true) this.selected[dataTableSelector].push(value);
    },

    select: function(checkbox) {
        let dataTableSelector = '#' + Utility.findAncestor(checkbox,'tabulator').id;

        if (this.selected[dataTableSelector] === undefined) {
            this.selected[dataTableSelector] = [];
        }

        if (this.selected[dataTableSelector] === true) {
            checkbox.checked = true;
            return true;
        }

        if (checkbox.checked) {
            if (!this.checkSelected(dataTableSelector, checkbox.value)) {
                this.selected[dataTableSelector].push(checkbox.value);
            }
        } else {
            if (this.checkSelected(dataTableSelector, checkbox.value)) {
                this.selected[dataTableSelector].splice(this.getSelectedIndex(dataTableSelector, checkbox.value),1);
            }
        }

        this.applySelection(dataTableSelector);

        return true;
    },

    checkSelected: function(selector, value) {
        var arr = this.selected[selector] || [];
        if (arr === true) return true;
        return this.arrayContains.call(arr, value) > -1;
    },

    getSelectedIndex: function(selector, value) {
        var arr = this.selected[selector] || [];
        return this.arrayContains.call(arr, value);
    },

    arrayContains: function(needle) {
        // Per spec, the way to identify NaN is that it is not equal to itself
        var findNaN = needle !== needle;
        var indexOf;

        if(!findNaN && typeof Array.prototype.indexOf === 'function') {
            indexOf = Array.prototype.indexOf;
        } else {
            indexOf = function(needle) {
                var i = -1, index = -1;

                for(i = 0; i < this.length; i++) {
                    var item = this[i];

                    if((findNaN && item !== item) || item === needle) {
                        index = i;
                        break;
                    }
                }

                return index;
            };
        }

        return indexOf.call(this, needle);
    }

};