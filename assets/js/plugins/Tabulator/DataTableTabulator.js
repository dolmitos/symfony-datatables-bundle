let heightAdjuster = require('./DataTableTabulatorHeightAdjuster');
let CellFormFormatter = require('./DataTableTabulatorCellFormFormatter');

let Utility = require('../utility');

module.exports = {

    placeholders: [],

    heightAdjuster: heightAdjuster,

    createCellFormFormatter(config) {
        return new CellFormFormatter(config);
    },

    initialize: function(tabulator, config) {
        this.heightAdjuster.initializeConfig(tabulator, config);

        this.heightAdjuster.updateTableHeight(tabulator);
    },

    ajaxRequestFunc: function(tabulator, url, config, params) {
        let self = this;

        var requestPromise = tabulator.defaultLoaderPromise(url, config, params);
        self.setPlaceholderLoading(tabulator);

        return new Promise(function(resolve, reject){
            requestPromise.then(function(data){
                resolve(data);
                self.removePlaceholder(tabulator);
                if (data.data && data.data.length === 0) self.setPlaceholderEmpty(tabulator);
            });
            requestPromise.catch(function(errorRequest) {
                // Force a resolve on error and simply return empty data
                resolve({'data':[],'page':1,'last_page':1,'statistics':[],'errors':[errorRequest.statusText || 'Server response error']});
                self.setPlaceholderEmpty(tabulator);
            });
        });
    },

    setPlaceholderLoading: function(tabulator) {
        let selector = '#' + tabulator.table.element.id;
        let template;
        if (this.placeholders[selector] && this.placeholders[selector]['loading']) {
            template = this.placeholders[selector]['loading'];
        }

        if (template) {
            try {
                this.removePlaceholder(tabulator);
                tabulator.table.options.placeholder = template;
                tabulator.table.redraw();
            }catch (e) {console.log(e);}
        }
    },

    removePlaceholder: function(tabulator) {
        try {
            let placeholder;

            while (placeholder = tabulator.table.element.querySelector('.tabulator-placeholder')) {
                placeholder.parentElement.removeChild(placeholder);
            }
            tabulator.table.options.placeholder = null;
            tabulator.table.redraw();
        } catch (e) {console.log(e);}
    },

    setPlaceholderEmpty: function(tabulator) {
        let selector = '#' + tabulator.table.element.id;
        let template;
        if (this.placeholders[selector] && this.placeholders[selector]['empty']) {
            template = this.placeholders[selector]['empty'];
        }

        if (template) {
            try {
                this.removePlaceholder(tabulator);
                tabulator.table.options.placeholder = template;
                tabulator.table.redraw();
            } catch (e) {
                console.log(e);
            }
        }
    },

    handleFilters: function(dataTableSelector) {
        let dataTableContainer = Utility.findAncestor(document.querySelector(dataTableSelector),'datatable-container'),
            dataTable = DataTables.container[dataTableSelector],
            filterForm = dataTableContainer.querySelector('.datatable-filter-container form'),
            url = filterForm.getAttribute('action'),
            method = filterForm.getAttribute('method');

        let filterParams = Utility.formToObject(filterForm);

        let ajaxConfig = {
            method: method, //set request type to Position
            headers: {
                "Accept": 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            }, //set specific content type
            credentials: 'include'
        };

        dataTable
            .getTabulator()
            .setData(url, filterParams, ajaxConfig)
            .then(function(data){

            })
            .catch(function(error){
                console.log(error);
            });
    },

};