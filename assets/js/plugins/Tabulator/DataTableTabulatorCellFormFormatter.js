"use strict";

class CellFormFormatter {
    constructor(config) {
        this.template = config.template || undefined;
        this.position = config.position || 0;
        this.identifier = config.identifier || undefined;
        this.data = config.data || {};
        this.formPrefix = config.formPrefix || '';
        this.element = undefined;
        this.updateElements = undefined;
        this.formElement = undefined;
        this.overrideDefault = (undefined !== config.overrideDefault ? config.overrideDefault : true);
    }

    configureUpdateElements() {
        this.getElement();
        this.updateElements = [];
        let allElements = this.getElement().getElementsByTagName("*");
        // Currently we're allowing all elements under this cell to be given a value
        // They only have to match the correct field identifier
        for (let i = 0, n = allElements.length; i < n; ++i) {
            let el = allElements[i];
            if (el.id && el.id.indexOf(this.formPrefix) === 0) {
                let matchedKey = this.matchIdToDataKey(el.id);
                if (false !== matchedKey) {
                    if (matchedKey === this.identifier) {
                        this.formElement = el;
                    }
                    if (this.overrideDefault) {
                        this.updateElements.push({
                            'identifier' : matchedKey,
                            'querySelector' : '#' + el.id,
                            'attribute' : null
                        });
                    }
                }
            }
        }
    }

    getUpdateElements() {
        if (undefined === this.updateElements) {
            this.configureUpdateElements();
        }
        return this.updateElements;
    }

    setUpdateElements(updateElements) {
        this.updateElements = updateElements;
    }
    
    getDataByIdentifier(identifier) {
        let value = this.data[identifier] || null;
        if (!value && identifier.indexOf('.') !== -1) {
            let level = 0;
            let path = identifier.split('.');
            let tmpData = this.data;
            while (tmpData[path[level]] && typeof tmpData[path[level]] === 'object') {
                tmpData = tmpData[path[level]];
                ++level;
            }
            value = tmpData[path[level]];
        }
        
        return value;
    }

    buildUpdateElements() {
        if (this.updateElements.length) {
            for (let i = 0, n = this.updateElements.length; i < n; ++i) {
                if (!this.updateElements[i].querySelector) this.updateElements[i].querySelector = '#' + this.formElement.id;
                let updateElement = this.getElement().querySelector(this.updateElements[i].querySelector);
                if (null !== updateElement) {
                    if (!this.updateElements[i].attribute) {
                        if (updateElement.nodeName === 'INPUT') {
                            updateElement.value = this.getDataByIdentifier(this.updateElements[i].identifier) || '';
                        } else if (updateElement.nodeName === 'IMG') {
                            updateElement.setAttribute('src', this.getDataByIdentifier(this.updateElements[i].identifier) || '');
                        } else if (updateElement.nodeName === 'SELECT') {
                            let opts = updateElement.options;
                            for (let opt, j = 0; opt = opts[j]; j++) {
                                if (opt.value === this.getDataByIdentifier(this.updateElements[i].identifier)) {
                                    updateElement.selectedIndex = j;
                                    break;
                                }
                            }
                        } else {
                            updateElement.innerHTML = this.getDataByIdentifier(this.updateElements[i].identifier) || '';
                        }
                    } else {
                        let attributeValue = this.updateElements[i].identifier ? this.getDataByIdentifier(this.updateElements[i].identifier) : this.updateElements[i].attributeValue;
                        updateElement.setAttribute(this.updateElements[i].attribute, attributeValue);
                    }
                }
            }
        }

    }

    getFormElement() {
        return this.formElement;
    }

    getElement() {
        if (undefined === this.element) {
            let cellTemplate = document.createElement('div');
            cellTemplate.innerHTML = this.template.replace(/__name__/g, this.position);
            // If this is empty, the contents were probably replaced in buildUpdateElements
            this.element = cellTemplate;
        }
        return this.element;
    }

    getDataKeyMap(data) {
        let dataKeys = [];
        let tmpKeys = data ? Object.keys(data) : [];
        tmpKeys.forEach((key) => {
            if (data[key] && typeof data[key] === "object") {
                let subDataKeys = this.getDataKeyMap(data[key]);
                subDataKeys.forEach((subKey) => {
                    dataKeys.push(key + '.' + subKey);
                });
            } else {
                dataKeys.push(key);
            }
        });

        return dataKeys;
    }


    matchIdToDataKey(id) {
        let dataKeys = this.getDataKeyMap(this.data);
        let potentialMatches = [];
        for(var i = 0, n = dataKeys.length; i < n; ++i) {
            let tmpDataKey = dataKeys[i].replace('.','_')
            if (
                id.indexOf('_' + tmpDataKey) !== -1 &&
                id.indexOf('_' + tmpDataKey) === (id.length - tmpDataKey.length - 1)) potentialMatches.push(dataKeys[i]);
        }
        if (potentialMatches.length > 1) {
            let longestMatchLength = 0, longestMatchIndex = 0;
            potentialMatches.forEach(function(match, index) {
                if (longestMatchLength < match.length) {
                    longestMatchLength = match.length;
                    longestMatchIndex = index;
                }
            });
            return potentialMatches[longestMatchIndex] || false;
        } else {
            return potentialMatches.pop() || false;
        }

        return false;
    }

}


module.exports = CellFormFormatter;
