module.exports = {

    heightConfigurationHolder: [],

    optimizedResize: require('../OptimizedResize'),
    
    initializeConfig: function(tabulator, config, selector) {
        if (!selector) {
            let selector = '#' + tabulator.element.id;
        }

        if (selector !== undefined) {

            this.heightConfigurationHolder[selector] = {};

            this.heightConfigurationHolder[selector].dynamicHeightMax = config.dynamicHeightMax || undefined;
            this.heightConfigurationHolder[selector].dynamicHeightMin = config.dynamicHeightMin || undefined;
            this.heightConfigurationHolder[selector].dynamicHeightRows = config.dynamicHeightRows || false;
            this.heightConfigurationHolder[selector].dynamicHeightRowCount = config.dynamicHeightRowCount || 10;
            this.heightConfigurationHolder[selector].dynamicHeightTarget = config.dynamicHeightTarget || undefined;
            this.heightConfigurationHolder[selector].dynamicHeightTargetPercentage = config.dynamicHeightTargetPercentage || 90;
            this.heightConfigurationHolder[selector].dynamicHeightTargetOffset = config.dynamicHeightTargetOffset || 0;

            let self = this;
            // Tabulator may not exist yet
            if (tabulator) {
                this.optimizedResize.add(function() {
                    self.updateTableHeight(tabulator);
                });
            }
        }
    },
    
    updateTableHeight: function(tabulator,rowCount) {
        let selector = '#' + tabulator.element.id;
        let height = parseInt(tabulator.element.style.height || 0);
        let dynamicHeightActive = false;
        if (!rowCount) rowCount = tabulator.rowManager.getDataCount();

        try {
            if (selector !== undefined && this.heightConfigurationHolder[selector]) {
                if (this.heightConfigurationHolder[selector].dynamicHeightRows) {
                    dynamicHeightActive = true;
                    let rowHeight = (
                        tabulator.rowManager.getRowFromPosition(0) &&
                        tabulator.rowManager.getRowFromPosition(0).getHeight()
                    ) ? tabulator.rowManager.getRowFromPosition(0).getHeight() : 42;
                    height = rowHeight *
                        (rowCount < this.heightConfigurationHolder[selector].dynamicHeightRowCount ?
                            rowCount : this.heightConfigurationHolder[selector].dynamicHeightRowCount)
                    height += 4; // Account for some bordering issues
                } else if (this.heightConfigurationHolder[selector].dynamicHeightTarget !== undefined) {
                    dynamicHeightActive = true;
                    let target = document.querySelector(this.heightConfigurationHolder[selector].dynamicHeightTarget);
                    let containerHeight;
                    if (target !== null && target !== undefined) {
                        containerHeight = target.offsetHeight;
                        // Account for padding
                        containerHeight -= parseInt(window.getComputedStyle(target).getPropertyValue('padding-top'));
                        containerHeight -= parseInt(window.getComputedStyle(target).getPropertyValue('padding-bottom'));
                    }

                    if (this.heightConfigurationHolder[selector].dynamicHeightTargetOffset > 0) {
                        containerHeight -= this.heightConfigurationHolder[selector].dynamicHeightTargetOffset;
                    }

                    if (containerHeight > 0) {

                        // Header container
                        let header = document.getElementById(tabulator.element.id.replace('datatable_table_','datatable_header_container_'));
                        if (header !== null && header !== undefined) containerHeight -= header.offsetHeight;
                        // Footer container
                        let footer = document.getElementById(tabulator.element.id.replace('datatable_table_','datatable_footer_container_'));
                        if (footer !== null && footer !== undefined) containerHeight -= footer.offsetHeight;

                    }

                    if (this.heightConfigurationHolder[selector].dynamicHeightTargetPercentage) {
                        containerHeight *= this.heightConfigurationHolder[selector].dynamicHeightTargetPercentage / 100;
                    }
                    height = containerHeight;
                }

                if (isNaN(height)) height = 0;

                if (
                    dynamicHeightActive &&
                    this.heightConfigurationHolder[selector].dynamicHeightMax !== undefined &&
                    height > this.heightConfigurationHolder[selector].dynamicHeightMax
                ) {
                    height = this.heightConfigurationHolder[selector].dynamicHeightMax;
                }

                if (
                    dynamicHeightActive &&
                    this.heightConfigurationHolder[selector].dynamicHeightMin !== undefined &&
                    height < this.heightConfigurationHolder[selector].dynamicHeightMin
                ) {
                    height = this.heightConfigurationHolder[selector].dynamicHeightMin;
                }


            }
        } catch (e) {}

        if (parseInt(tabulator.element.style.height) != Math.floor(height) && Math.floor(height) != 0) {
            height += tabulator.columnManager.getColumnByIndex(0).getHeight() ? tabulator.columnManager.getColumnByIndex(0).getHeight() : 40;
            tabulator.setHeight(height);
            tabulator.redraw();
        }
    },

    
};