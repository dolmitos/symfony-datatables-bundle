let Utility = require('./utility');

module.exports = {

    open: function(dataTableSelector) {
        let dataTableContainer = Utility.findAncestor(document.querySelector(dataTableSelector),'datatable-container');

        let actionBar = dataTableContainer.querySelector('.datatable-action-bar');
        if (actionBar) {
            if (!actionBar.classList.contains('opened'))actionBar.classList.add('opened');
            actionBar.classList.remove('closed');
        }
    },

    close: function(dataTableSelector) {
        let dataTableContainer = Utility.findAncestor(document.querySelector(dataTableSelector),'datatable-container');

        let actionBar = dataTableContainer.querySelector('.datatable-action-bar');
        if (actionBar) {
            if (!actionBar.classList.contains('closed')) actionBar.classList.add('closed');
            actionBar.classList.remove('opened');
        }
    },

};