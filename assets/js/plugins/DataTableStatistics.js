module.exports = {

    handleStatistics: function(dataTableElement, statistics) {
        for(let identifier in statistics) {
            let id = dataTableElement.id + '_' + identifier;
            let statisticElement = document.getElementById(id);
            if (statisticElement) {
                let container = statisticElement.querySelector('.value-container');
                if (container) {
                    container.innerHTML = statistics[identifier];
                }
            }
        }
    },

};