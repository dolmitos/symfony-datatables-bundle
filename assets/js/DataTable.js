"use strict";

const TYPE_TABULATOR = 'TYPE_TABULATOR';
const Tabulator = require('tabulator-tables');

/**
 * New DataTable automatically creates a Tabulator element, should it be a type TABULATOR
 *
 * Tabulator is given a property dataTable, which matches this object.
 *
 * This object manages the rest of the tabulator instance as a wrapper
 */
class DataTable {

    constructor(config) {
        this.name = config.name || undefined;
        this.selector = config.selector || undefined;
        this.type = config.type || undefined;
        this.data = config.data || undefined;
        this.options = config.options || undefined;
        this.editableKey = (undefined !== this.options ? this.options.editableKey : null) || 'edit';
        this.stateEditable = (undefined !== this.options ? this.options.defaultCellStateEditable : null) || false;

        if (this.getType() === TYPE_TABULATOR) {
            this.tabulator = new Tabulator(this.selector, this.options);
            this.tabulator.dataTable = this;
        }
    }

    addRow(data, pos, index) {
        let promise = this.getTabulator().addRow(data, pos, index);
        DataTables.tabulator.heightAdjuster.updateTableHeight(this.getTabulator());
        return promise;
    }

    editRow(index, row) {
        if (!row) row = this.getTabulator().getRow(index);

        let updateData = {};
        updateData[this.getTabulator().options.index] = row.getData()[this.getTabulator().options.index];
        updateData[this.editableKey] = true;
        this.getTabulator().updateData([updateData]);
    }

    edit() {
        if (false === this.stateEditable) {
            if (this.getType() === TYPE_TABULATOR) {
                let rows = this.getTabulator().getRows();
                for (let i = 0; i < rows.length; i++) {
                    this.editRow(i, rows[i]);
                }
            }

            this.getTabulator().redraw(true);
        }
        this.stateEditable = true;
    }

    viewRow(index, row) {
        if (!row) row = this.getTabulator().getRow(index);

        let updateData = {};
        updateData[this.getTabulator().options.index] = row.getData()[this.getTabulator().options.index];
        updateData[this.editableKey] = false;
        this.getTabulator().updateData([updateData]);
    }

    view() {
        if (true === this.stateEditable) {
            if (this.getType() === TYPE_TABULATOR) {
                let rows = this.getTabulator().getRows();
                for (let i = 0; i < rows.length; i++) {
                    this.viewRow(i, rows[i]);
                }
            }

            this.getTabulator().redraw(true);
        }
        this.stateEditable = false;
    }

    getStateEditable() {
        return this.stateEditable;
    }


    getEditableKey() {
        return this.editableKey;
    }

    getType() {
        return this.type;
    }
    
    getSelector() {
        return this.selector;
    }
    
    getOptions() {
        return this.options;
    }

    getTabulator() {
        return this.tabulator;
    }

    static typeTabulator() {
        return TYPE_TABULATOR;
    }

    static typeHtml() {
        return TYPE_HTML;
    }
}


module.exports = DataTable;
