import 'promise-polyfill/src/polyfill';
import 'formdata-polyfill';

let DataTables = {
    container : [],
    bulkSelect : require("./plugins/BulkSelect"),
    actionBar : require("./plugins/ActionBar"),
    tabulator : require("./plugins/Tabulator/DataTableTabulator"),
    statistics : require("./plugins/DataTableStatistics"),

    add: function(dataTableObject) {
        this.container[dataTableObject.getSelector()] = dataTableObject;

        if (dataTableObject.getType() === DataTable.typeTabulator()) {
            this.tabulator.initialize(dataTableObject.getTabulator(), dataTableObject.getOptions());
        }
    }
};

global.DataTables = DataTables;

let Utility = require('./plugins/utility');
global.DataTablesUtility = Utility;

let DataTable = require('./DataTable');
global.DataTable = DataTable;