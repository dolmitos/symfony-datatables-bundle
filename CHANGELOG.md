# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.5.7.3] - 2019-11-06
- Add IMG to cell formatter options and fill its' source with the value

## [0.5.7.2] - 2019-09-25
- Do not mark rows checkboxes as checked if they are disabled

## [0.5.7.1] - 2019-09-17
- Fix issue with BulkSelect check all logic breaking 

## [0.5.7] - 2019-09-10
- Add option to set disabled status for bulk select through bulk_disabled 
- Add option to set checkboxes as selected by default
- Improve performance of BulkSelect slightly
- Update bulk_checked value on checkbox creation to match BulkSelect container values

## [0.5.6] - 2019-09-05
- Add ability to map multi level data onto stepped form fields through the CellFormFormatter
- Add ability to map prototype fields by dot separation 

For example: parent->child->child form map can now match the DataTable identifier parent.child.child. 


## [0.5.5] - 2019-08-22
- Fix issue with BulkSelect never updating bulk_checked value properly
- Add rowHandle to column options

## [0.5.4.2] - 2019-08-14
- Update Tabulator to 4.4.1

## [0.5.4.1] - 2019-08-13
- Add name of the datatable to DataTable object
- Allow DataTable addRow to determine positioning through additional arguments


## [0.5.4] - 2019-08-12
- Fix issue with DataTableDataSetEvent not accepting null for data, even though other places do accept it
- Fix issue with custom placeholder always expecting a loader, even though it might not be required
- Upgrade to Tabulator 4.4.0 for additional stability
- Add invalidOptionsWarnings option to Tabulator table and set it to false by default
- Fix issue with non-progressive-loading Tabulator tables and allow the ajaxResponse callback to send only the data array to Tabulator
- Fix issue with renderStartedPassthrough callback being populated with dataLoadedPassthrough, thus also fixing height updater
- Add NaN management for dynamic height calculations

## [0.5.3.2] - 2019-07-12
- Remove lock files from the package
- Move webpack-encore-pack to require-dev
- Require php-cs-fixer
- Require husky and lint-staged
- Activate php-cs-fixer automatically on commited php files
- Add polling to webpack watch
- Fix bug where form field identifier could have been matched to a identifier string

  This occurred when the form field identifier was the same length as the identifier string, 
  thus searching for the string from the form field identifier resulted in a -1 and the length comparison check
  also returned -1 because of the expected additional _ symbol. -1 === X - X - 1

## [0.5.3.1] - 2019-06-04
- Allow strings to be used in responsive_layout tabulator option
- Add responsive parameter to column to be able to determine the order of hiding in responsive mode

## [0.5.3] - 2019-06-03
- Fix issue with statistic element where value container might not be found when doing manual statistic output
- Remove columnNames option from ColumnBuilder as it was never needed
- Add identifier as a key when adding columns to the columnBuilder
- Remove index from columns to prepare for group columns which will make it irrelevant 
- Add basic Tabulator column grouping for a single parent

## [0.5.2.2] - 2019-05-27
- Refix issue with getTreeParent, previous fix was not functional and broke positioning for child elements

## [0.5.2.1] - 2019-05-21
- Fix issue where getTreeParent would give error as it does not check for existance of dataTree property

## [0.5.2] - 2019-05-21
- Upgrade to Tabulator 4.2.7
- Add tabulator css to library since we already include the JS
- Change resize activation form dataLoaded to renderStarted callback due to change in Tabulator 4.2.5

## [0.5.1.8] - 2019-04-29
- Add position override for tree children

## [0.5.1.7] - 2019-04-22
- Ignore cell setValue error if it has been replaced moments before
- Add FormData extended support for IE11 and other sub-par browsers

## [0.5.1.6] - 2019-04-18
- Add width_shrink and width_grow to AbstractColumn
- Remove blur callback from form formatter
- Only set cell value if element exists
- Check if data element exists before asking for the length of it

## [0.5.1.5] - 2019-04-14
- Separate FormattableColumnInterface and ColumnInterface
- Attach an event to querySelectable element within cell element to update the cell value
- Update Tabulator to 4.2.5
- Fix issue with CellFormFormatter key matching with similar identifiers

## [0.5.1.4] - 2019-04-11
- Fix issue where option method starting with is were not loaded into config

## [0.5.1.3] - 2019-04-11
- Return promise when passing through row add to Tabulator
- Bugfix: form_override_default default was false though it should be true

## [0.5.1.2] - 2019-04-11
- Add ability to disable default override of cell form element

## [0.5.1.1] - 2019-04-10
- Add configFormatter call when obtaining only DataTable config
- Fix issue with DataTable data update when switching between view and edit modes, index key must match

## [0.5.1] - 2019-04-10
- Add render_datatable_tabulator_config twig function to return the options required for the datatable build
- Fix DataTable loading when no options are given
- Add convenience function addRow to DataTable class
- Add row resize to addRow function in DataTable class
- Add method for attaching a parent to form prototype to be able to include the datatable in an external form

## [0.5.0] - 2019-04-02
- Add php-cs-fixer configuration
- php-cs-fixer first run, update all files
- Add sourcemaps to compiled files
- Fix bulkselect single deselect
- Remove FormColumn dependency from BulkSelectionColumn
- Bugfix: Allow add_if to be added to filter
- Remove add_if option, as it was too basic to provide any real use
- Remove unnecessary property from FilterBuilder
- Change add function to private, as there is no way to property use it from an external source
- Change default filter form location and naming
- Add validation check to adding separate filter fields
- Remove custom filters in favor of symfony forms
- Remove checkbox_without_label type widget
- Add formatterParams option to column
- Bugfix: Small bug when gathering BaseMethods from AbstractOptions
- Allow for dynamic setting of column options, including from custom column classes
- Remove CheckboxWithoutLabelType - no longer used
- Add FormBuilder to DataTable to allow for form class field prototype rendering for cell contents
- Fix issues with Cell Form Formatter 
  
  1. Elements with multiple children had only the first child returned to the cell
  2. Issue where custom elements would be overwritten if they were using the same form prefix for their ids, 
      solution was to limit the automatic updating to only field ids that start with the form prefix
- Fix issue where formatting would give error is a formBuilder was not attached to the datatable
- Fix issue where manually added form fields would not exist on the prototype form
- Add multiple ways of adding attributes to form types through the FormMappingCallback
- php-cs-fixer run
- BC Break: Remove FormColumn usage option
- Remove useless validation checks from filter&formBuilders
- Combine similar matches when building tabulator columns and options
- Add editable flag to data when formBuilder is attached to the datatable
- Add wrapper for Tabulator and include functions to switch between edit/view
- Rebuild FormBuilder into EditFormBuilder and ViewFormBuilder to allow for custom form types used for view purposes

## [0.4.2.10] - 2019-02-13
- Fix wrong interface typehint on StatisticInterface::renderCellContent

## [0.4.2.9] - 2019-02-07
- Remove jQuery usage from JS files - this does not yet include view templates
- Fixed issue with Custom Filters where multiple select fields sent data in a format Symfony Forms could not interpret

## [0.4.2.8] - 2019-01-28
- Add column alignment options
- Add column title translation parameter options

## [0.4.2.7] - 2018-12-19
- Add headerFilterParams, editor and editorParams options to columns

## [0.4.2.6] - 2018-12-04
- Add column definition to FormCallback
- Add data to Tabulator if it is set immideately after creation
- Minor tweaks to automatic HeightAdjuster
- Stop HeightAdjustor size jumps by setting new height only when data is being set, not unset 

## [0.4.2.5] - 2018-11-20
- Fix filter existance check when outputting datatable custom filters
- Hide entire header container if headerShown is false

## [0.4.2.4] - 2018-11-15
- Add router and tokenstorage services to every DataTable by default
  
  For easy access to url building and authenticated user information
- Add AuthenticationChecker service and make it, router and tokenstorage optional

  Optional for systems not using users 

## [0.4.2.3] - 2018-11-15
- Add headerShown option
- Hide unfinished action bar
- Show filters block only when filterShown is true
- Move statistics check outside of container to remove unused element

## [0.4.2.2] - 2018-11-13
- Simplify form output to cells
- Fix cssClass on column for tabulator

## [0.4.2.1] - 2018-11-08
- Add headerFilter option to columns

## [0.4.2] - 2018-11-05
- Update tabulator options to comply with 4.1.0 
- Fix issue with the string 'get' in option method name
- Improve return type uniformity in AbstractOptions
- Add dynamic height options to Tabulator

## [0.4.1] - 2018-11-02
- Add ability to add errors to DataTable output
- Fix tabulator requirement to correct version (4.0.5)
- Add ability to set loader and empty placeholders for tabulator, include defaults

## [0.4] - 2018-10-23
- Remove Content-type to comply with changes in 4.0.2
- Disable html datatable until a fix is found for IE
- Include credentials in fetch requests through ajaxConfig

## [0.3.5.1] - 2018-10-22
- Show empty value when statistics have not yet loaded

## [0.3.5] - 2018-10-18
- Remove action bar placeholder button
- Allow for a more modular approach, separate datatable blocks

## [0.3.4] - 2018-10-01
- Fix issue where checkSelected wouldn't return true when all rows were checked
- Add ability to use Callbacks on columns
- Add eventDispatcher to DataTables
- Add DataSet event to DataTables
- Add bulk_checked data if BulkSelectionColumn is used
- Add bulk value automatically from id, if set
- Replace scroll listener with Tabulator based formatter for bulk select

## [0.3.3] - 2018-09-28
- Add renderable if data missing option to FormColumn
- Move DataTable js objects into a separate master object
- Rework callback passthrough and add dataLoaded passthrough
- Add BulkSelectionColumn
- Add bulkSelect checkbox selection logic
- Fix BulkSelect arrayContains
- Add bulkSelect applicator to ajax_response
- Add dummy ActionBar

## [0.3.2] - 2018-09-19
- Add ajax_response_passthrough
- Change FormColumn use of form option to mandatory
- Change formatter to allow for future formatter types
- Add Statistics block

## [0.3.1] - 2018-09-18
- Fix issue with multi form field being overwritten by last value

## [0.3.0] - 2018-09-13
- Set editable default null instead of false
- Upgrade to Tabulator 4.0
    - Remove deprecated options of sort and sort_dir
    - Update DataTableTabulator
    - Update DataTable initialization
    - Remove deprecated options of filter_type and filter_value
    - Rename option filter to filters
- Fix issue where options have the wrong type
- Restructure column options
- Add option validation for ajax_progressive_load and pagination

## [0.2.0] - 2018-09-12
- Fix DataTable Column sortable option
- Add ability to add FormFactory to every column for future use
- Remove placeholder text from footer 
- Add renderCellContent base function 
- Add DataTableFormatter
- Fix function return type hints (minor)
- Add FormColumn 
- Add DataTableResponse 
- Remove unused code
- Add default settings to DataTable generation based on type
- Add ability to add informational data into DataTable (count, page, perPage)
- Restructrure DataTableResponse and DataTableFormatter
- Fix default settings Tabulator callback template missing
- Add DataTableFormatter to table render 

## [0.1.6.2] - 2018-08-27
- Fix issue where DataTableTabulator was not properly loaded as a usable variable
- Use DataTableTabulator in filter logic instead of hardcoded JS
- Add formatter to column options

## [0.1.5] - 2018-07-28
- Moved translation domain to the top of each translation concerned template
- Changed ajaxProgressiveLoad to allow for string values, as Tabulator requires
- Added better description for ajaxFilterOverride
- Fix self made bug - added back previously removed getOptions from OptionsTrait
- Fix issue where filter default value was not used in first ajax request

## [0.1.4] - 2018-07-25
- Set Javascript part of filter within the check for an existing form
- Add ability to set a simple sub table in a cell for HTML type
- Allow column title to be null
- Fix issue where setting new options would override anything set previously with the default value.


## [0.1.3] - 2018-07-18
- Restructured Filter output
- Changed Filter form naming scheme to standard used by Form

## [0.1.2] - 2018-07-17
- Fix error where filter form was requested even when filter was not set

## [0.1.1] - 2018-07-17
- Fix error where bundle templates were called with the old method


## [0.1.0] - 2018-07-17
- Release basic Tabulator DataTable


[Unreleased]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.7.3...HEAD
[0.5.7.3]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.7.2...v0.5.7.3
[0.5.7.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.7.1...v0.5.7.2
[0.5.7.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.7...v0.5.7.1
[0.5.7]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.6...v0.5.7
[0.5.6]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.4.2...v0.5.5
[0.5.4.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.4.1...v0.5.4.2
[0.5.4.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.4...v0.5.4.1
[0.5.4]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.3.2...v0.5.4
[0.5.3.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.3.1...v0.5.3.2
[0.5.3.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.3...v0.5.3.1
[0.5.3]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.2.2...v0.5.3
[0.5.2.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.2.1...v0.5.2.2
[0.5.2.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.2...v0.5.2.1
[0.5.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.8...v0.5.2
[0.5.1.8]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.7...v0.5.1.8
[0.5.1.7]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.6...v0.5.1.7
[0.5.1.6]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.5...v0.5.1.6
[0.5.1.5]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.4...v0.5.1.5
[0.5.1.4]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.3...v0.5.1.4
[0.5.1.3]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.2...v0.5.1.3
[0.5.1.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1.1...v0.5.1.2
[0.5.1.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.1...v0.5.1.1
[0.5.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.10...v0.5.0
[0.4.2.10]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.9...v0.4.2.10
[0.4.2.9]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.8...v0.4.2.9
[0.4.2.8]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.7...v0.4.2.8
[0.4.2.7]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.6...v0.4.2.7
[0.4.2.6]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.5...v0.4.2.6
[0.4.2.5]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.4...v0.4.2.5
[0.4.2.4]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.3...v0.4.2.4
[0.4.2.3]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.2...v0.4.2.3
[0.4.2.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2.1...v0.4.2.2
[0.4.2.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.2...v0.4.2.1
[0.4.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.4...v0.4.1
[0.4]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.5.1...v0.4
[0.3.5.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.5...v0.3.5.1
[0.3.5]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.4...v0.3.5
[0.3.4]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.3...v0.3.4
[0.3.3]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.6.2...v0.2.0
[0.1.6.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.5...v0.1.6.2
[0.1.5]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/dolmitos/symfony-datatables-bundle/compare/v0.1.0
