DataTablesBundle 
====================

Reusable DataTables for Symfony 4
Built with [Tabulator](http://tabulator.info) V4.0

Dolm IT
------

<img src="https://www.dolmit.com/images/logo.svg" height="40px" alt="Dolm IT logo">
<br>

[Dolm IT](https://www.dolmit.com)

Documentation
-------------

Documentation is available on [Docs](Resources/doc/index.rst).

MIT License
-----------

License can be found [here](LICENSE).

Inspiration
----------
 - [Stwe / DataTablesBundle (Based off of)](https://github.com/stwe/DataTablesBundle)
 - [J. Brown / DataTablesBundle](https://bitbucket.org/brown298/datatablesbundle)
